
\section{Analysis of the Density Estimates and Forecasts} 

In this section I validate the framework developed here by considering the density estimates and forecasts that
it provides. 
As discussed in the introduction, we have two types of densities. 
First, I will consider the realized densities $RD_t$ as they do not require any parametric assumptions.
Then I will build a simple parametric model for the log-volatilities and consider the predictive densities
$h\left(r_t \mvert \F_{t-1}\right)$.

\subsection{Realized Density Comparison}

As discussed in the estimation section, we can estimate the realized density $RD_t$ by $N\left(0,
\widehat{\int_{t-1}^t \sigma^2(s) \dif s}\right) \ast \Lap\left(0, \widehat{\int_{t}^{t-1} \gamma^2(s) \dif
s}\right)$.
The question at hand is, obviously, are we doing this well?
Taken as a whole, we are.
To get into more detail, I compare my estimates to the pure diffusion realized densities computed by 
\textcite{andersen2003modeling}.  
To be fair to them in my diffusion density estimates, I follow them and use  the total variation as the
diffusion variation.
In other words, I use $\N\left(0, \widehat{\int_{t-1}^t \sigma^2(s) \dif s} + \widehat{\int_{t-1}^2 \gamma^2(s)
\dif s} \right)$ as the realized density in the diffusion case.
If I just used the diffusion volatility, I would force the diffusion volatility to badly miss the variance of the
daily return and unfairly advantage my estimates.

\begin{figure}[htb]
    \centering
    \caption{Standardized Realized Density $\left(\frac{r_t}{\sqrt{\Var(r_t)}}\right)$}
    \label{fig:realized_density_estimates}
    \includegraphics[width=.5\textwidth, height=.5\textwidth]{realized_density_comparison.pdf} 
\end{figure}%

Before, I quantitatively compare of the performance of the two estimators, it is useful to graphically compare the
estimates.
In \cref{fig:realized_density_estimates}, I show three different graphs, the densities of jump-diffusion
estimates, diffusion estimates
In order to make the data comparable across various estimates I standardize the returns by their unconditional
variance. 
The two estimates are very similar and also look similar to the data.

\begin{figure}[htb]
    \caption{Realized Density Comparison (PIT)}
    \label{fig:realized_density_comparison}
    \begin{subfigure}[t]{.49\textwidth}
        \centering
        \includegraphics[width=\textwidth, height=\textwidth]{realized_density_qq_comparison.pdf} 
    \end{subfigure}%
    \begin{subfigure}[t]{.49\textwidth}
        \centering
        \includegraphics[width=\textwidth, height=\textwidth]{jump_diffusion_pit_acf.pdf} \\

    \end{subfigure}
\end{figure}


I compare the estimates more rigorously in \cref{fig:realized_density_estimates} by computing the probability
integral transforms.
Again both densities perform well.
The jump-diffusion estimator does slightly better.
The only real issue is that the left-shoulder of the distribution is not adequately taken into account in either
of the estimators, but the jump-diffusion estimator does slightly better.  

If we look at the difference in deviations, i.e.\@ the graph in the lower-left corner of
\cref{fig:realized_density_comparison}, we can see that the jump-diffusion estimator dominates the diffusion
estimator almost uniformly across quantiles.
\cref{fig:realized_density_comparison} is the absolute deviations of the diffusion density PIT from its ideal
value minus the equivalent deviations for the jump-diffusion PIT.
The jump-diffusion estimator outperforms the most in the shoulders of the distribution.

Since the jump-diffusion density has fatter tails than the associated diffusion density, a careful examination of
the graph implies that the diffusion estimator is placing too much weight at relatively high-quantiles, likely so
that it does not badly mess them up.
This is to be expected as it is minimizing a squared criterion which overweights large deviations..
Intuitively,  the diffusion estimator is attempting to account for the fat-tails by making the variance larger. 
This suggests that in the extreme tails, where we do not have data to compare it to, the diffusion estimator will
once again perform poorly because as is the case here, the Gaussian density cannot match both the $99th$
percentile, and the $99.99th$ percentile say, just like it failing to simultaneously match the \num{75}th and
\num{95}th percentiles.

The graph on the right is the autocorrelation function of the probability integral transform.
As we can see, it is essentially independent and identically distributed across time.
As a result, the main error we have in the estimates is the skewness in the data that is not accounted for
properly.


If we consider formal tests of validity, both densities are rejected, but it takes significantly less data to
reject the diffusion estimator, see \cref{tbl:realized_density_comparison}.
This is likely from the skewness in the data which is, admittedly, not picked up appropriately by either
estimator. 
If we compare the log-predictive score, the two estimates are quite close, but the jump-diffusion does do slightly
better.\footnote{This is why the jump-diffusion estimate is circled.} 
These results are for the entire sample, but if we consider subsamples or other measures of closeness of
distributions, we get qualitatively similar results.


\begin{table}[ht]
    \centering
    \caption{Realized Density Comparison}
    \label{tbl:realized_density_comparison}
    \begin{tabularx}{\textwidth}{c X X}
        & Jump-Diffusion  & Diffusion \\
        \midrule 
        Log Probability Score  & \circletext{\num{3.235145058058402}} & \num{3.230137514257573} \\
        Kolmogorov-Smirnov p-value & \num{2.7571023686334684e-08} & \num{4.8605119928879503e-11}  \\
    \end{tabularx}
\end{table}

\subsection{Forecast Density Comparison}

To estimate the predictive distribution $h\left(r_t \mvert \F_{t-1}\right)$ we need to take a stance on the
predictive distribution of the volatilities---what I was calling $G$ in the \cref{sec:intro}.
In this section, I start by describing the model I use for $G$ in the jump-diffusion case and the analogous $G$
that I use in the diffusion case. 

I wan to stay simple as possible in this section, and so I follow much of the literature and adopt a parametric
Gaussian model for the dynamics of the log-volatilities.
As for their dynamics, I use an autoregressive model with the number of lags chosen by the Schwarz Information
Criterion (SIC).   

\begin{definition}[Jump-Diffusion Model]
    \begin{align*}
        r_t &\sim \N\left(0, \diffvolt\right) \ast \Lap\left(0, \jumpvolt\right) \\
        \log \begin{pmatrix}\diffvolt & \jumpvolt\end{pmatrix}' &\sim \text{Gaussian VAR}\left(\text{num
            lags}^{BIC}\right)  
    \end{align*}
\end{definition}

In the diffusion case, I also adopt an autoregressive framework with Gaussian errors for the log volatility.  
However, as argued for in \textcite{bollerslev1987conditionally} among many others, to reduce the potential for
misspecification in the tails, I allowed the innovations to the return distribution to have Student's
$t$-distributed errors. 
It is worth noting that as the degree-of-freedom parameter $\nu$ increases the $t$-distribution approaches the
Gaussian one, and so this is not a restriction.
Since I have a large amount of data, having to estimate an extra parameter should not affect the results
significantly by itself.

\begin{definition}[Diffusion Model]
    \begin{align*}
        \log \diffvolt &\sim \text{Gaussian\ AR}\left(\text{num lags}^{BIC}\right) \\
        r_t &\sim t(0, \diffvolt, \nu^{MLE}) 
    \end{align*}
\end{definition} 


Since the log-volatilities are observed (with error), estimating the autoregressive parameters can be done using
OLS.    
I use maximum likelihood to estimate the degrees of freedom parameter $\nu$.
I started \num{200} periods in, and then did rolling window estimation moving forward.
This way, I respect the error in estimating the parameters.

As can be seen in \cref{fig:forecast_jump_diffusion_model}, the estimated distribution does quite well. 
(The red line is the realization, the middle band contains the middle \num{50}\% of the distribution, and the
bands in contain \num{99}\% of the distribution in total.)
The realizations hit the various confidence intervals without approximately the correct probability, and the
estimated distributions are quite tight around the true distributions.

\begin{figure}[ht]
    \caption{Forecast Distributions Comparison}
    \label{fig:forecast_distribution_comparison}
    \begin{subfigure}[b]{.49\textwidth}
        \caption{Jump-Diffusion Model}
        \label{fig:forecast_jump_diffusion_model}
        \includegraphics[width=\textwidth]{jump_diffusion_forecast.pdf}  
    \end{subfigure}% 
    %      
    \begin{subfigure}[b]{.49\textwidth}
        \caption{Diffusion Model}
        \label{fig:forecast_diffusion_model}
        \includegraphics[width=\textwidth]{diffusion_forecast.pdf} 
    \end{subfigure}
\end{figure}


In \cref{fig:forecast_diffusion_model}, we also have reasonably good coverage over most of the distribution. 
\todo{Reword this.}
However, in order to pick up the occasional extreme-events caused by the time-varying fat-tails, the estimator
finds a low finds a low number for the degrees of freedom.
(Since we are doing rolling-window estimation, we have several different estimates depending on the time-period.
They cluster around \num{4}.)
This is likely caused by only having one parameter govern the fatness of the tails. 
In the jump-diffusion case, if the proposition of the jumps increase, the tails of the distribution will get
fatter.
In other words, the jump-diffusion model can handle time-variation in the fatness of the tails. 
Unless we let the degree-of-freedom parameter vary over time, the diffusion model cannot do this.
However, if let the degree-of-freedom parameter change over time it would not be identified.

To quantitatively compare between the two densities, I computed the log predictive score and the
continuously ranked probability score (CRPS). 
The log predictive score is essentially the Kullback-Leibler divergence used to compare density performance.
Here we do about \num{7} log-points better.
In other words, our estimator is about \num{7}\% closer to the true density on average. 
This may not sound like that much, but if we consider, as can be seen in
\cref{fig:forecast_distribution_comparison}, most of this out-performance is coming from the tails, since
financial assets are often highly leveraged, this implies significant increases in risk measurement which can then
be used to significantly increase risk-adjusted profit.

The CRPS is another commonly used measure of divergence, which I present primarily as a robustness check.
The correct measure of difference between densities will depend upon the application at hand.
The CRPS is the average absolute difference between the true CDF and the model-implied CDF.
Here again, we see that we are outperforming. 

\begin{table}[ht]
    \caption{Density Forecast Comparison}
    \centering
    \begin{tabularx}{\textwidth}{c X X}
        & Jump-Diffusion & Diffusion \\
        \toprule 
        Log Predictive Score  & \circletext{\num{3.05}} & \num{2.98} \\
        Continuously Ranked Probability Score  & \circletext{\num{3.42}} &  \num{3.43} \\
    \end{tabularx}
\end{table}

Now, I switch to absolute measures of performance of the estimator, instead of comparative ones.
Like I did in the previous section, I compute the probability integral transforms (PII)'s and seee how they
preform. 
I then compare the results of these two calculations.
The pit will be performing well if it is uniformly distributed an i.i.d over time.
By examining \cref{fig:density_forecast_evaluation}, we can see that this is the case.
The only even slight issue is that the data are skewed.
In particular, it performs well in the tails, and does not have the \textquote{butterfly-shape} that is often
found when the fat-tails are not appropriately accounted for.

\begin{figure}[htb]
    \caption{Density Forecast Evaluation}
    \label{fig:density_forecast_evaluation} 
    \begin{subfigure}[t]{.49\textwidth}
        \centering
        \caption{Probability Integral Transform (PIT) Density}
        \label{fig:forecast_pit_density}
        \includegraphics[width=\textwidth]{var_pit_density.pdf} 
    \end{subfigure}%
    \begin{subfigure}[t]{.49\textwidth}
        \centering
        \caption{PIT Autocorrelation}
        \label{fig:forecast_pit_autocorrelation}
        \includegraphics[width=\textwidth]{var_pit_autocorrelation.pdf} 
    \end{subfigure}
\end{figure}




        
