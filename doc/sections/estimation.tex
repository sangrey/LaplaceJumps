
\section{Estimation}\label{sec:estimation_framework}

This section constructs estimators for $\diffvolf{t}$ and $\jumpvolf{t}$ and their daily analogs. As is standard, the data do not identify the drift, and so we cannot estimate it. The proposed estimator for $\diffvolf{t}$ is adapted from \textcite{jacod2013quarticity}. I show that their estimator is still valid under my slightly more general assumptions. The estimator for $\jumpvolf{t}$ is completely new. In particular, I develop a consistent estimator for $\jumpvolf{\tau}$ for any fixed $\tau$.\footnote{In general, much of the theory that I develop can likely be extended to stopping times, but I leave that for future work.} This estimator is the first to consistently estimate any instantaneous jump variation measure. This implies that high-frequency data nonparametrically identify instantaneous jump dynamics. 

\subsection{Assumptions} \label{sec:assumptions}

I start by fixing notation and stating some assumptions. The instantaneous volatility estimators take an appropriately defined average over an increasing number of increments over a shrinking interval. In other words, for a given index --- $n$, we have a triangular array of increments. To make the notation even more complicated, we have both a true DGP with time-varying volatility and an approximate DGP, whose volatility is locally constant. 

This setup implies we must keep track of both triangular arrays as we take limits. I adopt the notation used in \textcite{jacod2012discretization} for the most part. Specifically, I use $\Delta^n_i p$ to refer to a increment $i$ in process $\logpricef{t}$ of length $\Delta^n$, and I take limits with respect to $n$, that is $\lbrace \Delta^n_i p\rbrace$ is a triangular array of increments of $\logpricef{t}$. The assumptions used are very similar to the standard ones used in the literature. \Titledref{ass:HL} is essentially \gentextcite{jacod2012discretization} Assumption H. The assumption on the jumps is slightly more general and more straightforward. 
 
\begin{assumption}[HL]
 \label{ass:HL} 
 \begin{enumerate}
  \item $\mu(t)$ is locally bounded. 
  \item $\diffrootvol{(t)}$ is c\`{a}dl\`{a}g (or c\`{a}gl\`{a}d). 
  \item $\jumprootvol{(t)}$ is c\`{a}dl\`{a}g (or c\`{a}gl\`{a}d). 
 \end{enumerate}

\end{assumption}

To reduce notation, I adopt the convention from the literature that $i \in \mathbb{Z}, i \leq 0 \implies \Delta^n_i p = 0$  to make processes well-defined over the entire line, not just where we estimate them. This convention sets the processes equal to zero outside of the relevant window.

\subsection{Instantaneous Volatility Estimators}\label{sec:estimating_local_vol}

In this section, I show that the diffusion volatility that I uses is consistent under the same assumptions I use elsewhere in the paper. 
The contribution here is admittedly small; other authors have results under almost identical assumptions.
See, for example, \textcite{jacod2013quarticity}.
I include these results for rhetorical consistency only; having a single set of assumptions is useful.

The intuition behind the estimators' convergence is that we are averaging the volatilities over shrinking intervals that approach $\tau$ from the left so that we estimate the volatilizes' left limit.  Let $k_n$ denote to the number of terms we averaging, $I(i,n) \coloneqq [(i - k_n - 1) \Delta^n, (i - 1) \Delta^n]$, and $\Delta^n_{i_n} p$ denote the change in $p$ in $I(i,n)$. If we choose a sequence $i \to \tau$, the interval approaches $\tau$ from the left. Also, as $\logpricef{t}$ is one-dimensional, the driving Wiener and variance-gamma processes can be assumed to be one-dimensional without loss of generality. 


\begin{theorem}[Estimating the Instantaneous Diffusion Volatility]
 \label{theorem:localDiffusionVol}
 
 Let $\logpricef{t}$ be an \Ito\ semimartingale with interval support satisfying \titledrefsthree{ass:HL}{ass:I}{ass:SQ}. Let $k_n, \Delta^n$ satisfy $k_n \to \infty$ and $k_n \sqrt{\Delta_n} \to 0$, and let $0 < \tau < \infty$ be a deterministic time. Define $i_n = i - k_n -1$. Let $c_1 (\Delta^n)^{1/4} < v_1^n < c_2 \sqrt{\Delta^n}$ for some constants $c_1, c_2$ and $v^n_2 \to 1$. Then
% 
 \begin{equation*}
    \widehat{\diffrootvol{}}^2_{i_n}\left(k_n, \tau-, p\right) \coloneqq \frac{1}{k_n \Delta^n} \sum_{m=0}^{k_n-1} v_2^n \abs*{\Delta^n_{i_n, m} p}^2 1\lbrace \abs*{\Delta^n_{i_n, m} p} \leq v^n_1 \rbrace \pto \diffvolf{\tau-}.
 \end{equation*}
 
\end{theorem}

One might think we could use an analogous strategy to estimate $\jumpvolf{t}$, i.e., form an estimator of $\predQV*{\jump{p}}(t)$ by truncating away the small increments and take the time derivative of the resulting object. In fact, \textcite[256]{jacod2012discretization} show that this estimator converges to zero in their proof of the validity of their estimator for $\diffvolf{t}$. By considering a specific time $\tau$, we implicitly condition on $\tau$. Doing this reduces the variation in the locations, and shrinking the window eliminates variation from large jumps. If we also truncate away variation arising from the small jumps, we have no variation left to identify the jump volatility with.

Over a fixed interval, the quadratic variation of jump processes and diffusive processes are of the same asymptotic order as we shrink $\Delta^n_{i_n}$, \parencite{jacod2010limit}. If we consider shrinking intervals, this is no longer the case. Instead, it is the absolute value of the stochastic volatility variance-gamma and diffusion processes that have similar asymptotic properties. The absolute value of a standard variance-gamma process, $\abs{\Lap}(t)$, is a well-behaved object, just like the absolute value of a Wiener process, $\abs{W}(t)$, and they vanish at the same asymptotic rate: $\sqrt{\Delta}$.\footnote{Note, neither of the processes is a martingale. They are semimartingales.} Consequently, the $\lim_{\Delta \to 0} \abs*{\Delta^n_{i_n} \logpricef{t}}$ contains both $\jumpvolf{\tau}$ and $\diffvolf{\tau}$.\footnote{It is worth noting that this estimator is for the instantaneous absolute value of the martingale part, it will not pick up non-martingale parts of the jump process.}

\begin{proposition}[Estimating the Instantaneous Absolute Volatility]
 \label{thm:localabsvol}
 
  Let $\logpricef{t}$ be an \Ito\ semimartingale with interval support satisfying \titledrefsthree{ass:HL}{ass:I}{ass:SQ}. Let $k_n, \Delta^n$ satisfy $k_n \to \infty$ and $k_n \sqrt{\Delta^n} \to 0$, and let $0 < \tau < \infty$ be a deterministic time. Define $i_n \coloneqq i - k_n -1$. 

  Then the following holds, where $\erfcx \coloneqq \frac{2 \exp(x^2)}{\sqrt{\pi}} \int_{x}^{\infty} \exp(-s^2) \dif s$:\footnote{This function, $\erfcx$, is the scaled complementary error function. It is a reparameterization of Mill's ratio. Most scientific programming suites provide efficient, numerically-stable implementations.}

  \begin{equation*} 
    \frac{1}{k_n \sqrt{\Delta^n}} \sum_{m=0}^{k_n-1} \abs{\Delta^n_{i_n+m} p } \pto \E\abs*{\N(0,1)} \diffrootvol{(\tau-)} + \frac{\jumprootvol{(\tau-)}}{\sqrt{2}} \erfcx\left(\frac{\diffrootvol{(\tau-)}}{\jumprootvol{(\tau-)}}\right). 
 \end{equation*}
 
\end{proposition}

As long as the volatilities are locally constant around $\tau$, the implied parametric form gives a limiting value for the absolute value as a function of $\diffvolf{\tau}$ and $\jumpvolf{\tau}$. The expression on the right of the equation in \cref{them:localjumpvolt} is the mean of the convolution of $\abs{\N\left(0, \diffvol{}(\tau-)\right)}$ and $\abs{\Lap\left(0, \jumpvol{}(\tau-)\right)}$.

We combine this convolution and $\diffvolf{\tau}$ to estimate $\jumpvolf{\tau}$. To do this, we must weight the difference between the absolute population moment as a function of $\jumprootvol{(\tau)}$ and the absolute sample moment.\footnote{ Note, $\abs{p(t_1) - p(t_2)} \neq \sum_m \abs{\Delta^n p_m}$, and so there is no reason to think that the limits should coincide as you take averages over a shrinking interval.  In particular, because of the scaling by $\sqrt{\Delta^n}$ not $\Delta^n$ if we summed the estimator below the result would diverge over a fixed interval and if we averaged the result we would not get the estimator of \textcite{todorov2011limit} and so there is no reason for the limits to coincide.}

\begin{theorem}[Estimating the Instantaneous Jump Volatility]

  \label{them:localjumpvolt} 
  Let $\logpricef{t}$ be an \Ito\ semimartingale with interval support satisfying \titledrefsthree{ass:HL}{ass:I}{ass:SQ}. Let $k_n, \Delta^n$ satisfy $k_n \to \infty$ and $k_n \sqrt{\Delta^n} \to 0$, and let $0 < \tau < \infty$ be a deterministic time. Define $i_n = i - k_n -1$. Let $\widehat{\diffrootvol{}}_n(\tau-)$ converge in probability to $\diffrootvol{(\tau-)}$. Let $\jumprootvol{(\tau)} > 0$ and $g$ be strictly-increasing, convex, and continuous, then the following holds:
%
  \begin{align*}
    \widehat{\jumprootvol{}}(k_n, \tau-, p) &\coloneqq \argmin\limits_{\gamma} g\left(\abs*{ \frac{1}{k_n \sqrt{\Delta^n}} \sum_{m=0}^{k_n-1} \abs*{\Delta^n_{i_n +m} p} - \E\abs*{\N(0,1)} \widehat{\diffrootvol{}}_n(\tau-) 
% 
    - \frac{\gamma \erfcx\left(\frac{\widehat{\diffrootvol{}}_n(\tau-)}{\gamma}\right)}{2}}\right) \\
%    
    &\quad \pto \jumprootvol{(\tau-)}. 
  \end{align*}
\end{theorem}

\subsection{Implementation}\label{sec:implementation}

We now have estimators for both the instantaneous and integrated volatilities. The difficult part is estimating the instantaneous volatilities. The integrated volatilities are their averages. In practice, two issues affect the analysis. First, we must remove market microstructure noise. To do this, I adopt the pre-averaging approach argued for in \textcite[Eqn.\@ (3.9)]{podolskij2009bipower}.\footnote{Proving my estimators are still consistent in the pre-averaging case is an interesting avenue for future research.} Define the function: $g(x) \coloneqq (1 - (2 x - 1)^2 \boldOne \lbrace x >= 0 \rbrace \boldOne \lbrace x <= 1\rbrace$. The pre-averaged data is the rolling average of the true data: $\bar{p}_{i_n} \coloneqq \frac{1}{\kappa_n \sqrt{\int_0^1 g^2(s) \dif s}} \sum_{m=1}^{\kappa_n-1} g(\frac{m -1}{ \kappa_n}) \Delta_{i_n + m}^n p$. The $g$ function corrects for the error introduced by the pre-averaging.

If $\kappa_n \propto 1 / \sqrt{\Delta^n}$, we likely achieve the optimal rate in the presence of noise, but the noise leads to an asymptotic bias in most cases, \parencite{jacod2010limit}. To avoid this, I set $\kappa_n = \lfloor \frac{\theta}{(\Delta^n)^{0.55}} \rfloor$. This rate is useful because we can apply the estimators directly to the pre-averaged data, and it is not obvious exactly what bias exists when estimating the instantaneous absolute variation.\footnote{The transformation creating $\bar{p}_{i_n}$ does not affect the volatilities but does affect the mean.} I set $\theta = 0.5$, which both is recommend by \textcite{hautsch2013preaveraging} and works well in my simulations. 

I apply \cref{theorem:localDiffusionVol} to estimate $\diffvolf{\tau-}$. To do this, we must choose $v_2^n$ to converge to $1$; I let $v_2^n = 1$. More importantly, I must choose the truncation threshold $v_1^n$. We need $v_1^n$ to asymptotically upper bound the diffusion part. In the literature, papers usually set $v_1^n = c \widetilde{\diffrootvol{}}(\tau-) \Delta_n^{0.49}$, where $\widetilde{\diffrootvol{}}(\tau-)$ is a preliminary estimator for $\diffrootvol{}$ and $c$ is a number of standard deviations chosen by the econometrician. Since the tails of Laplace and Gaussian variables both decline rapidly, solving this deconvolution problem is quite  difficult, requiring a tight bound. The law of the iterated logarithm tightly bounds the deviations of a Gaussian variable, and so I use $v_1^n = \sqrt{2} \widetilde{\diffrootvol{}}(\tau-) \sqrt{\Delta_n} \sqrt{\log(\log(1 / \Delta_n))}$. 

The tails of the Laplace and Gaussian random variables both decline rapidly. The Gaussian density's tails are proportional to $\exp(-x^2/2)$, while the Laplace density's tails are proportional to $\exp(-x/\sqrt{2})$. Distinguishing these two is quite difficult in practice. Setting $v_1^n \propto \Delta^{.49}$, does not work particularly well in this scenario as I show in \cref{sec:estimator_performance}. This bound, although valid, is not particularly tight, and that matters when there are a lot of jumps.  

To form a preliminary estimator, I start with the $1.25$ times bipower variation and then iterate until convergence. We must start by overestimating the volatility to avoid incorrectly setting $\widehat{\diffrootvol{}}(\tau-) = 0$ because that truncates away all the increments.  I choose $k_n$, which controls the length of the interval over which the volatilities are treated as approximately constant, to equal $\bar{\kappa} + (\Delta^n)^{1/4}$ because that works well in simulations with market microstructure. 

Now that we can estimate $\diffvolf{\tau-}$, we need an estimator for the local absolute value. I plug the pre-averaged data into \cref{them:localjumpvolt}. It is worth noting that the theory I develop is for the no-noise case; the particular implementation likely is not affected by the noise, but I have not proven that. An appealing avenue for future research would be extending these results to cover the noise case. 

\subsection{Integrated Volatilities}\label{sec:integrated_vol_estimators}

We want to estimate discrete increments of the volatilities. To do this, I average the instantaneous estimators each day. The diffusion estimator defined this way coincides with standard diffusion estimators in the literature up to edge effects. 

\begin{remark}[Consistency of the Integrated Estimators]
  \label{thm:consistency_integrated_estimators}
 Let $\logpricef{t}$ be an \Ito\ semimartingale with interval support satisfying \titledrefsthree{ass:HL}{ass:I}{ass:SQ}. Let $k_n, \Delta^n$ satisfy $k_n \to \infty$ and $k_n \sqrt{\Delta_n} \to 0$. Define $i_n = i - k_n -1$. Then
% 
 \begin{equation} \label{eqn:int_diff_vol_est}
   \widehat{\diffrootvol{}}^2_t \coloneqq \frac{1}{\# t_n \in [t-1,t]} \sum_{t-1 < t_n \leq t} \widehat{\diffrootvol{}}^2(k_n, t_n, p) \pto \int_{t-1}^t \diffvolf{s} \dif s,
 \end{equation}
%
 and
%
 \begin{equation} \label{eqn:int_jump_vol_est}
   \widehat{\jumprootvol{}}^2_t \coloneqq \frac{1}{\# t_n \in [t-1,t]} \sum_{t-1 < t_n \leq t} \widehat{\jumprootvol{}}^2(k_n, t_n, p) \pto \int_{t-1}^t \jumpvolf{s} \dif s.
 \end{equation}
\end{remark}

\begin{proof}
The integrated estimators are averages of consistent estimators of $\diffvolf{t}$ and $\jumpvolf{t}$. Averages of consistent estimators are consistent by the law of iterated expectations, Jensen's inequality applied to the square, and Chebyshev's theorem.
\end{proof} 

Implementing the discrete volatility estimators is straightforward; we can take daily averages of the instantaneous volatilities. To estimate the realized density, plug the daily estimates into \cref{eqn:realized_density_rep}. Since this function is uniformly continuous given a lower bound on the volatilities, the resulting estimators should work well. Conversely, we cannot estimate the dynamics of the volatilities without time-series variation.


