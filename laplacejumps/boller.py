import numpy as np
import pandas as pd
import statsmodels.api as sm
from statsmodels.base.model import GenericLikelihoodModel, GenericLikelihoodModelResults
from scipy.special import kn
from scipy import stats
from contextlib import redirect_stderr


def calculate_h_series(deviations, root_omega, root_beta, root_loading):

    h_series = [np.mean(deviations**2)]
    for dev in deviations:
        h_series.append(root_omega**2 + root_loading**2 * dev**2 + root_beta**2 * h_series[-1])
    assert np.amin(h_series) > 0

    return np.ravel(h_series)


class GarchBP(GenericLikelihoodModel):
    def __init__(self, data, lags, **kwds):

        self.data_in = data
        endog = data['bipower_vol']
        exog = sm.add_constant(pd.concat([data['bipower_vol'].shift(1).rolling(lag).mean() for lag in lags] +
                                         [data['excess_rtn'].shift(1)], axis=1).dropna())
        exog.columns = ['const'] + [f"bipower_vol_{lag}" for lag in lags] + ['excess_rtn']

        endog = endog.loc[exog.index]
        self.regressand_names = endog.name
        self.regressor_names = exog.columns
        self.root_real_vol = (.5 * data['real_vol']).apply(np.exp).loc[endog.index]
        self.lags = lags

        super().__init__(endog, exog, **kwds)

    @property
    def parameter_names(self):
        return ['alpha0', 'alpha1', 'alpha2', 'alpha3', 'theta1', 'theta2', 'theta3', 'root_omega',
                'root_loading', 'root_beta', 'df', 'root_scale']

    @staticmethod
    def prediction(bipower_regressors, std_excess_rtn, alpha, theta):

        standardized_excess_rtn = np.ravel(std_excess_rtn)

        pred_log_bipower1 = np.asarray(bipower_regressors).dot(alpha)
        pred_log_bipower2 = theta.dot([standardized_excess_rtn, (standardized_excess_rtn < 0),
                                       standardized_excess_rtn * (standardized_excess_rtn < 0)])

        prediction = pred_log_bipower1 - pred_log_bipower2
        return prediction

    def loglike(self, params):
        alpha = np.ravel(params[:4])
        theta = np.ravel(params[4:7])
        root_omega = params[7]
        root_loading = params[8]
        root_beta = params[9]
        nu = params[10]
        root_scale = params[11]
        x_rtn_index = self.regressor_names.get_loc('excess_rtn')

        bipower_deviations = self.endog - self.prediction(self.exog[:, :4], self.exog[:, x_rtn_index] /
                                                          self.root_real_vol, alpha, theta)

        h_series = calculate_h_series(bipower_deviations[:-1], root_omega, root_beta, root_loading)

        returnval = (np.sum(self.log_normal_mixture(bipower_deviations / np.sqrt(h_series),
                                                    nu=nu, root_scale=root_scale)) -
                     .5 * np.sum(np.log(h_series)))

        return returnval

    @staticmethod
    def log_normal_mixture(x, nu=0, root_scale=1):

        with np.errstate(under='ignore', divide='ignore'):
            #             val = np.log(np.exp(np.log(1-prob) + stats.norm.logpdf(x))
            #                       + np.exp(np.log(prob) + stats.norm.logpdf(x, loc=mu, scale=scale)))
            val = stats.t.logpdf(x, scale=root_scale**2, df=nu)
        return val

    def fit(self, start_params=None, method='nm', maxiter=1500, full_output=1,
            disp=False, callback=None, **kwargs):

        if start_params is None:
            alpha = list(sm.OLS(endog=self.endog, exog=self.exog[:, :4]).fit().params)
            theta = [0, -.1, 0]
            # root_omega = [0]
            root_loading = [1]
            root_beta = [1]
            df = [.1]
            mu = [1]
            sigma = [.05]
            start_params = alpha.copy() + theta + root_beta + root_loading + df + mu + sigma
        fit_method = super().fit
        mlefit = fit_method(start_params=start_params, method=method, maxiter=maxiter, full_output=full_output,
                            disp=disp, callback=callback, **kwargs)
        genericmlefit = GarchBPResults(self, mlefit)

        return genericmlefit


class GarchBPResults(GenericLikelihoodModelResults):

    def __init__(self, *args, **kwargs):

        super().__init__(*args, **kwargs)
        self.root_real_vol = (.5 * self.data_in['real_vol']).apply(np.exp)

    @property
    def data_in(self):
        return self.model.data_in

    @property
    def regressor_names(self):
        return self.model.regressor_names

    @property
    def alpha(self):
        return np.ravel([self.params_dict[name] for name in self.params_dict.keys() if 'alpha' in name])

    @property
    def theta(self):
        return np.ravel([self.params_dict[name] for name in self.params_dict.keys() if 'theta' in name])

    def innovations(self, size=1):
        innovations = stats.t.rvs(size=size, scale=self.params_dict['root_scale']**2, df=self.params_dict['df'])
        return innovations
#         pvals = [1-self.params_dict['prob'], self.params_dict['prob']]
#         components = np.random.multinomial(n=size, pvals=pvals)
#         standard_rvs = stats.norm.rvs(size=components[0])
#         other_rvs = stats.norm.rvs(loc=self.params_dict['mu'], scale=self.params_dict['sigma'],
#                                   size=components[1])
#         innovations = np.concatenate([standard_rvs, other_rvs])
#         np.random.shuffle(innovations)
        return innovations

    @property
    def h_series(self):
        x_rtn_index = self.regressor_names.get_loc('excess_rtn')

        pred = self.model.prediction(self.exog[:, :4], self.exog[:, x_rtn_index] / self.model.root_real_vol,
                                     self.alpha, self.theta)
        bipower_deviation = (self.endog - pred)

        h_series = calculate_h_series(bipower_deviation, self.params_dict['root_omega'],
                                      self.params_dict['root_beta'], self.params_dict['root_loading'])[:-1]

        return h_series, bipower_deviation

    @property
    def params_dict(self):
        return {key: val for key, val in zip(self.model.parameter_names, self.params)}

    def density_predict(self, forecast_dim=1):

        last_excess_rtn = self.data_in.loc[:, 'excess_rtn'].iloc[-1]
        root_real_vol = self.root_real_vol.iloc[-1]

        bi_exog = sm.add_constant(pd.concat([self.data_in['bipower_vol'].shift().rolling(lag).mean() for lag
                                             in self.model.lags], axis=1)).dropna().iloc[-1]

        mean = self.model.prediction(bi_exog, last_excess_rtn / root_real_vol, self.alpha, self.theta)

        h_series, deviations = self.h_series
        next_h = (self.params_dict['root_omega']**2 + abs(self.params_dict['root_loading'])**2 *
                  deviations[-1]**2 + abs(self.params_dict['root_beta'])**2 * h_series[-1])

        innovation = self.innovations(size=forecast_dim)

        return pd.DataFrame(mean + np.sqrt(next_h) * innovation).T


class LogBPJumps(GenericLikelihoodModel):

    def __init__(self, data, lags, **kwds):
        endog = data.loc[:, 'bi_jumps'].to_frame()
        first_regressors = sm.add_constant(pd.concat([data.loc[:, 'bi_jumps'].shift(nlag)
                                                      for nlag in lags], axis=1))
        first_regressors.columns = ['const'] + [f'bi_jumps.shift_{nlag}' for nlag in lags]
        exog = pd.concat([first_regressors, data.loc[:, 'excess_rtn'].shift(1)], axis=1).dropna()
        endog = endog.loc[exog.index]
        self.regressand_names = endog.columns
        self.regressor_names = exog.columns
        self.root_real_vol = (.5 * data['real_vol']).apply(np.exp).loc[endog.index]
        super().__init__(endog, exog, **kwds)
        self.data_in = data
        self.lags = lags

    @property
    def parameter_names(self):
        return ['delta0', 'delta1', 'delta2', 'psi1', 'psi2', 'psi3', 'sigma']

    @staticmethod
    def mean(delta_regressors, standard_x_rtn, delta, psi):

        part1 = delta_regressors.dot(delta)
        part2 = psi.dot([standard_x_rtn, (standard_x_rtn < 0), standard_x_rtn * (standard_x_rtn < 0)])

        return part1 + part2

    def loglike(self, params):
        delta = np.ravel(params[:3])
        psi = np.ravel(params[3:6])
        sigma = np.ravel(params[6])
#         prob = np.ravel(params[7])
#         alpha = np.ravel(params[8])
#         beta = np.ravel(params[9])
#         mu_ig = np.ravel(params[10])
#         lambda_ig = np.ravel(params[11])

        x_rtn_index = self.regressor_names.get_loc('excess_rtn')
        mean = self.mean(self.exog[:, :3], self.exog[:, x_rtn_index] / self.root_real_vol, delta, psi)

        bi_jump_dev = (pd.Series(self.endog) - mean)

#         return np.sum(self.ig_nig_mixture(bi_jump_dev, prob=special.expit(prob), beta=special.expit(beta) * alpha**2,
#                                           scale_nig=sigma, alpha=alpha**2, lambda_ig=lambda_ig, mu_ig=mu_ig))
        return np.sum(stats.norm.logpdf(bi_jump_dev, scale=sigma))

#     @staticmethod
#     def ig_nig_mixture(x, prob=.1, alpha=1, beta=0, scale_nig=1, lambda_ig=.5, mu_ig=1):

#         part1 = normalinvgauss.pdf(x, mu=0, alpha=alpha, beta=beta, delta=scale_nig)
#         part2 = stats.invgauss.pdf(x, mu=mu_ig, scale=lambda_ig)

#         return np.log((1-prob)*part1 + prob*part2)

    def fit(self, start_params=None, method='nm', maxiter=1500, full_output=1, disp=False, callback=None, **kwargs):

        if start_params is None:
            delta = list(sm.OLS(endog=self.endog, exog=self.exog[:, :3]).fit().params)
            psi = [-.1, 0, 0]
            sigma = [.05]
#             prob=[.5]
#             alpha=[5]
#             beta = [.5]
#             mu_ig = [.2]
#             lambda_ig = [1]

            start_params = delta + psi + sigma  # + prob + alpha + beta + mu_ig + lambda_ig

        fit_method = super().fit
        mlefit = fit_method(start_params=start_params,
                            method=method, maxiter=maxiter,
                            full_output=full_output,
                            disp=disp, callback=callback, **kwargs)
        genericmlefit = LogBPResults(self, mlefit)

        return genericmlefit


class LogBPResults(GenericLikelihoodModelResults):

    def __init__(self, *args, **kwargs):

        super().__init__(*args, **kwargs)
        self.root_real_vol = (.5 * self.data_in['real_vol']).apply(np.exp)

    @property
    def data_in(self):
        return self.model.data_in

    @property
    def regressor_names(self):
        return self.model.regressor_names

    @property
    def delta(self):
        return np.ravel([self.params_dict[key] for key in self.params_dict.keys() if 'delta' in key])

    @property
    def psi(self):
        return np.ravel([self.params_dict[key] for key in self.params_dict.keys() if 'psi' in key])

    @property
    def params_dict(self):
        return {key: val for key, val in zip(self.model.parameter_names, self.params)}

    def density_predict(self, forecast_dim=1):
        bi_exog = sm.add_constant(pd.concat([self.data_in.loc[:, 'bi_jumps'].shift(-1).shift(nlag)
                                             for nlag in self.model.lags], axis=1)).iloc[-1]
        root_real_vol = (.5 * self.data_in['real_vol']).apply(np.exp).iloc[-1]
        excess_rtn = self.data_in.loc[:, 'excess_rtn'].iloc[-1]

        mean = self.model.mean(bi_exog, excess_rtn / root_real_vol, self.delta, self.psi)

        innovations = stats.norm.rvs(size=forecast_dim, scale=self.params_dict['sigma'])

        return pd.DataFrame(mean + innovations).T


class ReturnRV(GenericLikelihoodModel):

    def __init__(self, data, lags, **kwds):

        endog = data['excess_rtn']
        exog = sm.add_constant(pd.concat([data['excess_rtn'].shift(n) for n in lags], axis=1).dropna())
        self.weights = (-data['real_vol']).apply(np.exp).loc[exog.index]
        self.data_in = data.copy()
        super().__init__(endog=endog.loc[exog.index], exog=exog, **kwds)
        self.lags = lags

    def loglike(self, params):

        beta = params[:-1]
        scale = params[-1]

        mean = np.asarray(self.exog) @ np.ravel(beta)

        standardized_resid = (self.endog - mean) * np.sqrt(np.ravel(self.weights))

        return np.sum(stats.norm.logpdf(standardized_resid, scale=scale)) + .5 * np.sum(np.log(self.weights))

    def fit(self, start_params=None, method='nm', maxiter=1500, full_output=1, disp=False, callback=None,
            **kwargs):

        if start_params is None:
            initial_model = sm.WLS(endog=self.endog, exog=self.exog, weights=self.weights).fit()
            beta = list(np.ravel(initial_model.params))
            sigma = [initial_model.scale]

            start_params = beta + sigma

        fit_method = super().fit
        mlefit = fit_method(start_params=start_params,
                            method=method, maxiter=maxiter,
                            full_output=full_output,
                            disp=disp, callback=callback, **kwargs)
        genericmlefit = ReturnRVResults(self, mlefit)

        return genericmlefit


class ReturnRVResults(GenericLikelihoodModelResults):

    @property
    def data_in(self):
        return self.model.data_in

    def density_predict(self, forecast_dim=1, real_vol=1):

        beta = self.params[:-1]
        scale = self.params[-1]
        exog = np.ravel(sm.add_constant(pd.concat([self.data_in['excess_rtn'].shift(-1).shift(n)
                                                   for n in self.model.lags], axis=1).dropna()).iloc[-1])
        return pd.DataFrame(beta @ exog +
                            np.sqrt(real_vol) * stats.norm.rvs(size=forecast_dim, scale=scale)).T


class CombinedModelResults:

    def __init__(self, jump_results, diffusion_results, return_results):
        self.jump_results = jump_results
        self.diffusion_results = diffusion_results
        self.return_results = return_results

    def density_predict(self, forecast_dim=1, H=None):

        bipower = self.diffusion_results.density_predict(forecast_dim=forecast_dim)
        jumps = self.jump_results.density_predict(forecast_dim=forecast_dim)

        real_vol = np.exp(.5 * (bipower + jumps))

        returnval = pd.DataFrame(np.ravel(self.return_results.density_predict(forecast_dim=forecast_dim,
                                                                              real_vol=real_vol))).T

        return returnval


class CombinedModel:

    def __init__(self, data, diff_lags, jump_lags, rtn_lags):

        self.diffusion_model = GarchBP(data, diff_lags)
        self.jumps_model = LogBPJumps(data, jump_lags)
        self.return_model = ReturnRV(data, rtn_lags)

    def fit(self):

        result = CombinedModelResults(self.jumps_model.fit(), self.diffusion_model.fit(maxiter=5000),
                                      self.return_model.fit())
        return result


def estimate_boller(x, diff_lags, jump_lags, rtn_lags):
    """
    Computes estimates of the model and returns them and a string that tless you what values we used to estimate
    the mode.

    Paramters
    -------
    x : tuple
        The paramters needed to estimate the model.
    Returns
    -----
    string
    CombinedModelResults
    """
    vol_data = x

    with open('../logging2.tmp.txt', mode='w') as file:
        with redirect_stderr(file):
            model = CombinedModel(vol_data, diff_lags=diff_lags, jump_lags=jump_lags, rtn_lags=rtn_lags).fit()

    key = "/".join(['Log Volatility', 'BP-J-\\rxt', '\\rxt', vol_data.index[-1].strftime('%Y-%m-%d')])

    return key, model


class normalinvgauss(stats.rv_continuous):
    from scipy.special import kn
    """ A Normal-Inverse Gaussian Distribution """

    def pdf(x, mu=0, alpha=1, beta=0, delta=1):

        assert abs(beta) < alpha
        x = np.ravel(x)
        root_arg = np.sqrt(delta**2 + (x - mu)**2)

        val1 = alpha * delta * kn(1, alpha * root_arg) / (np.pi * root_arg)
        val2 = np.exp(delta * (alpha**2 - beta**2) + beta * (x - mu))

        return val1 * val2

    def rvs(size=1, mu=0, alpha=1, beta=0, delta=1):
        assert abs(beta) < alpha

        invgauss_rvs = stats.invgauss.rvs(size=size, mu=delta, scale=np.sqrt(alpha**2 - beta**2))
        normal_rvs = stats.norm.rvs(size=size)

        returnvals = mu + beta * invgauss_rvs + invgauss_rvs**.5 * normal_rvs
        return returnvals
