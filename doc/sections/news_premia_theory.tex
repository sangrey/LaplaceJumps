\section{News Premia: Theory} \label{sec:news_premia_theory}


Discontinuous prices or information flows that this paper considers break the derivation of CAPM-style results where risk-premia are instantaneous covariances with marginal utility.  In particular, risk premia are no longer proportional to the integrated diffusive covariation between prices and stochastic discount factors.  For example, in \gentextcite{ai2018risk} world, we have an announcement SDF (A-SDF) whose covariation with returns is also priced, while in \gentextcite{tsai2018pricing} world, it is the covariation between the SDF and returns during extreme events that matters.  This section unifies these two theories by decomposing the covariation between the prices and the investor's pricing kernels into their predictable and unpredictable components.  In particular, it shows that risk premia have two components in the general case.  Contrary to the discussion in \textcite[31]{tsai2018pricing}, both of these terms are, appropriately-defined, covariances, and so we would expect returns to exhibit a factor structure. 

\subsection{Preferences}\label{sec:investor_preferences}

To avoid introducing more notation than necessary, I characterize the investor's decision problem over a short period $\Delta$ and take $\Delta \to 0$.  Following \textcite{ai2018risk}, I adopt the intertemporal preferences represented as in \textcite{strzalecki2013temporal}.  Let $V(t)$ be the representative investor's value function at time $t$, and $u(\cdot)$ be the associated flow utility over current period consumption --- $C(t)$.  Let $\kappa$ denote the rate of time-preference.  I assume that $\kappa$ is constant for notation convenience, but this can be easily generalized.

\begin{definition}[Certainty Equivalence Functional]
    \label{defn:cef_total}
    \begin{equation*}
        V(t) = \int_t^{t+\Delta} u\left(c\left(t\right)\right) \dif t + \I\left[\int_{s \geq t+\Delta} \exp\left(- \kappa \left(s - t\right)\right) u(C(s)) \dif s \mvert \F_{t-} \right]. 
    \end{equation*}
\end{definition}

I immediately specialize to the form given in \cref{defn:cef_total}, which is \parencite[observation ii, p. 1401]{ai2018risk}.  Most of the results still go through in the general case, albeit with a loss of interpretability.  In this case, investors preferences are represented as 
%
\begin{equation}
    \label{defn:certainty_equivalence_functional}
    \I\left[V(t) \mvert \F_{t-}\right] = \phi^{-1}\left(\E\left[\phi(V(t))\mvert \F_{t-} \right]\right), 
\end{equation}
%
for some strictly increasing function $\phi$. Preferences having this form include the recursive utility of \textcite{kreps1978temporal} and \textcite{epstein1989substitution} and the second-order expected utility of \textcite{ergin2009theory}.  If $\phi$ is the identity function, preferences are time-separable.  They also cleanly characterize the problem at hand. \Textcite{ai2018risk} show that these form of preferences lead to an announcement premium if and only if $\phi$ is concave.

I make filtration, $\F_{t}$, explicit in \cref{defn:certainty_equivalence_functional} to emphasize that certainty equivalence functionals map information sets to utility.  In particular, $\mathcal{I}$ is just the expectations operator, $\E\left[\cdot \mvert \F_{t-}\right]$, if preferences are time-separable.

Note, if $V(t)$ is continuous, it is predictable, i.e., $V(t) \in \F_{t-}$, then  
%
\begin{equation}
    \I\left[V(t) \mvert \F_{t-}\right] = \phi^{-1}\left(\E[\phi(V(t)) \mvert \F_{t-}\right) 
    = \phi^{-1}\left(\phi(V(t)) \E[1\mvert \F_{t-}\right) 
    = \phi^{-1}(\phi(V(t)) ) 
    = V(t).
\end{equation}
%
In other words, the recursive utilities can be appropriately reparameterized in terms of a time-separable preferences. Now, there is no reason to assume the reparameterization is particularly convenient for analysis, just that it exists. This validity of this reparameterization is why \textcite[Theorem 5]{tsai2018pricing} find a single risk price is sufficient even in the presence of recursive utility as long as the underlying shocks are
continuous.

To make this more concrete, consider the example of Epstein-Zin preferences.  I adopt the notation used in
\textcite{bansal2004risks}. Let $\rho$ denote risk aversion and $\psi$ denote to the intertemporal elasticity of
substitution (IES).  Then Epstein-Zin Utility can be represented as: 
%
\begin{equation}
    \label{eqn:epstien_zin}
    U_t = \left[C_t^{1 - 1 / \psi} + \exp(-\kappa \Delta) \E\left[U_{t+ \Delta}^{1-\rho}\mvert \F_{t}
    \right]^{\frac{1 - 1 / \psi}{1 - \rho}}\right]^{\frac{1}{1 - 1 / \psi}}  
\end{equation}

The formulation in \cref{eqn:epstien_zin} is not in the form given in \cref{defn:certainty_equivalence_functional},
and so is not particularly useful for our purposes.
Define $V_t \coloneqq U_{t}^{1 - 1 / \psi}$ and reparameterize \cref{eqn:epstien_zin} as 
%
\begin{equation}
    V_t = \left[C_t^{1 - 1/\psi} + \exp(-\kappa \Delta) \E\left[V_{t + \Delta}^{\frac{1 - \rho}{1-1 / \psi}}
    \mvert \F_{t} \right]^{\frac{1 - 1 / \psi}{1 - \rho}}\right].
\end{equation}
%
Let $\phi(V) \coloneqq \frac{1- \rho}{1 - 1 / \psi} V^{\frac{1-\rho}{1 - 1 / \psi}}$ and $U(C(t)) \coloneqq C_t^{1-1/\psi}$, then we have: \footnote{The constant in front cancels between $\phi$ and $\phi^{-1}$, and so does not affect the level of utility. It is there to ensure that $\phi$ is an increasing function regardless of the values of the parameters.} 
% 
\begin{equation}
    V_t = \left[u(C_t) + \exp(-\kappa \Delta) \phi^{-1} \left(\E\left[\phi\left(V_{t + \Delta}\right)
    \mvert \F_{t} \right]\right)\right].
\end{equation}


\subsection{The Investor's Portfolio Optimization Problem}\label{sec:investors_problem}

To fix intuition, consider a one-period version of the model.  The investor can continually trade between time $0$
and time $1$ and consumes her wealth at time $1$ as displayed in \cref{fig:timing}.  At some time $\tau \in (0,1)$,
a news item is released, on which the investor can trade.  The investor's preferences satisfy the following
utility recursion for any time $\tau > t$.
%
\begin{equation} 
    V_t(W_t) = u(C_t) + \phi^{-1}\left(\E\left[\phi\left(V_{\tau}(W_{\tau})\right) \mvert \F_{t-}\right]\right).  
\end{equation}

Assume that the investor has access to three assets.  1) A risk-less asset, $\chi_{f,t}$, that pays off \num{1} unit in every period, and whose price  equals \num{1} because investors do not discount the future.  Essentially, it is a costless storage technology.  2) An asset, $\zeta_t$, whose payout, $R_{\zeta}$, is announced by the news release.  3) An asset $\xi_t$ whose payout  $R_{\xi}$ realizes as a Brownian motion, i.e., its variance and mean are both proportional to the length of the remaining interval.  \Cref{fig:timing} displays the timing.  I maintain the convention where the time subscript refers to when the variable first enters the investor's information set.

\begin{figure}[htb]

    \caption{Timing}
    \label{fig:timing}

    \bigskip

    \centering
    \includegraphics[width=.75\textwidth]{timeline.pdf}
\end{figure}

Since this is a finite-horizon problem, we can solve it by working backward.  At time \num{1}, all uncertainty has
been resolved, and the representative agent eats all of her wealth:
%
\begin{equation}
    V_1(W_1) = u(W_1).
\end{equation}
%
Let $\tau < t < 1$, then the investor can trade $\xi_t$ and the risk-less asset $\chi_t$.  However, since the news was already released, we know the payout of $\zeta_t$, and so it is a risk-free asset.  Consequently, the value function satisfies
%
\begin{equation}
    V_{t}(W_{t}) = \max_{\xi} \phi^{-1}(\E\left[\phi\left(V_1\left(W_{1}\right)\right) \mvert
    \F_{t} \right]), \text{where}\ 
%
    W_{1} = W_{t} + (1 - t) (R_{\xi} - 1) \xi_{{t}},
\end{equation}
%
because she gets return \num{1} from the risk-less asset and $R_{\xi}$ from the risky asset over the course of the
entire interval.  By substituting the constraint into the problem, and noting that $V_1(W_1) = u(W_1)$, this
simplifies to
%
\begin{equation}
    \label{eqn:simplified_news_value_function}
    V_{t}(W_{t}) = \max_{\xi} \phi^{-1}\left(\E\left[\phi\left(u\left(W_{t} + (1- t) \xi_{t} (R_{\xi} -1)
    \right)\right) \mvert \F_{t} \right]\right).  
\end{equation}
%
The first-order condition is 
%
\begin{equation}
    \label{eqn:discrete_time_asset_pricing_eqn}
    0 = \E\left[\phi'(u(W_{1})) u'(W_{1})(1-t)(R_{\xi_t} - 1) \mvert \F_{t} \right],
\end{equation}
%
since the term arising from the derivative of $\phi^{-1}$ is always positive. 

The investor's problem for some time $t$ in $(0, \tau)$  has similar structure except now she trades both assets. 
%
\begin{gather}
    \label{eqn:consumption_problem}
    V_t(W_t) = \max_{\zeta_t,\xi_t } \phi^{-1}(\E\left[\phi(V_\tau(W_\tau)) \mvert \F_t\right])  \\
%
    W_\tau =  W_t + (R_{\zeta} -1) \zeta_t  + (\tau - t) (R_{\xi}-1) \xi_t
\end{gather}
%
Substituting the constraints into the problem gives
%
\begin{equation}
    \label{eqn:consump_value_func}
    V_t(W_t) = \max_{\zeta_t, \xi_t} \phi^{-1}\left(\E\left[\phi\left(V_{\tau}\left(W_t + (R_{\zeta} -1) \zeta_t + (\tau - t) (R_{\xi} - 1) \xi_t \right)\right) \mvert \F_t\right]\right).  
\end{equation}
%
Taking first-order conditions with respect to the $x$ for $x \in \lbrace \zeta, \xi \rbrace$ and simplifying gives 
%
\begin{equation}
    0 = \E\left[\phi'\left(V_{\tau}\left(W_{\tau}\right)\right)V_{\tau}'(W_{\tau}) \left(R_{x} - 1\right) \mvert \F_{t}\right].
\end{equation}

Consider some time immediately before $\tau$, $\tau-$, and some time right after $\tau$, $\tau+$.  Then, substitute \cref{eqn:discrete_time_asset_pricing_eqn} into \cref{eqn:consump_value_func} and consider the derivative with respect to $\xi_t$:\footnote{I am taking limits here with respect to time loosely here to provide intuition. I make the statements rigorous in the theorems below.}
%
\begin{equation}
    0 = \E\left[\E\left[\phi'\left(V_{\tau}\left(W_{\tau}\right)\right)V_{\tau}'(W_{\tau}) \mvert \F_{t+} \right]
    (\dif \xi) (R_{\xi} - 1) \mvert \F_{\tau-} \right].
\end{equation}

Since $\xi$  is continuous, it is orthogonal to all discontinuous process, and so we can replace the expectation with respect to $\tau^{+}$ with an expectation with respect to $\tau^{-}$:
%
\begin{equation}
    0 = \E\left[\E\left[\phi'\left(V_{\tau}\left(W_{\tau}\right)\right)V_{\tau}'(W_{\tau}) \mvert \F_{t-} \right]
        (R_{\xi} - 1) \mvert \F_{\tau-} \right]
\end{equation}
%
Consequently, the investor only cares about the predictable part of the co-variation. In order for the returns to have finite variances, this must be proportional to the length of the interval. Their variance is proportional to $(\tau^{+}) - (\tau^{-}) \approx 0$. Hence, the Brownian asset $\xi$ is risk-less over short enough intervals.  

I substitute \cref{eqn:discrete_time_asset_pricing_eqn} into \cref{eqn:consump_value_func}, and consider the derivative with respect to $\zeta_t$. The jump asset, $\xi_t$, is not risk-less over short enough intervals:
%
\begin{equation}
    0 = \E\left[\E\left[\phi'\left(V_{\tau}\left(W_{\tau}\right)\right)V_{\tau}'(W_{\tau})(R_{\zeta} - 1) (\dif
            \zeta) \mvert \F_{\tau-} \right] \mvert \F_{\tau-} \right].
\end{equation}
% 
We cannot pull $R_{\zeta}$ outside of the inner expectation because it is not predictable, i.e., it is not
contained in the $\F_{\tau-}$ information set.  Hence, there is no reason to expect the ex-post variation to be
proportional to the length of the interval.

To facilitate comparing the two equations, isolate the unpredictable variation in the SDF by multiplying and
dividing through by its left-limit, which we can pull through the inner expectation:  
%
\begin{equation}
    0 = \E\left[\phi'\left(V_{\tau-}\left(W_{\tau-}\right)\right)V_{\tau-}'(W_{\tau-})\E\left[\frac{
    \phi'\left(V_{\tau}\left(W_{\tau}\right)\right)V_{\tau}'(W_{\tau})}{
    \phi'\left(V_{\tau-}\left(W_{\tau-}\right)\right)V_{\tau}'(W_{\tau})}(R_{\zeta} - 1) (\dif \zeta) \mvert
    \F_{t-} \right] \mvert \F_{\tau-} \right] 
\end{equation}

Note, $\phi'(V_{\tau-}(W_{\tau-})) V_{\tau-}'(W_{\tau-})$ is predictable, while $\frac{ \phi'\left(V_{\tau}\left(W_{\tau}\right)\right)V_{\tau}'(W_{\tau})}{ \phi'\left(V_{\tau-}\left(W_{\tau-}\right)\right)V_{\tau}'(W_{\tau})}$ is purely unpredictable.

\subsection{Deriving the Asset-Pricing Equation}

The market environment is mostly standard. We need a series of technical conditions that ensure that preferences are reasonable and first-order conditions uniquely characterize the optimum.

\begin{assumption}{Market Environment}
    \label{ass:market_assumptions}
    \begin{enumerate}
        \item Both $u$ and $\phi$ are Lipschitz continuous with Lipschitz derivatives. 
%
        \item $u: \R \to \R$ has strictly positive  first-order derivatives, and $\phi$ is increasing with first- and second-order derivatives that are bounded away from zero and infinity. 
%
        \item A representative investor prices all assets.
%
        \item Consumption --- $C(t)$ --- is an \Ito\ semimartingale.
%
        \item All of the stochastic processes do not contain predictable jumps.
    \end{enumerate}
\end{assumption}

The fourth assumption is the principal distinction from the setup in \textcite{ai2018risk}.  \Cref{ass:market_assumptions} generalize their assumptions by allowing consumption to jump.  I will discuss later how my results slightly simplify if we require consumption to be continuous.  The third assumption is likely unnecessarily restrictive, most of the results in this section would go through in terms of a marginal investor's preferences.  I make this assumption to simplify the exposition.


Consider an representative investor with preferences given by \cref{defn:certainty_equivalence_functional}.  She has access to a (potentially infinite) vector of assets $\Xi(t) \coloneqq \xi_{1}(t), \ldots$.   Assume for simplicity that she has no other sources of income.  Over some small length of time $\Delta$, the investor's problem is as follows.  She enters into the period with asset allocation $\Xi(t-\Delta)$, and prices are $\logpricef{t}$.\footnote{The timing notation may seem somewhat strange here because it maintains the convention used elsewhere in the paper where time arguments denote when the objects first enter the representative investor's information set.} She need to solve for consumption $C(t)$ and an asset allocation $\Xi(t)$.  The results are reported cum-dividend to avoid introducing even more notation. The extension to the ex-dividend case is straightforward.\footnote{Cum-dividend means before dividend. Assets here behave like Bitcoin or gold and never pay out dividends.} 
 
\begin{problem}{Consumer's Portfolio Allocation}

    \label{defn:consumer_problem}

    \begin{gather*}
        V\left(\Xi(t-\Delta), P(t)\right)  = \max_{C(t),\, \Xi(t)} \int_{t}^{t+\Delta} u(C(s)) \dif s  + \phi^{-1}\left(\E\left[\frac{\phi\left(V\left(\Xi(t), P(t+\Delta)\right)\right)}{\exp\left(\kappa \Delta\right)} \mvert \F_{t}\right]\right) \\
%
        C(t) + \sum_{i} P_i(t) \xi_{i}(t-\Delta) = \sum_{i} P_i(t) \xi_{i}(t)
    \end{gather*}

\end{problem}


The continuous-time problem is the limit of \cref{defn:consumer_problem} as $\Delta$ approaches $0$.  The
trade-offs are slightly easier to see in the discrete-time problem.  The investor must purchase consumption,
$C(t)$, and assets, $\Xi(t)$, at prices, $P_{i}(t)$, using wealth, $\sum_{i} P_i(t) \Xi(t-\Delta)$.  Let
$\widetilde{P}(t) = \exp\left(-\kappa(t)\right) \logpricef{t}$ be the appropriately discounted price.  We are interested in
excess returns, not returns themselves.  Then we can derive the following result, where $\logpricef{t}$ refers to the
price.\footnote{This is a generalization of \textcite[Theorem 2]{ai2018risk} to allow for jumps in consumption.}


\begin{restatable}[Asset-Pricing Equation]{theorem}{assetPricingEqn}
    \label{thm:asset_pricing_eqn}
    Let \cref{ass:market_assumptions} hold, prices be \Ito\ semimartingales, and the representative consumer face
    \cref{defn:consumer_problem} as $\Delta \to 0$.
    Assume preferences are such that optimal consumption is strictly positive.
    Define 
%
    \begin{equation*}
        M^{UP}(t) \coloneqq \frac{\phi'(V\left(W(t)\right)}{\phi'(V\left(W(t-)\right)}\,
%
        \text{and}\
%
        M(t) \coloneqq \frac{\phi'(V\left(W(t-)\right) }{\phi'\left(\phi^{-1}\left(\E\left[\phi(V(W(t))) \mvert
            \F_{t-}\right]\right)\right)} \frac{V'\left(W(t)\right)}{u'(c(t-))}.    
    \end{equation*}
%
    Then $M^{UP}(t)$ is a purely discontinuous martingale, and for all stopping times $\tau > t$,\footnote{I use
    the $UP$ superscript because $M^{UP}$ is an unpredictable process.}
%
    \begin{equation*}
        \widetilde{P}(t)  = \E\left[M(\tau) M^{UP}(\tau) \widetilde{P}(\tau) \mvert \F_{t}\right]
    \end{equation*}
%
\end{restatable}

Conceptually, \cref{thm:asset_pricing_eqn} is straightforward.  
Prices are semimartingales, and so we have a pricing kernel --- $\mathcal{M}(t)$ --- that prices all assets: 
%
\begin{equation}
    \label{eqn:pricing_kernel}
    \widetilde{P}(t) = \E\left[\mathcal{M}(\tau) \widetilde{P}(\tau) \mvert \F_t\right].  
\end{equation}
%
However, $\mathcal{M}(t)\, \cancel{\propto}\, V'(W(t))$.
Instead, it has two parts: $M(t)$, which reflects compensation for consumption risk and $M^{UP}(t)$ which reflects
compensation for discontinuities in the investor's information set. 


\subsection{Deriving Risk Premia}\label{sec:risk_premia_derivation}

The end of the previous section is essentially where \textcite{ai2018risk} stop.  \Cref{sec:news_premia_empirics} estimates risk premia, and so I must derive risk premia from \cref{thm:asset_pricing_eqn}.  If prices were continuous, \Ito's formula lets us solve for the expected log-return in terms of the covariance between the $M(t)$ and $\logpricef{t}$.  However, the generalized \Ito's formula in the literature that applies to general semimartingale does not have a simple form in terms of covariances.  To resolve this impasse, I derive a generalized \Ito's formula in terms of predictable quadratic covariation, (integrated diffusive and jump volatilities) that has the standard form but applies to jump processes.  


\begin{restatable}[An \Ito's Formula for the Expectation of a Square Integrable
    Semimartingale]{lemma}{genItosLemma}

    \label{lemma:expectation_ito_formula}
    Let $f$ be a twice-differentiable function and $\widetilde{Z}$ be a vector-valued semimartingale with locally bounded predictable $\predQV{Z}(t)$.  Then the differential of $f$ satisfies 
%
    \begin{equation*}
        \dif \E\left[f(\widetilde{Z}) \mvert \F_{t-}\right] = \E\left[f'(\tilde{Z}(t-)) \dif \tilde{Z}(t) \mvert \F_{t-}\right] + \frac{1}{2} f''(\tilde{Z}(t-)) \dif \langle \tilde{Z} \rangle (t). 
    \end{equation*}

\end{restatable}
The assumptions and conclusion in \Cref{lemma:expectation_ito_formula}  are both weaker than \Ito's formula for continuous processes.  We do not need continuous processes, but the equality only holds in expectation.  However, this is sufficient for our purposes as risk premia are expectations.  Importantly, the convexity correction has the same form as it does in the standard \Ito's formula.  

I now compute risk premia by applying  \Cref{lemma:expectation_ito_formula} to the logarithm.  Let $m(t) \coloneqq \log(M(t))$ and $m^{UP}(t) \coloneqq \log(M^{UP}(t))$.  Recall that throughout, $\logpricef{t}$ refers the log-price.  Let $P_f$ denote the price of the risk-free asset.

\begin{restatable}[Asset-Pricing Equation]{theorem}{riskPremia}
    \label{thm:riskPremia}
    Let  the assumptions in \cref{ass:market_assumptions} hold, $P_i(t)$ be an \Ito\ semimartingales, and the representative consumer face \cref{defn:consumer_problem} as $\Delta \to 0$.  Assume that preferences are such that optimal consumption is strictly positive.  Then risk-premia for some asset $i$ is
%
    \begin{equation*}
        \E\left[\frac{\dif P_{i}(t)}{P_{i}(t-)} - \frac{\dif P_{f}(t)}{P_{f}(t-)} \mvert \F_{t-}\right]  = - \dif \predQV{m, \diff{p} + \jump{p}}(t) - \dif \predQV{m^{UP},\jump{p}}(t).  
    \end{equation*}
\end{restatable}

The cost of the assumptions' generality is that \cref{thm:riskPremia} is rather abstract. To make the representation more concrete, consider a few specializations.  First, assume that preferences are time-separable and consumption is continuous.  Then $M^J(t)$ is identically one, and $M(t) = \frac{u'(C(t))}{u'(C(t-))}$ by the envelop theorem.  Consequently, $ \E\left[\frac{\dif P_{i}(t)}{P_{i}(t-)} - \frac{\dif P_{f}(t)}{P_{f}(t-)} \mvert \F_{t-}\right]  = - \sigma_m' \sigma_{p_i}$. This is the consumption-CAPM model of \textcite{breeden1979intertemporal}.


In addition,  since $m(t)$ is continuous, which is implied by $u'(\cdot)$ being a smooth function and $C(t)$ being continuous, jumps do not command a premium.  This is because time-separability implies $m^{UP}(t)$ is identically zero and so $\dif \predQV{m, \diff{p} + \jump{p}}(t) + \dif \predQV{m^{UP},\jump{p}}(t) = \dif \predQV{m, \diff{p}}$.

If we allow for jumps and recursive utility, \cref{thm:riskPremia} generalizes \textcite[Theorem 5]{tsai2018pricing}.  The key difference is that it is apparent that the second term is a covariance.  Even in the presence of jumps, risk premia can be split into a risk price and risk quantity. It is not immediately apparent in  \gentextcite{tsai2018pricing} environment that their formula is a covariance, but as long as returns have finite variance, we can rewrite their expression as a predictable quadratic variation.\footnote{This is implied by \cref{lemma:expectation_ito_formula}.} 

If we are in a world like \textcite{ai2018risk}, where consumption is continuous and the envelop theorem holds
(which implies $V'(W(t))$ is a continuous process), the equation in \cref{thm:riskPremia} simplifies to 
%
\begin{equation}
    \E\left[\frac{\dif P_{i}(t)}{P_{i}(t-)} - \frac{\dif P_{f}(t)}{P_{f}(t-)} \mvert \F_{t-}\right]  = -
    \dif \predQV{m, \diff{p}}(t) - \dif \predQV{m^{UP},\jump{p}}(t).  
\end{equation}
%
If we further assume that high-frequency consumption movements can be ignored, i.e., $u'(c(t-)) = u'(C(t))$, we can combine the two terms using the law of iterated expectations. 
%
\begin{equation}
    \E\left[\frac{\dif P_{i}(t)}{P_{i}(t-)} - \frac{\dif P_{f}(t)}{P_{f}(t-)} \mvert \F_{t-}\right]  = -
    \dif \predQV*{\frac{\phi'(V(Wt))}{\phi'(V(W(t-)))}, \logpricef{t}}. 
\end{equation}
%
Again, we have a single risk-price and risk-premia equal  $-\sigma_{m}' \sqrt{(\totalvolt)_i}$.  Doing this is equivalent to assuming that the market wealth portfolio is the only risk factor and ignoring movements in the wealth-consumption ratio.  However, as will be shown below, the data require a two-factor model.

We could instead assume that consumption is continuous and the envelop theorem holds, but high-frequency movements in consumption cannot be ignored.  In that case, the risk premia equation is
%
\begin{equation}
    \E\left[\frac{\dif P_{i}(t)}{P_{i}(t-)} - \frac{\dif P_{f}(t)}{P_{f}(t-)} \mvert \F_{t-}\right]  = -
    \dif \predQV{m, \diff{p}}(t) - \dif \predQV{m^{UP},\jump{p}}(t).  
\end{equation}
%
In this case, we could isolate the effects of each of the two factors by estimating the risk-premia as a bivariate function of $\diffvolt$ and $\jumpvolt$.  In that case, we could view the second term as a measure of announcement premia, similar  to how \textcite{ai2018risk} use the excess returns on FOMC days.  However, then the regressions done below imply that $\phi$ is convex because $\jumpvolt$ predicts lower risk premia once we condition in $\diffvolt$.  Regardless, the data demand we have two factors that move at high-frequency.  They also require the news risk premium to be less than the diffusion volatility premium.

