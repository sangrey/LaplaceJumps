
\section{Data Generating Process}\label{sec:data_generating_process}

Models of prices differ along two different dimensions. They can be either continuous or discrete, and they can be either in continuous-time or in discrete-time. I write down a continuous-time DGP with jumps and derive the implied discrete-time representation. I also discuss the purely continuous special case that my DGP nests to provide a point of comparison. 

\subsection{Continuous-Time DGP}

We know from \textcites{dambis1965decomposition, dubins1965continuous} that continuous \Ito\ semimartingales are stochastic volatility diffusions. That is, for some drift, $\mu(t)$, and diffusion volatility, $\diffvolf{t}$, we can represent the log-price process as $ \logpricef[\dif]{t} = \mu(t) \dif t + \diffrootvol{(t)} \dif W(t)$, where $W(t)$ is a Wiener process. 

The standard nonparametric way to add jumps to these models is to assume that prices are \Ito\ semimartingales. This representation is quite general because it only requires that prices are semimartingales and each of the components of the process have time-derivatives. The log-price being an \Ito\ semimartingale implies that the jump part is an integral with respect to a Poisson random measure. Let $n$ be a Poisson random measure with associated compensator, $\nu$. The function $\delta(s,x)$ controls the magnitude of the process. In general, the triple $(\delta, n, \nu)$ is not unique, which allows us to pick a particularly useful representation later.


\begin{definition}{Jump-Diffusion DGP (Grigelionis Form of an \Ito\ Semimartingale)}
  \label{defn:jump_diffusion_proces}
  \begin{alignat*}{2}
    &\logpricef{t} &&= p(0) + \int_0^t \mu(s) \dif s + \int_0^t \diffrootvol{(s)} \dif W(s) + \int_0^t
    \int_{X} \delta(s,x) 1\lbrace \norm{\delta(x,s)} \leq \boldsymbol{1} \rbrace (n - \nu) (\dif s, \dif x) \\
     & &&\quad + \int_0^t \int_{X} \delta(s,x) \boldsymbol{1}\lbrace \norm{\delta(x,s)} > 1 \rbrace n (\dif s, \dif x)
     \nonumber 
  \end{alignat*}
\end{definition}

I simplify \cref{defn:jump_diffusion_proces} by adding the following assumption.
%
\begin{assumption}[Square-Integrable]\label{ass:SQ}
  The process, $\logpricef{t}$, is locally-square integrable. 
\end{assumption}

\Titledref{ass:SQ} is relatively innocuous in practice because it holds whenever returns have conditional variances. 
Although many high-frequency papers initially allow for jumps that are so large no compensator exists, they almost always restrict themselves to processes that satisfy \titledref{ass:SQ} when they derive estimators. 
That being said, it is a real restriction on the space of processes that we consider. 
The Davis-Burkholder-Gundy inequalities imply that the we can bound the supremum of the process by the predictable quadratic variation. 
This bounds expectations of the $p$-norms for all $p$, i.e., we are ruling out all processes without all their moments. 

Making \titledref{ass:SQ} now simplifies notation because it implies that the jump measure has a predictable compensator. 
Assuming without loss of generality that $p(0) = 0$ gives
%
$\logpricef{t} = \int_0^t \mu(s) \dif s + \int_0^t \diffrootvol{(s)} \dif W(s) + \int_0^t \int_{X} \delta(s,x) (n - \nu) (\dif s, \dif x)$. 
%
I also assume without loss of generality that $n$ is a standard Poisson random measure.  

This representation is quite general and can handle a great variety of different price processes. 
In particular, it allows for both finite and infinite variation,  and also places no restriction on the Blumenthal-Getoor index.
In fact, \textcite{aitsahalia2012identifying} uses \cref{defn:jump_diffusion_proces} in their study of Blumenthal-Getoor indices. 

However, the representation in \cref{defn:jump_diffusion_proces} is rather intractable and not identified. For each $\tau$, $\delta(\tau, \cdot)$ is a function. For each set $A \subset X$, we have a Poisson process. It takes infinitely-many finite-sized open sets to partition $\R$. Each of these infinitely-many sets has a time-varying Poisson intensity. The $\delta$ function combines these intensities. To estimate this process, we would have to estimate these infinitely-many intensity parameters for each $\tau$ using only one realization.

\subsection{Discrete-Time DGP}\label{sec:daily_rtn_density}

Before I relate the discrete- and continuous-time returns, we must know what a discrete-time return is. The discrete-time return is just the change in (an increment of) the price process over some length of time, say a day.\footnote{Throughout, I focus on daily returns whose length I normalize to one, but there is nothing special about a day. We could perform the same analysis over any discrete length of time.} Throughout, I use subscripts to refer to daily objects, and functional notation to refer to stochastic processes. I index each variable by the time it first becomes known to the investor, i.e., becomes measurable in the filtration induced by the prices. For example, $\rtnt$ is the daily return on date $t$, while $\logpricef{t}$ is the log-price at time $t$: $\rtnt \coloneqq \int_{t-1}^t \logpricef[\dif]{t}$.

This return has a density --- $h$ --- in each period given the available information at the end of the day before --- $\F_{t-1}$ --- giving $\rtnt \ivert \F_{t-1} \sim h\left(\rtnt \mvert \F_{t-1}\right)$. This predictive density fully characterizes the statistical risk that investors face. In particular, any statistical measure of risk, such as Expected Shortfall or Value-at-Risk, is a statistic of this density. 

Daily returns are not very well-behaved objects in that they are unpredictable and their distributions vary substantially over time. Furthermore, we only observe one observation for each $h(\rtnt \ivert \F_{t-1})$. Since $\F_{t-1}$ grows each day, $h(\rtnt \ivert \F_{t-1})$ is a function-valued time-varying parameter. Modeling such parameters is quite difficult. Hence the literature (for example, the ARCH and GARCH models) focuses on representations for $h(\rtnt \ivert \F_{t-1})$ in terms of a well-behaved sufficient statistic for the dynamics. The most common choice for $x_t$ is some measure of volatility. 

These models use $x_t$ to separate $h(\rtnt \ivert \F_{t-1})$ into three parts. The first --- $x_t$ --- is well-behaved and predictable and hence easily forecastable. The second is noise as far as prediction is concerned and has an associated density --- $f$. It affects the risk investors face but not the density's dynamics. The third part --- $G$ --- is a process governing $x_t$'s dynamics.

Both $f$ and $G$ are fixed across-time, and $G$ is simple if we chose $x_t$ well. This gives $\rtnt \ivert \F_{t-1} \sim h(\rtnt \ivert \F_{t-1}) = \int_{x_t} f(\rtnt \ivert x_t) \dif G (x_t \ivert \F_{t-1} )$, replacing the question how should we model $h(\rtnt \ivert \F_{t-1})$ with three related questions. What should we use for $x_t$? What should use for $f$? What should we use for $G$?

For example, consider the following simple stochastic volatility model: $\rtnt \sim \diffrootvol{_t} \N(0,1)$ and $\log\left(\diffvolt\right) = \rho \log\left(\diffvol{t-1}\right) + \sigma_{\sigma} \N(0,1)$. This model uses volatility, $\diffvolt$, as $x_t$,  $f$ is a Gaussian distribution, and $\diffvolt$ follows an $AR(1)$ process in logs. 

Now that we have a discrete-time DGP, we can define the \textit{realized density}. 

\begin{definition}[Realized Density]
  \label{defn:realized_density}
  
  \begin{equation*}
    \RDt \coloneqq f\left(\rtnt \mvert x\right) \Big\vert_{x = x_{t}} 
  \end{equation*}
    
\end{definition}

Just as the realized volatility, $RV_t$, is the particular value of the volatility that realizes in a given day, the realized density, $\RDt$, is the conditional density that realizes that day. For example, in the model given above, the realized density is $f(\rtnt \ivert x_t) = f(\rtnt \ivert \sigma^2_t)$.

The realized density is useful because it separates the dynamic and static parts of the process. Besides, it is precisely the part of the likelihood that high-frequency data identifies, as shown in \cref{sec:estimation_framework}. Once we have $\RDt$, we only need to model $G$. This is much simpler than modeling $h(\rtnt \ivert \F_{t-1})$ directly as long as we chose a well-behaved $x_t$.

\section{Modeling Jump Processes}\label{sec:modelling_jump_processes}

The literature usually chooses a $x_t$ that is some volatility measure. This section constructs a new volatility measure. This measure, unlike the jump part of the quadratic variation (i.e., the sum of squared jumps), is an ex-ante measure. This distinction is fundamental to the representation constructed below.

\subsection{Jump Volatility}

One way to define the instantaneous diffusion volatility, $\diffvolf{t}$ is as follows; $\diffvolf{t}$ is the appropriately standardized variance of the diffusion part of the process over a shrinking interval.\footnote{I use superscript $D$ to refer to the diffusion part of the process.}

\begin{definition}[Instantaneous Diffusion Volatility]
    \label{defn:instantaneous_diffusion_vol}
    \begin{equation*}
        \diffvolt \coloneqq \lim_{\Delta \to 0} \frac{1}{\Delta} \E\left[ \abs*{\diff{p}(t+\Delta) - \diff{p}(t)}^2 \mvert \F_{t-}
        \right],
    \end{equation*}
%
    where $\diff{p}(t)$ is the continuous martingale part of the Ito-semimartingale.
\end{definition} 


One key subtlety of this definition is that we are only using the information available before time $t$.  Variances are forward-looking operators.  This subtlety is not essential in the diffusion case. The ex-ante and ex-post measures coincide, and so the literature has not stressed it.  In the jump case, however, it is fundamental. 

Diffusion volatility's key advantage is that we can time-aggregate it easily. The daily volatility is just the integral of the high-frequency volatility: $\diffvolt \coloneqq \int_{t-1}^t \diffvolf{t} \dif s$. The goal moving forward is to construct a sufficient statistic for the jump dynamics that also has this aggregation property. To do this, I define the \textit{jump volatility} --- $\jumpvolf{t}$. Volatilities are variances, and so we can construct the jump analog to \cref{defn:instantaneous_diffusion_vol}. I substitute the diffusion part of the prices, $\diff{p}(t)$, with the jump part --- $\jump{p}(t)$. In other words, the instantaneous jump volatility is the local variance of $\jump{p}(t)$.
 
\begin{definition}[Instantaneous Jump Volatility]
    \label{defn:instantaneous_jump_vol}

    \begin{equation*}
        \jumpvolf{t} \coloneqq \lim_{\Delta \to 0} \frac{1}{\Delta} \E\left[ \abs*{\jump{p}(t+\Delta) - \jump{p}(t)}^2 \mvert \F_{t-}
        \right], 
    \end{equation*}
%
    where $\jump{p}$ is the jump martingale part of the Ito-semimartingale.
\end{definition}
 
The integrated jump volatility is defined in the obvious way: $\jumpvolt \coloneqq \int_{t-1}^t \jumpvolf{s} \dif s$.  We can also define $\jumpvolf{t}$ in terms of \cref{defn:jump_diffusion_proces}. The jump volatility is the time-derivative of the predictable quadratic variation of the jump part of the process.
Note, the Ito-diffusion and finite predictable quadratic variation assumptions imply that these limits exist.
They do not exist full generality.
 
\begin{remark}[Jump Volatility and the Predictable Quadratic Variation]
  \label{thm:jump_vol_and_predictable_quad_var}

  Let $\logpricef{t}$ and $\int_{X} \delta^2(t, x) \dif x$ be \Ito\ semimartingales satisfying \titledref{ass:SQ}. Also assume that $\int_{X} \delta^2(t,x) \dif x$ is c\'{a}dl\'{a}g. Then the following holds where $\predQV{\jump{p}}(t)$ is the predictable quadratic variation of $\jump{p}(t)$: 
%
  \begin{equation*}
      \jumpvolf{t} = \lim_{\Delta \to 0} \frac{1}{\Delta} \int_{t}^{t+\Delta} \int_{X} \delta^2(s,x) \nu (\dif x, \dif s)  
%
      = \lim_{\Delta \to 0} \frac{1}{\Delta} (\predQV{\jump{p}}(t+\Delta) - \predQV{\jump{p}}(t)). 
  \end{equation*}
\end{remark}

The are three main advantages of $\jumpvolt$ over the jump part of the quadratic variation. First, since jump processes are not absolutely continuous, there is no ex-post analog to $\jumpvolf{t}$. We cannot take the quadratic variation's time derivative like we can take the predictable quadratic variation's time-derivative. Second, by conditioning on $\jumpvolf{t}$, I construct a closed-form nonparametric continuous-time representation for $\logpricef{t}$ in \cref{sec:dynamic_price_processes}. This representation avoids any truncation. \Textcite{todorov2014limit} must truncate all of the jumps above a shrinking threshold to derive their results while using an ex-post measure. As a final advantage, high-frequency data nonparametrically identify both $\jumpvolf{t}$ and $\jumpvolt$. \Cref{sec:estimation_framework} shows this by constructing consistent estimators for them. 


\subsection{Static Jump Processes (Variance-Gamma Process)}\label{sec:var_gamma_processes}

The next section constructs the static model that my model reduces to when there are no dynamics. Define $N(t)$ as the process that determines when $\jump{p}(t)$ jumps: 
%
\begin{equation}
    N(t) \coloneqq \sum_{s \leq t} \boldOne\lbrace \abs*{\jump{p}(t) - \jump{p}(t-)} > 0 \rbrace.  
\end{equation}
%
Let $\kappa(t) \coloneqq \lbrace p(t) \ivert N(t) \neq N(t-) \rbrace $ be the process that controls the jump magnitudes, i.e, $\kappa(t)$ is an ordered collection of $\N(0,1)$ random variables. Then the jump part of the prices process has the form: $\jump{p}(t) = \sum_{s \leq t} \kappa(s) \Delta N(s) $.

This equation's variability comes from two places: the number of jumps and their magnitudes. Since we are in a time series context, the number of jumps and their locations carry the same information. Hence, we can rewrite the jump volatility as follows using the law of iterated expectations (where we assume $N(\tau) = 0$): 
%
\begin{equation}
  \jumpvolf{\tau} 
  = \lim_{\Delta \to 0} \E\left[\frac{\abs*{p_{\tau + \Delta} - p_\tau}^2}{\Delta} \mvert \F_{\tau}\right] 
  = \lim_{\Delta \to 0} \E\left[ \E\left[ \sum_{i=1}^{N(\tau+\Delta)} \frac{\Var\left(\kappa_i(\tau)\right)}{\Delta} \mvert \F_\tau, N\left(\tau+\Delta\right) \right] \mvert \F_\tau \right].
  \end{equation}
%
We we can simplify this using the definition of $N(t)$: $\jumpvolf{\tau} = \lim_{\Delta \to 0} \E[\frac{N(\tau+\Delta)}{\Delta}] \E[\kappa(\tau)^2] = \frac{\Delta}{\Delta} = 1$.
%  
That is the variance of the jump process is the intensity multiplied by the magnitude's variance. Changing the jump intensity or expected magnitude alters the variance of $\jump{p}(t)$ in precisely the same way. This irrelevance is useful because the data do not identify the intensity and magnitude functions but do identify the volatility.  This lack of identification no longer affects our results if we take $\E[N(t)] \to \infty$. In taking this limit, we must model the distribution of $\kappa(t)$ properly so that $\logpricef{t}$ remains square-integrable.\footnote{Just letting $\logpricef{t}$ be an ordered collection of $\N(0,1)$ variables does not work.} In particular, only finitely many jumps can exceed any fixed $\epsilon > 0$ in magnitude; otherwise, the price diverges. Consequently, we must shrink the size of the increments towards zero as we let $\E[N(t)] \to \infty$.

\subsection{Jump Process Representation Theorem}\label{sec:dynamic_price_processes}
\subsubsection{\Ito\ Semimartingales}

Recall the simplified Grigelionis form of the semimartingale: $\logpricef{t} = \int_0^t \mu(s) \dif s + \int_0^t \diffrootvol{(s)} \dif W(s) + \int_0^t \int_{X} \delta(s,x) (n - \nu) (\dif s, \dif x)$. I use the variance-gamma processes and the jump volatility discussed above to simplify the representation for the jump part of the process. To do this, I introduce some empirically-innocuous assumptions that are not entirely standard in the literature. First, $\logpricef{t}$ must have infinite-activity jumps. In other words, we need at least one jump in every finite interval. This assumption implies two results. First, we do not need to track the probability that there are no jumps in a specific interval. Second, it identifies $\jumpvolf{t}$. If we consider an interval without jumps, we obviously cannot estimate $\jumpvolf{t}$ because there is no variation to identify it.

\begin{assumption}[Infinite-Activity Jumps]\label{ass:I}
  The $\logpricef{t}$ process has infinite-activity jumps. 
\end{assumption}

\Titledref{ass:I} sounds very restrictive at first and contradicts the compound Poisson assumption often used in the literature. However, it is innocuous for two reasons. First, the literature is essentially unanimous in arguing that jumps are quite common in the data, as discussed in \cref{sec:introduction}. Second, standard variance-gamma processes are limits of compound Poisson processes. As long as we have a sufficient number of jumps, the representation will work well in practice. \Cref{sec:finite_activity_jumps} further discusses this.

The last assumption requires jump times to be unpredictable.
%
\begin{assumption}[No Predictable Jumps]\label{ass:UJ}
  No predictable stopping times $\tau$ exists such that the event $\logpricef{\tau} \neq \logpricef{\tau}$ is contained in the information set $\F_{\tau}$.
\end{assumption}
 
Having laid out the assumptions, I state the first main theorem. I later prove a more general proposition, \cref{theorem:timechangedlaplace}. However, I have now described the environment sufficiently to make the result understandable. Stating the result now should make it easier to understand the structure of the next section.
Throughout I will use $\Lap(t)$ to refer to the \emph{standard} variance-gamma process, i.e., a variance-gamma process where all of the scale parameters equal one and all location parameters equal zero. This standard variance-gamma process is a Wiener process time-changed by an exponential process, making it the process generalization of the Laplace distribution.\footnote{The Laplace distribution can be represented as a normal random variable with an exponentially-distributed variance. } Its increments are Laplace random variables.

\begin{theorem}[Locally Square-Integrable It\^{o} Semimartingales as Integrals]
  \label{thm:ito_semimartingale_rep}
  Let $\logpricef{t}$ be an \Ito\ semimartingale with interval support satisfying \titledrefsthree{ass:SQ}{ass:I}{ass:UJ}. Then we can represent $\logpricef{t}$ as 
%  
  \begin{equation*}
    \label{eqn:integral_representation}
    \logpricef{t} = \int_0^t \mu(s) \dif s + \int_0^t \diffrootvol{(s)} \dif W(s) + \frac{1}{\sqrt{2}} \int_0^t \jumprootvol{(s)} \dif\Lap(s). 
  \end{equation*}
\end{theorem}

This representation replaces the function $\delta(\tau,\cdot)$, with a single scalar $\jumpvolf{\tau}$ for each $\tau$. In addition the integrator is switched from a compensated Poisson random measure, $(n - \nu)$, to a standard variance-gamma process, $\Lap(t)$. 
The main drawback of this theorem is that the $\diffrootvol{t}$ process is not explicit or even conditionally independent of $\dif\Lap(t)$. 
This is also true in the stochastic-volatility diffusion case, \cref{thm:ito_semimartingale_rep}.  \footnote{Consider the geometric Brownian motion, $\dif X(t) = X(t-) \dif W(s)$, the innovations to the volatility and the process are the same.} 
Furthermore, understanding the precise structure that is implied is more complex in the jump case because the underlying probability space of jump processes and hence the way that they can be related is more complex than in the diffusion case where a particular covariance structure is sufficient as long as the diffusion volatility is also continuous.

Consequently, this jump process can exhibit properties such as skewness and infinite variation that the variance gamma process does not exhibit.\footnote{Variance-gamma processes are infinite activity processes, not infinite variation processes.} 


\subsubsection{Time-Change Representation}

The proof of \cref{thm:ito_semimartingale_rep} relies on \cref{theorem:timechangedlaplace} and its corollaries, which I have not yet proven. In practice, \cref{theorem:timechangedlaplace} is the fundamental result. The other results are straightforward implications of it. \Cref{theorem:timechangedlaplace} is a time-change representation for jump processes and is closely related to the time-change representations in the diffusion case. Consequently, let us recall those results. 

The validity of the standard diffusion representation for general continuous martingales is implied by the Dambis-Dubins-Schwarz theorem, which shows that any continuous martingale time-changed by its predictable quadratic variation is a Wiener process. To put it in mathematical notation, \textcites{dambis1965decomposition,dubins1965continuous} says that $\diff{p}(t) \lequals W(\predQV*{\diff{p}}(t))$, where the equals sign with an $\mathcal{L}$ above it refers to equality in law. This equation evaluates the Wiener process at the random-clock $\predQV{\diff{p}(t)}$.

The crucial difference between the jump part and the continuous part of a semimartingale is that the variation in the continuous part comes from variation in magnitudes, while the jump part has two sources of variation: the magnitudes and the locations. Intuitively, the Dambis-Dubins-Schwarz theorem separates the variation in any continuous martingale into a predictable part (the volatility) and i.i.d.\@ innovations. By doing this, the martingale becomes a sum of appropriately scaled independent random variables. In other words, it is a \textquote{central limit theorem.}\footnote{Technically, this result is a law of large numbers in the diffusion case, not a central limit theorem because the convergence here is almost sure instead of in law.} In fact, one method to prove static central limit theorems is deriving them from this result.

In the jump case, though, the dynamics are more complicated. Not only do the magnitudes vary, but the locations also vary. This is why the data do not identify existing time-change representations for jumps, such as those in \parencites{monroe1972embedding, geman2001time, barndorff2010change}. When we take the infill asymptotics, variation in both the jump sizes and locations still matters in the limit. In other words, a jump martingale is a sum of a random number of random summands. If the number of summands is geometrically-distributed, various geometric-stable central limit theorems tell us how the sum behaves as the expected number of summands approaches infinity, \parencite{mittnik1993modeling}.

We can generalize the \Ito\ semimartingale assumption in \cref{thm:ito_semimartingale_rep} by only requiring that $\jump{p}(t)$ is an integral with respect to a Poisson random measure. In particular, $\logpricef{t}$'s characteristics do not need to have time-derivatives. 

\begin{theorem}[Time-Changing Jump Martingales]

  \label{theorem:timechangedlaplace}  

  Let $\jump{p}(t)$ be a purely discontinuous martingale with interval support satisfying \titledrefsthree{ass:SQ}{ass:I}{ass:UJ} that can be represented as $H \ast (n - \nu)$ where $H(t)$ is a predictable process, $n$ a Poisson random measure, and $\nu$ its predictable compensator with Lebesgue base L\'{e}vy measure. 

  Then $\jump{p}(t)$ has the same distributions as a standard variance-gamma process time-changed by its predictable quadratic variation. In other words, $\jump{p}(t) \lequals \Lap\left(\predQV*{\jump{p}}(t)\right)$.\footnote{Note, equality here only holds in law unlike in the Dambis-Dubins-Schwarz theorem, where it holds almost surely.} 
\end{theorem}

The proof of this theorem is in \cref{appendix:representationtheorem}. I present the intuition here. The first result we must establish is that the jump locations and magnitudes are conditionally independent. Thankfully, the Poisson random measure representation implies that the location and magnitude risk are independent given $\F_{t-}$. 

Given some time $\tau$ I condition on the first jump after $\tau$. I then show that the magnitudes are a continuous process in this new filtration after the appropriate time-change. Thus, I can apply the Dambis-Dubins-Schwarz theorem there, which results in a time-changed Wiener process. Since a Poisson random measure is generated by Poisson processes and  the times between jumps for a Poisson process are exponential random variables, by keeping careful track of how the exponential time-changes aggregate, we get that the time-change coming from the locations is a standard gamma process. The quadratic variation of $\logpricef{t}$ combines the quadratic variations arising from each of two time-changes. Therefore, the initial process is a time-changed standard variance-gamma process. 

Perhaps the most surprising part of \cref{theorem:timechangedlaplace} is that it implies we can \textquote{borrow-strength} from the small-jumps to learn about the large jumps without making any parametric assumptions.
At first glance, this likely appears difficult to believe.
This implies that local square-integrability is a stronger assumption than we might first expect. 
The Davis-Burkholder-Gundy inequality lets us bound the supremum of the deviations semimartingale by the predictable quadratic variation.
That is, we can control all moments of the process if the predictable quadratic variation is bounded.
If all the moments between two representations, the distributions will as well.

Analogously, the standard central limit theorem and the Dambis-Dubins-Schwarz theorem are examples square-integrability gives us parametric results in the continuous case, and the geometric central limit theorems are examples where we get parametric results in the static analogue to a jump process. 
Perhaps, it should not be too surprising that we can generalize those results to the stochastic process case, just like we can generalize the standard central limit theorem.

Time-changed results are not explicit, and so we would like an integral representation as well.
\Cref{corollary:integrallaplace} is the jump analogue to stochastic volatility diffusion representations for continuous martingales. 

\begin{corollary}[Jumps Processes as Integrals]
  \label{corollary:integrallaplace}
  
  Let $\jump{p}(t)$ be a purely discontinuous \Ito\ martingale with interval support satisfying \titledrefsthree{ass:SQ}{ass:I}{ass:UJ}. Then $\jump{p}(t) = \frac{1}{\sqrt{2}} \int_0^t \jumprootvol{(s)} \dif\Lap(s)$, where $\Lap$ is a standard variance-gamma process. 

\end{corollary}

  


\subsubsection{Processes with Finite-Activity Jumps}\label{sec:finite_activity_jumps}

The most controversial assumption I make is \titledref{ass:I}. Various authors have claimed that the data have a large, but finite, number of jumps in each period. One might wonder what happens if the data violate this assumption.  In that case, given an interval $I$, the price process is a point mass at zero if it does not jump. If the price does jump, we can use similar arguments to those above to construct a time-changed Wiener process.  In other words, the ex-ante distribution over each interval is a mixture of a point mass at zero and a scale Gaussian mixture where the mixing weights between the two parts are the probability of the jump in that interval.  

\begin{theorem}[Time-Changing Finite-Activity Jump Martingales]
  \label{corollary:timechangedlaplacemix} 
  Let $\jump{p}(t)$ be a compound Poisson martingale with interval support and mean-zero jump magnitudes.\footnote{Compound Poisson processes do not have any predictable jumps and are locally-square integrable.} Let $\nu$ denote the intensity process, and $\rho(t)$ denote the time of the next jump. Then
%
\begin{equation}
  \jump{p}(t) =
    \begin{cases}
        \mathcal{W}\left(\predQV*{\jump{p} \mvert \rho}(t) \right)      & \text{with intensity } \nu    \\
        \delta_0(t)                                             & \text{otherwise}
    \end{cases}
\end{equation}  
\end{theorem}

The critical difference between \cref{theorem:timechangedlaplace} and \cref{corollary:timechangedlaplacemix} is that we must keep track of the probability of no jump realizing. Unlike the infinite-activity case, the data do not identify time-change, and two states govern the process, not just one. The dependence of $\predQV{\jump{p} \ivert \rho}$ on $\rho$ disappears in the infinite-activity case because for any interval $I$ the probability of a jump occurring equals one. Additionally, we need not directly assume that the magnitudes are mean-zero.  We only require the initial process to be a martingale because we need not worry about the process's mean in intervals without jumps in the infinite activity case.

The main benefit of \cref{corollary:timechangedlaplacemix} is that it implies that \cref{theorem:timechangedlaplace} is the limiting case of a finite-activity process as the intensity approaches infinity (which is apparent in the proof of the infinite-activity case). This result is useful because it implies that the representation in \cref{thm:ito_semimartingale_rep} approximates the true DGP well if the intensity is relatively large. 

\subsection{Deriving the Realized Density}\label{sec:realized_density}

Having derived the continuous-time representation, we can solve the time-aggregation problem and derive the realized density. \Textcite{barndorff2002econometric, andersen2003modeling} concurrently derived the realized density in the diffusion case; although, they did not use that term. They showed that if volatility and prices are uncorrelated, $\diffvolt$ is a sufficient statistic for the dynamics under some technical conditions and that conditional on $\diffvolt$, the return density is Gaussian.

This conditional Gaussianity separates the daily return distribution into a well-behaved volatility component and a Gaussian noise component. To relate it to the previous discussion, we have the following decomposition for $h(\rtnt \ivert \F_{t-1})$ if the price is a martingale:
%
\begin{equation}
  \label{eqn:diffusion_realized_density}
  f\left(\rtnt \mvert \diffvolt\right) = f \Bigg\rvert_{x_t = \left[\int_{t-1}^t \diffvolf{s} \dif s\right]} = \N\left(0, \int_{t-1}^t \diffvolf{s} \dif s\right).
\end{equation}

I now discuss the realized density in the case with jumps. Recall that $\dif \logpricef{t} = \diffrootvol{(t)} \dif W(t) + \int_{X} \delta(t,x) (n - \nu) (\dif x, \dif t)$. Conditional on $\diffvolf{t}$ and $\delta(t,\cdot)$, the jumps and diffusion parts are independent. Consequently, returns are the sum of two conditionally independent components. Densities of sums of independent components are convolutions of the summands' densities. We know, as discussed above, that the diffusion part is a Gaussian density whose variance equals the integrated diffusion volatility. Hence, we only need to develop a parametric expression for the jump part.

Let $\Lap(0,x)$ refer to the Laplace density with mean zero and variance $x$ and recall that $\ast$ is the standard convolution symbol. Then we have the following discrete-time representation. 

\begin{theorem}[Realized Density Representation]

  \label{thm:realized_density_representation}
  Let $\logpricef{t}$ be an \Ito\ semimartingale with interval support satisfying \titledrefsthree{ass:SQ}{ass:I}{ass:UJ}. Let $\diffvolf{t}$ and $\jumpvolf{t}$ be semimartingales whose martingale components are independent of the martingale component of $\logpricef{t}$ and $\mu(t)$ be a predictable finite-variation process. Then
%
  \begin{equation}
    \label{eqn:realized_density_rep}
    \RDt\left( r_t \mvert \mu_t, \diffvolt, \jumpvolt\right) = \N\left(\int_{t-1}^t \mu(s) \dif s, \int_{t-1}^t \diffvolf{s} \dif s\right) \ast \Lap\left(0, \int_{t-1}^t \jumpvolf{s} \dif s\right), \\ 
  \end{equation}
%
  and the predictive density is 
%
  \begin{equation}
    \label{eqn:predictive_density_rep}
    h\left(r_t \mvert \F_{t-1} \right) = \int_{\mu_t, \diffvolt, \jumpvolt} \RDt\left(r_t \mvert \mu_t, \diffvolt, \jumpvolt\right) \dif G\left(\mu_t, \diffvolt, \jumpvolt \mvert \F_{t-1} \right).
  \end{equation}

\end{theorem}

The intuition behind \cref{thm:realized_density_representation} is the following. If $\jumpvolf{t}$ was constant, we could pull it out of the integral without affecting the distribution: $\int_{t-1}^t \jumprootvol{(t-1)} \dif \Lap(s) \lequals \frac{\jumprootvol{_{t-1}}}{\sqrt{2}} \int_{t-1}^t \dif \Lap(s)$. Since increments of the standard variance-gamma process are Laplace distributed, the second component is distributed $\Lap(0,1)$. Consequently, conditionally on $\jumpvol{t-1}$, we have a Laplace distribution with the specified variance. The $\sqrt{2}$ term arises because the scale of a Laplace distribution is the square root of one-half the variance. We can replace the constant assumption on the volatilities with independence conditions between the martingale components to recover the general case. 
We get that $\mu(t)$ is independent of the martingale parts of the process for free because it is a predictable and has finite-variation.

Integrating $\RDt$ out using its distribution $G$ recovers \cref{eqn:predictive_density_rep} from \cref{eqn:realized_density_rep}. In practice, we likely want to model $G$ directly. This model has the same form as the various stochastic volatility / GARCH type models in the diffusion case. 

The primary assumption that \cref{thm:realized_density_representation} adds is the independence between the martingale components. As in \textcite{andersen2003modeling} We need this assumption to time-aggregate the process because the marginal and conditional distributions given the volatilities of $\logpricef{t}$ must coincide. Importantly, this assumption restricts the leverage effect but does not assume away all dependence. The volatilities and drift can be arbitrarily related. As long as it takes a positive amount of time for the volatilities to affect the level of the prices or vice-versa, this assumption is satisfied. Besides, the observed correlation between the martingale parts is close to zero at high frequency as noticed by \textcite{aitsahalia2013leverage}, who call it \textquote{the leverage effect puzzle.}\footnote{There is some evidence that this is an artifact of the estimation procedure, and so I leave to future work the optimal way to bring it into my framework. One way to do this is by keeping track of this correlation (as \textcites{bandi2012timevarying, kalnina2017nonparametric} do) and making Gaussian and Laplacian conditioning arguments. }
