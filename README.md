This repository contains code, papers, and so on related to the Laplace Jumps project.  In particular, it contains
all of the code necessary to download the data from WRDS TAQ database, estimate the volatilities, and perform the
empirical analysis. It also contains the associated paper. If you use the code contained herein, a citation to
that paper "Jumps, Realized Densities, and News Premia" by Paul Sangrey would be greatly appreciated. It builds a
Python package that can be downloaded from https://anaconda.org/sangrey/laplacejumps using Conda.
