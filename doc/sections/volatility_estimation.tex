
\section{Volatility Estimation}


\subsection{Assumption HL implies Assumption SHL}

Similar to \textcite{jacod2012discretization}, I prove the global convergence by proving local convergence.  I do this by slightly modifying \gentextcite{jacod2012discretization} Assumption SH in the same way that I modified their Assumption H in \cref{sec:assumptions}.  I also slightly modify the literature's Assumption SH. Let  $\omega$ index the underlying probability space $\Omega$. 

\begin{assumption}[SHL]
  \label{ass:SHL}
  We have \titledref{ass:HL} and there is a constant $K$ such that the following hold for all $t$ and all $\omega$: 
%
  \begin{equation*}
    \norm{b(t, \omega)} < K,\ 
    \norm{\diffrootvol{(t, \omega)}} < K,\
    \norm{\jumprootvol{(t, \omega)}} < K.
  \end{equation*}
\end{assumption}


These two assumptions are closely related. \Titledref{ass:HL} is the local version of \titledref{ass:SHL}. \Titledref{ass:HL} only restricts the local behavior of the function, while \titledref{ass:SHL} make the equivalent conditions globally. Since convergence in the Skorokhod topology only depends upon local behavior, convergence under \titledref{ass:SHL} implies convergence under \titledref{ass:HL}. The arrow with $\mathcal{L}\text{-}s$ above it denotes stable convergence in law. 

\begin{lemma}[HL implies SHL]

  \label{lemma:hltoshl}
  If an \Ito\ semimartingale $\logpricef{t}^n \slto \logpricef{t}$ under \titledref{ass:SHL}, then $\logpricef{t}^n \slto \logpricef{t}$ under \titledref{ass:HL}, and the equivalent statement holds for convergence in probability. 

\end{lemma}

\begin{proof}

Let $U^n(p)(t)$ and $U(p)(t)$ refer to two processes that are defined in terms of $\logpricef{t}$. In the first step, I define a process in terms of $\logpricef{t}$ that satisfies \titledrefs{ass:SHL}{ass:I} and characterize its relationship to $p(t)$.  In the second step, I show that if that $\logpricef{t}$ satisfies \titledrefs{ass:HL}{ass:I}, then $U^n(p)(t) \slto U(p)(t)$ under \titledref{ass:SHL} implies $U^n(p)(t) \slto U(p)(t)$ under \titledref{ass:HL}.  I then show that \titledref{ass:I} is unnecessary, and similar statements hold for convergence in probability and convergence of stopped processes.  

\subsubsection*{Step 1}

We can assume without loss of generality that $\mu(0) = 0$, and so there is a localizing sequence $\tau_j$ such that $\norm{\mu(t)} \leq j$ if $0 \leq t \leq \tau_j$.  Define the stopping times $R_j = \inf\left(t : \norm{\logpricef{t}} + \norm{\sigma(t)} \geq \xi\right)$ and the stopping times $Q_j = \inf\left(t : \norm{\logpricef{t}} + \norm{\gamma(t)} \geq \xi\right)$.  These increase to $\infty$ as well.  Therefore, we can set $S_j = \tau_j \wedge R_j \wedge Q_j$. 

Define the following processes: 
%    
\begin{equation}
    \mu^{(j)}(t) = \mu(t \wedge S_j),\ \sigma^{(j)}(t) = \sigma(t \wedge S_j),\ \gamma^{(j)}(t) = \gamma(t \wedge S_j)
\end{equation}
%
and
%
\begin{equation}
    p^{(j)}(t) = 
    \begin{cases}
         0 &\text{if}\ S_j = 0 \\
        p(0) + \int_0^t \mu^{(j)}(s) \dif s + \int_0^t \sigma^{(j)}(s) \dif W(s) + \int_0^t \gamma^{(j)} \dif\Lap(s) &\text{if}\ S_j > 0.
    \end{cases}
\end{equation}

The local characteristics of $p^{(j)}(t)$ and $p(t)$ agree when $t < S_j$ because they are defined to be the same.  If $S_j = 0$, then $\norm{\logpricef{t}} = 0$, and so we are equal there as well.  Furthermore, if we use the same driving measures $W(t)$ and $\Lap(t)$ to represent both processes, the equality is not just in distribution, but $\omega$ by $\omega$, where the original processes are defined relative to an event space $\Omega$.  In addition, $p^{(j)}(t)$ satisfies \titledref{ass:SHL}, since $\norm{p^{(j)}(t)} \leq 3\xi$. 

   \subsubsection*{Step 2}
    By the proof of Lemma 4.4.9 in \textcite{jacod2012discretization}, the above statement is sufficient to show that the estimators defined above imply convergence stably-in-law.  Then this holds for any process, and so it holds for the stopped versions above.  Convergence stably-in-law implies convergence in probability if the two processes are defined on the same probability space, which we do not change above. So if the initial result implies convergence in probability, the new one does as well.

    If $\logpricef{t}$ does not satisfy \titledref{ass:I}, then it is locally a convolution of a Laplacian mixture and the zero process by \cref{corollary:timechangedlaplacemix}.  Replacing part of the sample path with $0$ does not violate any boundedness conditions.  Therefore, we can replace  $p^{(j)}(t)$ with the $0$ process when necessary, and so the result even holds if \titledref{ass:I} does not hold. 

\end{proof}

\subsection[Instantaneous Absolute Volatility]{\Cref{thm:localabsvol} Instantaneous Absolute Volatility}

\begin{proof}

I start this proof by deriving the mean of the absolute volatility under an assumption that $\sigma(t)$ and $\gamma(t)$ are locally constant.  I then show that the estimator in that situation converges to its mean.  I then relax the assumption of locally-constant volatility. 

\subsubsection*{Step 1}
     
    In this section, I start by applying It\^{o}'s Formula for convex functions to $\abs*{p}(t)$ to separate its variation into its jump and continuous components.  Recall the left-derivative of the absolute value function is  $f_{-}^{\prime} = \sgn(x)$. Using \textcite[Theorem 6.65]{medvegyev2007stochastic}, where $A(t)$ is a finite-valued increasing process, we can rewrite $\abs*{\logpricef{t}}$ as
%
    \begin{equation}
        \abs*{\logpricef{t}} = \int_0^t \sgn(p(s-)) \dif p(s) + A(t)  = \int_0^t \sgn(p(s-)) \dif W(s) + \int_0^t \sgn(p(s-)) \dif \Lap(s)  + A(t).
    \end{equation}
%
    This $A(t)$ can be absorbed into the drift term of $\logpricef{t}$ and vanishes as $\Delta \to 0$.  
    
    If the Laplace part and the diffusion parts have the same sign, $\abs*{p}(t) - A(t)$ is the sum of the absolute values of the two processes.  Since the innovation processes are independent and symmetric, this occurs one-half of the time. If they have different signs, the situation is more complicated.  In that case, $\sgn(p(s-))$ is the same as the sign of the larger, in magnitude, of the two processes. Let $\Omega^{\Lap}$ denote the set where the Laplace part has a larger magnitude and $\Omega^{W}$ denote the part where the diffusion part does. Then
%
    \begin{align}
        \label{eqn:cond_diff_equation}
        &\phantom{=} \abs*{\logpricef{t}} - A(t) \ivert \text{the signs differ}  \\
%        
        &=  \int_0^t \sgn(W(s-)) 1_{\Omega^{W}}(s-) \sigma(s) \dif W(s) - \int_0^t \sgn(\Lap(s-)) 1_{\Omega^{W}}(s-) \gamma(s) \dif \Lap(s) \nonumber \\ 
%
        &+\int_0^t \sgn(\Lap(s-)) 1_{\Omega^{\Lap}}(s-) \gamma(s) \dif \Lap(s) -  \int_0^t \sgn(W(s-)) 1_{\Omega^{\Lap}}(s-) \sigma(s) \dif W(s). \notag
    \end{align}

    Let $\Delta$ be the length of an interval over which $\gamma(t)$ and $\sigma(t)$ are constant, and let $\abs*{\psi}$ and $\abs{\phi}$ denote the densities of the absolute values of a Laplace and Gaussian variables, respectively.  Then we can rewrite an increment of \cref{eqn:cond_diff_equation} as follows conditional on the signs differing as follows:
%
    \begin{equation}
        \int_{0}^{\infty} \int_{x}^{\infty} (y - x) \psi_{\gamma,\Delta}(x) \abs{\phi}_{\sigma, \Delta}(y) \dif x \dif y   +  \int_{0}^{\infty} \int_{y}^{\infty} (x-y) \psi_{\gamma,\Delta}(x) \abs{\phi}_{\sigma, \Delta} \dif y \dif x. 
    \end{equation}
%
        Plugging in the parametric forms of $\psi$ and $\phi$ gives:\footnote{A standard computer algebra system can be used to perform the requisite integration.}
%
        \begin{equation}
        \frac{\sqrt{\Delta}}{\sqrt{2}} \left(-\gamma + \frac{2}{\sqrt{\pi}} \sigma + \gamma \erfcx{\left (\frac{\sigma}{\gamma} \right )}\right) + \frac{\gamma \sqrt{\Delta}}{\sqrt{2}} \erfcx\left(\frac{\sigma}{\gamma}\right) 
%
         \sqrt{\Delta} \left( m_1 \sigma + \frac{\gamma}{\sqrt{2}} \left(2 \erfcx\left(\frac{\sigma}{\gamma}\right) - 1 \right)\right).    
         \label{eqn:mean_abs_diff}
     \end{equation}

When both parts have have the same sign, the absolute value is just the sum of the absolute values and so we can rewrite \cref{eqn:cond_diff_equation} given that the signs equal as $m_1 \sigma \sqrt{\Delta} + \frac{\gamma}{\sqrt{2}} \sqrt{\Delta}$. By taking the average of this and \cref{eqn:mean_abs_diff}, we can solve for \cref{eqn:cond_diff_equation}:
%
    \begin{equation}
        \label{eqn:mean_abs_x}
        \E \abs*{\logpricef{t}} - A(t) =  m_1 \sigma \sqrt{\Delta} + \frac{\gamma \sqrt{\Delta}}{\sqrt{2}} \erfcx\left(\frac{\sigma}{\gamma}\right).
    \end{equation}
    
\subsubsection*{Step 3}

This section considers the asymptotic behavior of the estimator. It proves convergence in mean-square, which implies convergence in probability.  Let $\Omega_n$ be the set where the two increments have the same sign and let $\lambda_n$ be its accompanying Lebesgue measure.

Assume that $\sigma(t)$ and $\gamma(t)$ are step functions, there exists a sequence $\lbrace \tau_j \rbrace$ such that $\sigma(t)$ and $\gamma(t)$ are constant in $(\tau_{j}, \tau_{j+1})$.
Hence, 
%
\begin{equation}
    \logpricef{t} = \sum_{j} \int_{\tau_j}^{\tau_{j+1}} \sigma\left(\tau_j\right) \dif W(s) + \int_{\tau_j}^{\tau_{j+1}} \gamma\left(\tau_j\right) \dif\Lap(s) 
    \label{eqn:xn_t}
\end{equation}

Consider the squared norm of the difference between the estimator and its expectation.  It is worth noting that as $k_n$ gets large, we are averaging over times earlier and earlier with respect to $\tau$, which is why the bottom part of the integral is growing with $k_n$, not the top part.  We can assume without loss of generality $\sigma(t)$ and $\gamma(t)$ are constant over $\tau - k_n \Delta^n, \tau$ by taking $k_n \Delta^n$ to $0$ faster than the mesh of the $\tau_j$ goes to zero, (which it may not at all).  Consequently, we let $\tau$ depend upon $n$ in our notation.

We can rewrite the sample and population difference as 
%
\begin{align}
    &\phantom{=}  \E\left[ \norm*{\frac{1}{k_n \Delta^n} \sum_{m=0}^n \abs*{\Delta_{i_n +m}^n  p} - \abs*{m_1 \sigma(\tau_n) + \frac{\gamma(\tau_n)}{\sqrt{2}} \erfcx\left(\frac{\sigma(\tau_n)}{\gamma(\tau_n)}\right)} }^2 \right]  \\
    %
    &= \frac{1}{k_n^2 \Delta^n} \E\Bigg[ \Bigg\lVert \sum_{m=0}^{k_n} \abs*{\int_{\tau(n,m+1)}^{\tau(n,m)} \sigma(\tau(n,m+1)) \dif W(s) + \int_{\tau(n,m+1)}^{\tau(n,m)} \gamma(\tau(n, m) \dif \Lap(s)}  \\
    %
    &\quad\quad - k_n \sqrt{\Delta^n} \abs*{m_1 \sigma(\tau_n) + \frac{\gamma(\tau_n)}{\sqrt{2}}  \erfcx\left(\frac{\sigma(\tau_n)}{\gamma(\tau_n)}\right)}\Bigg\rVert^2 \Bigg].  \nonumber 
\end{align} 
%
By applying \cref{eqn:mean_abs_x},  we have
%
\begin{align}
    \label{eqn:x_t_approx}
    &= \frac{1}{k_n^2 \Delta^n} \E\Bigg[ \Bigg\lVert k_n \sqrt{\Delta^n} \abs*{m_1 \sigma(\tau(n,m)) + \frac{\gamma(\tau(n,m))}{\sqrt{2}}  \erfcx\left(\frac{\sigma(\tau(n))}{\gamma(\tau(n,m))}\right)} + A(t) \\
    %
    &\quad\quad- k_n \sqrt{\Delta^n} \abs*{m_1 \sigma(\tau_n) + \frac{\gamma(\tau_n)}{\sqrt{2}} \erfcx\left(\frac{\sigma(\tau_n)}{\gamma(\tau_n)}\right)}\Bigg\rVert^2 \Bigg].  \nonumber 
\end{align}
%
Simplifying implies this equals 
%
\begin{equation}
    O_p\left(\frac{k_n^2 (\Delta^n)^2}{k_n^2 \Delta^n}\right) + O_p(\Delta^n), 
    \label{eqn:est_pred_abs_norm}
\end{equation}
%
since $A(t)$ has finite-variation.

\subsubsection*{Step 5}
    
I now show the step function approximation is innocuous. 
Consider a sequence $\tau_n \to \tau$, and define $\tilde{\sigma}(t) = \sigma(\max \tau_n : \tau_n \leq t)$, and similarly for $\tilde{\gamma}(t)$.  Define $\xi_x^2(t) = \sup_{s_1, s_2 < t \wedge \tau} \abs*{x(s_1) - \tilde{x}(s_2)}^2$ for $x$ equal to $\sigma$ and $\gamma$, and let $\xi_b^2(t) = \sum_{s_1, s_2 < t \wedge \tau} \abs*{b(s_1) - b(s_2)}$.  These functions exist and are  almost surely finite by localization since $\sigma$, $\gamma$, and $b$ are locally-bounded.  Now, consider the squared distance between any semimartingale satisfying our assumptions and the one used in \cref{eqn:xn_t}.  Let $t_1, t_2 < \tau$.  Consider:
%
\begin{equation}
    \E\Bigg[ \bigg\rVert \int_{t_1}^{t_2} \mu(s) \dif s + \int_{t_1}^{t_2} \sigma(s) \dif W(s) + \frac{1}{2}
        \int_{t_1}^{t_2} \gamma(s) \dif\Lap(s) 
    - \left(\int_{t_1}^{t_2} \tilde{\sigma}(s) \dif W(s) + \frac{1}{2} \int_{t_1}^{t_2}
      \tilde{\gamma}(s) \dif\Lap(s)\right)\bigg\lVert^2\Bigg].
  \end{equation}
%
    Increasing the range is valid because all of the integrands are positive:
%
\begin{alignat}{2}
    &\leq && \E\left[ \int_{t_1}^{\tau} \mu(s)^2(s) + \int_{t_1}^{\tau} \abs{\tilde{\sigma}(s) - \sigma(s)}^2 \dif
      s + \frac{1}{2} \int_{t_1}^{\tau} \abs{\tilde{\gamma}(s) - \gamma(s)}^2 \dif s \right]. \\ 
%
    \intertext{Then we can bound each of the terms:}    
%
    \label{eqn:step_regular_pred_norm}
    &= && (O(1) \gamma^2_b (\tau) + O(1) \gamma^2_{\sigma}(\tau) + O(1) \gamma^2_{\gamma}(\tau)) (\tau - t_2)  = O(1) (\tau - t_2).
\end{alignat}

In other words, if we choose a sequence of meshes so that the supremum of their magnitudes $\Delta^n \to 0$ and the minimal $\tau - k_n \Delta^n \to 0$, the entire square converges.  As one might expect from the definition of integration, approximating the integrands by step functions is innocuous.

Finally, we combine the previous steps to bound the entire process.  Note, since variances of sums can be written in terms of variance of the original parts and their covariance, the asymptotic rate at which the quadratic variation decreases towards zero equals the larger of the asymptotic rates at which its constituent components do.  Let $Y(t)$ be the absolute value of the process derived in \cref{eqn:xn_t}.  Consider the mean-square deviation of the estimator from its limiting value: 
%
\begin{equation}
    \frac{1}{k_n^2 \Delta_n}  \E\Bigg[ \Bigg\lVert \sum_{m=0}^{k_n-1} \abs{\Delta^n_{i_n+m} p} - Y(t) +  Y(t)
%
    - \left(m_1 \sigma(\tau-)k_n \sqrt{\Delta^n} + \frac{\gamma(\tau-)}{\sqrt{2}} \erfcx\left(\frac{\sigma(\tau-)}{\gamma(\tau-)}\right)  k_n \sqrt{\Delta^n} \right) \Bigg\rVert^2 \Bigg].  \notag
\end{equation}
%
Splitting the term into two parts and applying \cref{eqn:est_pred_abs_norm} and \cref{eqn:step_regular_pred_norm} gives:
%
    $\frac{1}{k_n^2 \Delta_n}  \left( O(\Delta^2_n k_n^2) + O(\Delta_n k_n)\right) \to 0$.


\end{proof}

\subsection[Instantaneous Diffusion Volatility]{\Cref{theorem:localDiffusionVol} Instantaneous Diffusion Volatility}

\begin{proof}
    
By localization, we can replace \titledref{ass:HL} with \titledref{ass:SHL} without loss of generality.  Also, the jump martingale part of the process is a sum of an integral with respect to  $\Lap(t)$ and $\delta_0(t)$, where the weights depend upon the jumps' intensity by \cref{corollary:timechangedlaplacemix}. The jump increments of that part are almost surely zero, and so if we separate the space into parts where $\Lap(t)$ is active and where $\delta_0(t)$ is active, we only have to deal with the first section.  Consequently, we can assume without loss of generality that the jump part is an integral of with respect to $\Lap(t)$. The part of the proof regarding the process's continuous part does change.

\subsubsection*{Step 1}

I proceed by showing convergence in mean-square, which implies convergence in probability.  Note, $\abs*{\Delta^n_{i_n +m} p}^2 = O_p(\Delta^n)$ for all $i$, since $\logpricef{t}$ is an integral with bounded integrands and integrators whose quadratic variation is proportional to $\Delta^n$.  Consider the jump part of the variation.  To prove initial estimator's consistency, I must show that the jump part converges to zero.

Following \textcite[258]{jacod2012discretization}, for all $w,x,y, z \in \R$, $\epsilon \in (0,1]$, and $v \geq 1$, 
%
\begin{equation}    
    \label{eqn:four_part_bound}
    \abs*{(x + y + z + w) 1\lbrace \abs*{x + y + z + w} < v\rbrace - x^2 } \leq K \frac{\abs{x}^4}{v^2} + \epsilon x^2 + \frac{K}{\epsilon} ((v^2 \wedge y^2) + z^2 + w^2).
\end{equation}

    Define the following four processes. The continuous variation is split into two parts, one with locally constant volatility and the other with additional deviation coming from changes in the volatility: 
% 
        $Y^n(t) \coloneqq \sigma(\tau_n) (W_t - W_{\tau_n}) 1\lbrace \tau_n \leq t\rbrace$,
%
        $Y^{'n}(t) \coloneqq \int_{\tau_n \wedge t}^t (\sigma(s) -  \sigma(\tau_n) \dif W(s)$,
%
        $Z^{n}(t) \coloneqq \int_{\tau_n \wedge t}^t \gamma(s) \dif\Lap(s)$, and
%
        $B^n(t) \coloneqq \int_{\tau_n \wedge t}^t \mu(s) \dif s$. 

Note, $p(\tau_n \wedge t) = Y^n(t) + Y^{'n}(t) + Z^{n}(t) + B^n(t)$.  Now, we can use \cref{eqn:four_part_bound} with $x = \frac{\Delta^n_{i_n+m} Y^n}{\sqrt{\Delta^n}}$, $y = \frac{\Delta^n_{i_n+m} Z^n_{i_n+m}}{\sqrt{\Delta^n}}$, and $w = \frac{\Delta^n_{i_n +m} B^n_{i_n +m}}{\sqrt{\Delta^n}}$.  The main issue here is showing that all of the parts except for $Y^n(t)$ converge to zero because it has correct variance. Take $v = \frac{v_n}{\sqrt{\Delta^n}} = \omega_n$, where $\omega_n = o_p(1 / \Delta^n)$ and $1 / \omega_n$ is $o_p(\sqrt{\Delta})$.  Then  $\frac{1}{k_n \Delta^n} \sum_{m=0}^{k_n -1} \abs*{(Y_t^n)^2 - (p_t^n)^2}$ is bounded by 
%
    \begin{equation}
        \frac{1}{k_n} \sum_{m=0}^{k_n -1} \Bigg(\frac{K}{\omega_n^2} \abs*{\frac{\Delta^n_{i_n +m}
        Y^n}{\sqrt{\Delta^n}}}^4 + \epsilon \abs*{\frac{\Delta^n_{i_n +m} Y^n}{\sqrt{\Delta^n}}}^2 
%
        + \frac{K}{\omega_n^2 \epsilon} \abs*{\frac{\Delta^n_{i_n+m} Z}{\sqrt{\Delta^n}\omega_n}}^ 2+
        \frac{K}{\epsilon} \abs*{\frac{\Delta^n_{i_n +m} Y^{'n}}{\sqrt{\Delta^n}}}^2 +
        \frac{K}{\epsilon}\abs*{\frac{\Delta^n_{i_n+m} B}{\sqrt{\Delta^n}}}^2\Bigg). 
    \end{equation}
%    
    Set $\xi_n = \sum_{s \in \abs*{\tau_n, \tau_n + (k_n +2)\Delta^n}} \abs*{\sigma(s) - \sigma(\tau_n)}^2$, which is bounded and converges to zero, and $\phi_n = \sum_{s \in \abs*{\tau_n, \tau_n + (k_n +2)\Delta^n}} \abs*{\gamma(s)}^2$ The key hard part is bounding $\Delta^n_{i_n +m} Z$.  Clearly, $E\abs*{\Delta^n_{i_n +m}} \leq  \phi_n \sqrt{\Delta^n}$.  Consider the part of the variation in $Z(t)$ that comes from jumps smaller than $1$ in magnitude, where $1$ is an arbitrary constant picked for the sake of simplicity:
%
    \begin{equation}
        \E \abs*{\Lap(0, \phi_n) \wedge 1} = \phi_n \sqrt{\Delta^n}  - \exp\left(-\frac{1}{\phi_n \sqrt{\Delta^n}}\right) (\phi_n \sqrt{\Delta^n} + 1) \leq O\left(\frac{1}{\sqrt{\Delta^n}}\right) \exp\left(-\frac{1}{\phi_n \sqrt{\Delta^n}}\right). 
    \end{equation}

    In addition, since $\tau_n$ is a stopping time, the probability that a jump exceeds $1$ in the previous $k_n$ periods declines to $0$ almost surely with $\Delta^n$.  Consequently, $\frac{1}{\omega_n^2} \frac{\Delta^n_{i_n+m} Z}{\sqrt{\Delta^n} \omega_n} \stackrel{a.s.}{\in} O_p\left(\frac{1}{\Delta^n \omega_n^3}\right) \exp\left(-\frac{1}{\phi_n \sqrt{\Delta^n}}\right) = o_p(1)$ as exponential functions decay faster than polynomials increase. 

    I use $K$ to refer to an arbitrary constant, which may change.  The drift term is $\Delta^n_{i_n +m} B$, and so $\abs*{\Delta^n_{i_n +m} B} \leq K \Delta^n$. Meanwhile, $\E[ \abs*{\Delta^n_{i_n +m } Y^n}^4 \vert \F_{(i_n +m -1) \Delta^n} ] \leq K (\Delta^n)^2$, and $\E[ \abs*{\Delta^n_{i_n +m} Y^{'n} }^n \vert \F_{(i_n +m -1)\Delta^n} ] \leq K \Delta^2 \E[\xi_n \vert \F_{(i_n +m -1) \Delta^n} \leq K \Delta^n$.  As a consequence, we have the following where $\xi_n$ is some sequence converging to zero:
%
    \begin{equation} 
        \E[\abs*{(Y_t^n)^2 - (p_t^n)^2}] \leq K \epsilon + \frac{K}{\epsilon} (o_p(1) + o_p(1) +\E[\xi_n]).
        \label{eqn:diff_vol_combined_bound}
    \end{equation}
   
%
    Taking $n \to \infty$ and then $\epsilon \to 0$, makes the left-hand side \cref{eqn:diff_vol_combined_bound} converge to zero. 

    \subsubsection*{Step 2}
    
    Consider $\lim_{n \to \infty} \frac{1}{k_n \Delta^n} \sum_{m=0}^{k_n -1}  \abs*{Y_t^n}^2$ is. Its definition implies it converges to the variance of the increment: 
%
    \begin{equation}
         \frac{1}{k_n \Delta^n} \sum_{m=0}^{k_n -1} \abs*{\sigma_{\tau_n} (W_t - W_{\tau_n}) 1\lbrace \tau_n \leq
         t\rbrace}^2 
%
        =  \sigma(\tau_n)^2 \frac{1}{k_n} \sum_{m=0}^{k_n -1} \abs*{\frac{\Delta^n_{i_n + m}
           W}{\sqrt{\Delta^n}}}^2 \to \sigma(\tau_n)^2
    \end{equation}

    Since the square is a convex function, we can combine these two previous limits, given that the original expression converges to $\sigma(\tau_n)^2$.  However, this is the local integrated volatility evaluated at $\tau_n$, which was the object of interest.  Multiplying the expression by a value converging to $1$ does not change the results. 
    
\end{proof}

\subsection[Instantaneous Jump Volatility]{\Cref{them:localjumpvolt} Instantaneous Jump Volatility}

\begin{proof}
    Note, $0$-subscripts denote population objects.  Consider
%
    \begin{equation}
        \widehat{Q}_n(\gamma) \coloneqq g\left(\abs*{\frac{1}{k_n \sqrt{\Delta}} \sum_{m=0}^{k_n-1} \abs*{\Delta^n_{i_n +m} p}  - m_1 \hat{\sigma}(\tau-) - \gamma \erfcx\left(\frac{\sigma_0}{\gamma \sqrt{2}}\right)}\right).    
    \end{equation}

    Note that $\widehat{Q}_n(\gamma)$ is implicitly a continuous function of $\hat{\sigma}_n(\tau-)$.  By assumption, $\hat{\sigma}_n(\tau-) \pto \sigma_0$, and so we can suppress that dependence in our notation and plug in $\sigma_0$.  In addition, $g$ is an increasing function and both $g$ and the absolute value are convex, continuous functions, we can use the continuous mapping theorem to derive the limiting value of $\widehat{Q}_n(\gamma)$.  
%
    \begin{equation}
        Q_0(\gamma) \coloneqq g\left(\abs*{\gamma_0 \erfcx\left(\frac{\sigma_0}{\gamma_0 \sqrt{2}}\right) - \gamma \erfcx\left(\frac{\sigma_0}{\gamma \sqrt{2}}\right)}\right).
    \end{equation}

Clearly, this equals zero when $\gamma = \gamma_0$.  If both $\widehat{Q}_n(\gamma)$ and $Q_0(\gamma)$ are  strictly convex, the minimum is unique.  Define $A(\sigma, \gamma) \coloneqq \gamma \erfcx\left(\frac{\sigma}{\gamma \sqrt{2}}\right)$.  Showing $A(\sigma, \gamma)$ is strictly increasing for all $\sigma$ is sufficient to show this convexity because of properties assumed about  $g$ and the absolute-value function. Consider  
%
    \begin{equation}
        \frac{\partial}{\partial \gamma} \gamma \erfcx\left(\frac{\sigma}{\gamma \sqrt{2}}\right) = \erfcx\left(\frac{\sigma}{\gamma \sqrt{2}}\right) - \left. \frac{\sigma}{\gamma^2 \sqrt{2}} \frac{\partial}{\partial x} \erfcx(x)\right\rvert_{x=\frac{\sigma }{\gamma \sqrt{2}}}.
    \end{equation}
%
Since $\erfcx$ is a positive, decreasing function, the last term is negative, and so the entire equation is strictly positive.  This result implies that $\widehat{Q}_n(\gamma)$ and $Q_0(\gamma)$ are both strictly convex as functions of $\gamma$, which then implies the minimum given above is strict.

Since $\gamma_0 > 0$, it lies in the interior of a convex set. By \textcite[Theorem 2.7]{newey1994large}, $\hat{\gamma}_n$ is a unique minimizer and $\hat{\gamma}_n \pto \gamma_0$.


\end{proof}

