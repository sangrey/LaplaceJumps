\documentclass[11pt, letterpaper, twoside]{article}

%Setup the packages to use UTF-8
\usepackage{fontspec}
\usepackage{polyglossia}
\setdefaultlanguage[variant=american]{english}

\usepackage{tikz}
\usetikzlibrary{decorations}
\usetikzlibrary{automata,positioning}

%Use AMS packages 
\usepackage{amsmath, amssymb, amsthm}
\usepackage{mathrsfs, mathtools}
\usepackage{thmtools, thm-restate}

% Use standard orientation of the code. 
\usepackage[onehalfspacing]{setspace}
\usepackage[headheight=\baselineskip, margin=1in]{geometry}

\usepackage[backend=biber, autopunct=true, authordate, hyperref=true, url=false, doi=false,
isbn=false, eprint=false]{biblatex-chicago} 


% Captions and References 
\usepackage[colorlinks]{hyperref}
\usepackage[noabbrev, capitalize, nameinlink]{cleveref}
\usepackage{csquotes}
\usepackage{siunitx}

% %%%%%%%%%%%%%%%%%%% Commands %%%%%%%%%%%%%%%%%%% 
\newcommand*{\Cov}{\mathbb{C}\mathrm{ov}}
\newcommand*{\E}{\mathbb{E}}
\newcommand*{\F}{\mathcal{F}}
\newcommand*{\I}{\mathcal{I}}
\newcommand*{\D}{\mathcal{D}}
\newcommand*{\Lap}{\mathcal{L}}  
\newcommand*{\N}{N}
\newcommand*{\R}{\mathbb{R}}
\newcommand*{\Var}{\mathbb{V}\mathrm{ar}}
\newcommand*{\aequals}{\overset{a.s.}{=}}
\newcommand*{\lequals}{\overset{\mathcal{L}}{=}}
\newcommand*{\dif}{\mathop{}\!d}
\newcommand*{\mvert}[1][\middle]{\ensuremath{\,#1\vert\,}}
\newcommand*{\ivert}[1][\middle]{\ensuremath{\,\vert\,}}
\newcommand*{\sLog}{\mathcal{L}og}


\addbibresource{density_estimation.bib}

\title{Jumps, Realized Densities, and News Premia} 
\author{Paul Sangrey} 


\begin{document}

\maketitle    
\thispagestyle{empty}

%Reword this.
To layout the simplest framework possible I start with a two period version of the model.
The investor consumes $C_0$ at the beginning of the period and $C_1$ at the end of the period.
At some point in time $\tau$ in $(0,1)$ a news item is released, which the investor can trade on. 
The investor's preferences satisfy the following utility recursion for any time $\tau > t$.
%
\begin{equation}
    V_t(W_t) = u(C_t) + \phi^{-1}\left(\E\left[\phi\left(V_{\tau}(W_{\tau})\right) \mvert \F_{t-}\right]\right).
\end{equation}
%
The setup is identical to the sequential trading model \textcite[15]{ai2018risk} lay out except I let the news
(announcement) be released at some random time between the two times the investor consumes instead of a
pre-specified time.

I assume that the investor has access to three assets. 
1) A risk-less asset, $\chi_{f,t}$, that pays off \num{1} unit in every period, and whose price will equal
\num{1} because investors do not discount the future. 
Essentially, you can view it as a costless storage technology.
2) An asset, $\zeta_t$, whose payout $R_{\zeta,t}$ is announced by the  news release.
3) An asset $\xi_t$ whose payout  --- $R_{\xi_t}$ --- is realized smoothly over time, i.e.\@ I model it as a
continuously-tradable Brownian motion. 
\Cref{fig:timing} displays the timing.
I maintain the convention where the time subscript refers to when the variable first enters the investor's
information  set.
The investor's problem here is the problem laid out in \textcite[eqn. 18]{ai2018risk} simplified to only have
three trading opportunities and three assets. 

\begin{figure}[htb]

    \caption{Timing}
    \label{fig:timing}

    \vspace{\baselineskip}

    \centering
    \begin{tikzpicture}
        %draw horizontal line
        \draw (0,0) -- (8,0);
    %    
        % draw vertical lines
        \draw (1, .25) -- (1,-.25);
        \draw (5, .25) -- (5,-.25);
        \draw (7, .25) -- (7,-.25);

        \node[anchor=south]  at (1, .45) {0};
        \node[anchor=south]  at (5, .45) {$\tau$};
        \node[anchor=south]  at (7, .45) {1};
    %    
        % draw nodes
        \node[anchor=north] (a)  at (5, -1.5) {News Released}; 
        \draw[->] (a)  -- (5, -.5); 
% %
        \node[anchor=north] (b)  at (1, -1.5) {$C_0$}; 
        \draw[->] (b)  -- (1, -.5); 
        \node[anchor=north] (c)  at (7, -1.5) {$C_3$}; 
        \draw[->] (c)  -- (7, -.5); 
    \end{tikzpicture}
\end{figure}


I solve this problem by working backwards.
At time \num{1}, all uncertainty has been resolved and the representative agent eats all of their wealth:
%
\begin{equation}
    V_1(W_1) = u(W_1).
\end{equation}
%
Let $\tau < t < 1$, then the investor can trade $\xi$ and the risk-less asset $\chi$, but that is it because
$\zeta$ is now risk-less.
In other words, her value function is 
%
\begin{equation}
    V_{t}(W_{t}) = \max_{\xi} \phi^{-1}(\E\left[\phi\left(V_1\left(W_{1}\right)\right) \mvert
    \F_{t} \right]), \text{where}\ 
%
    W_{1} = W_{t} - \xi_{{t}}  + R_{\xi} \xi_{{t}},
\end{equation}
%
because she gets return \num{1} from the risk-less asset and $R_{\xi}$ from the risky asset.
%
By substituting the constraint into the problem, and noting that $V_1(W_1) = u(W_1)$, this is 
%
\begin{equation}
    \label{eqn:simplified_news_value_function}
    V_{t}(W_{t}) = \max_{\xi} \phi^{-1}\left(\E\left[\phi\left(u\left(W_{t} +
    \xi_{t} (R_{\xi} -1) \right)\right) \mvert \F_{t} \right]\right). 
\end{equation}
%
The first-order condition is
%
\begin{equation}
    0 = \frac{\E\left[\phi'\left(u\left(W_{1}\right)\right)u'(W_{1}) \left(R_{\xi} - 1\right) \mvert
\F_{t}\right]}{\phi'(\phi^{-1}(\E\left[\phi(u(W_{1})) \mvert \F_{t}\right] )))}.
\end{equation}
%
The denominator never equals zero as $\phi$ is strictly increasing, giving 
%
\begin{equation}
    \label{eqn:discrete_time_asset_pricing_eqn}
    0 = \E\left[\phi'(u(W_{1})) u'(W_{1}) (R_{\xi} - 1) \mvert \F_{t} \right].
\end{equation}
%
In addition, standard envelope theorem results give the following formula for $V_{t}'(W_{t})$ where $W_1$ is the
result of the optimal choice of $\xi$.
%
\begin{equation}
    \label{eqn:envelope_formula}
    V_{t}'(W_{t}) = \frac{\E\left[\phi'\left(u\left(W_1 \right)\right) u'\left(W_1\right) \mvert \F_{t}
    \right]}{\phi'\left(\phi^{-1}\left(\E\left[\phi\left(u\left(W_1 \right)\right) \mvert \F_{t}
    \right]\right)\right)}. 
\end{equation}

The investor's problem for some time $T$ in $(0, \tau)$  has similar structure except now, she can trade both
assets. 
%
\begin{gather}
    \label{eqn:consumption_problem}
    V_T(W_T) = \max_{\zeta} \phi^{-1}(\E\left[\phi(V_\tau(W_\tau)) \mvert \F_T\right])  \\
%
    \chi + \zeta + \xi = W_T \\ 
%
    W_\tau =  R_{\zeta} \zeta  + R_{\xi} \xi  + \chi  
\end{gather}
%
% \Cref{eqn:consumption_problem} is precisely the problem laid out in \textcite[eqn.  17]{ai2018risk} simplified
% appropriately. 
%
I now substitute the constraints into the problem, giving 
%
\begin{equation}
    \label{eqn:consump_value_func}
    V_T(W_T) = \max_{\zeta, \xi} \phi^{-1}\left(\E\left[\phi\left(V_{\tau}\left(W_T + (R_{\zeta} -1) \zeta +
                    (R_{\xi} - 1) \xi \right)\right) \mvert \F_T\right]\right).  
\end{equation}
%
The first-order condition with respect to the $x$ for $x \in \lbrace \zeta, \xi \rbrace$ is 
%
\begin{equation}
    0 = \frac{\E\left[\phi'\left(V_{\tau}\left(W_{\tau}\right)\right)V_{\tau}'(W_{\tau}) \left(R_{x} - 1\right)
\mvert \F_{T}\right]}{\phi'(\phi^{-1}(\E\left[\phi(V_{\tau}(W_{\tau})) \mvert \F_{T}\right] )))}.
\end{equation}
%
The denominator is always positive and so that gives
%
\begin{equation}
    0 = \E\left[\phi'\left(V_{\tau}\left(W_{\tau}\right)\right)V_{\tau}'(W_{\tau}) \left(R_{x} - 1\right) \mvert
    \F_{T}\right]
\end{equation}
%
By substituting in \cref{eqn:envelope_formula} for $t= \tau$.
%
\begin{equation}
    \label{eqn:expanded}
    \E\left[\phi'\left(V_{\tau}\left(W_{\tau}\right)\right)\frac{\E\left[\phi'\left(u\left(W_1 \right)\right)
    u'(W_1) \mvert \F_{\tau} \right]}{\phi'\left(\phi^{-1}\left(\E\left[\phi\left(u\left(W_1 \right)\right) \mvert
    \F_{\tau} \right]\right)\right)} \left(R_{x} - 1\right) \mvert \F_{T}\right] = 0. 
\end{equation}
%
In \cref{eqn:envelope_formula}, I imposed that $\xi$ was chosen optimally,  $V_{\tau}(W_{\tau}) =
\phi^{-1}\E\left[\phi(u(W_1)) \mvert \F_{\tau}\right]$, and so \cref{eqn:expanded} simplifies to 
%
\begin{equation}
    \E\left[\E\left[\phi'\left(u\left(W_1 \right)\right) u'(W_1) \mvert \F_{\tau} \right]\left(R_{x} - 1\right)
    \mvert \F_{T}\right] = 0. 
\end{equation}
%
for $x \in \lbrace \xi, \zeta \rbrace$.
Consider $x = \zeta$, we know that the entire covariance occurs in $0, \tau]$ because $\zeta$ is risk-free after
$\tau$.
%
\begin{equation}
    \E\left[\E\left[\phi'\left(u\left(W_1 \right)\right) u'(W_1) \mvert \F_{\tau} \right]\left(R_{\zeta} -
    1\right) \mvert \F_{T}\right] = 0. 
\end{equation}
%
We can multiply through by the predictable projection of the term inside the inner projection it is a continuous
process, the only discrete release of information occurs at $\tau$ and discontinuous and continuous processes are
orthogonal. 
%
\begin{equation}
    \E\left[\frac{\E\left[\phi'\left(u\left(W_1 \right)\right) u'(W_1) \mvert \F_{\tau}
    \right]}{\E\left[\phi'\left(u\left(W_1 \right)\right) u'(W_1) \mvert \F_{\tau-} \right]}\left(R_{\zeta} -
    1\right) \mvert \F_{T}\right] = 0. 
\end{equation}
%
Since utility is continuous, we assumed wealth was continuous, and the utility function is sufficiently smooth is
is the same in the numerator and the denominator for $\tau$ sufficiently close to $1$.
(This claim only hold approximately in discrete-time, but if you take limits it hold exactly.) 
%
\begin{equation}
    \E\left[\frac{\E\left[\phi'\left(u\left(W_1 \right)\right) \mvert \F_{\tau}
    \right]}{\E\left[\phi'\left(u\left(W_1 \right)\right) \mvert \F_{\tau-} \right]}\left(R_{\zeta} -
    1\right) \mvert \F_{T}\right] = 0. 
\end{equation}
%
In other words, the curvature of $\phi$ entirely determine the risk-premia for $\zeta$.
The argument above does not go through for $\xi$ though.
The left-hand sides of the previous two equations are the same as are the denominators of the right-hand sides.
%
Conversely, the first term is precisely $m_{*}(t)$  and so to put in terms used by \textcite{ai2018risk}, we have
an announcement premium.
%
\begin{equation}
    0 = \E\left[\E\left[m_{*} (R_{\zeta} - 1) \mvert \F_{t-}\right] \mvert \F_{T}\right].
\end{equation}
%
We can rewrite this as a risk-premium if we assume prices are log-normal.
%
\begin{equation}
    \E[r_{\zeta}] = -\Cov\left[m_{*}, r_{\zeta} \mvert \F_{t-}\right].
\end{equation}


Conversely, consider what happens if we replace $x$ with $\xi$.  
Since $\xi$ is assumed to be continuous, we can ignore discrete-changes at any point in time.
In other words,  there is no difference between $\F_{T}$ and $\F_{T-}$.
%
\begin{equation}
    \E\left[\E\left[\phi'\left(u\left(W_1 \right)\right) u'(W_1) \mvert \F_{\tau-} \right]\left(R_{\zeta} -
    1\right) \mvert \F_{T}\right] = 0. 
\end{equation}
Using the law of iterated expectation gives
%
\begin{equation}
    \E\left[\phi'\left(u\left(W_1 \right)\right) u'(W_1) \left(R_{\zeta} - 1\right) \mvert \F_{T}\right] = 0. 
\end{equation}

%
We can multiply through by the predictable projection of the term inside the inner projection it is a continuous
process, the only discrete release of information occurs at $\tau$ and discontinuous and continuous processes are
orthogonal. 
%
\begin{equation}
    \E\left[\frac{\E\left[\phi'\left(u\left(W_1 \right)\right) u'(W_1) \mvert \F_{\tau}
    \right]}{\E\left[\phi'\left(u\left(W_1 \right)\right) u'(W_1) \mvert \F_{\tau-} \right]}\left(R_{\xi} -
    1\right) \mvert \F_{T}\right] = 0. 
\end{equation}
%


Notice, the key question is here is whether the left and right limiting information sets ($\F_{t-}$ and
$\F_{t+})$ are identical. 
To see this more starkly, assume that $\F_{t-} = \F_{t+}$.
Then 
%
\begin{align}
    \phi^{-1} \E\left[\phi(V) \mvert \F_{t-}\right] 
%
    &= \phi^{-1} \E\left[\phi(V) \mvert \F_{t+}\right]
%
    \intertext{However, since $V_t$ is $\F_{t+}$ measurable, we can pull it, and hence $\phi$ out of the
    expectation.}
%
    &= \phi^{-1} (\phi(V)) \\
    %
    &= V
    %
\end{align}
%



\end{document}



