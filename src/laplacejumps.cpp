#include <stdexcept>
#include <cmath>
#include <pybind11/pybind11.h>
#include <pybind11/stl.h>
#include "pybind11/operators.h"
#include <pybind11/iostream.h>
#include <arma_wrapper.h>
#include "laplacejumps.h"


namespace py = pybind11;
using namespace pybind11::literals;
using namespace std::string_literals;
using aw::npulong;
using aw::npdouble;
using aw::dmat;
using aw::dvec;
using stream_redirect = py::call_guard<py::scoped_ostream_redirect>;                                               


double erfcx(double x) {

    if (std::isinf(x) && (x > 0)) {
        return 0;
    }
    else if (std::isinf(x) && (x < 0)) {
        return arma::datum::inf;
    }

    return std::exp(std::pow(x,2.0) + std::log(std::erfc(x)));

}

double abs_gaussian_moments(size_t power, double scale=1) {

    double log_moment =  0.5 * power * std::log(2);
    log_moment +=  std::lgamma(0.5 * (power + 1));
    log_moment -= 0.5 * std::log(arma::datum::pi);
    log_moment += power * std::log(static_cast<double>(scale));

    return std::exp(log_moment);

}

double mean_abs_vol(double sigma, double gamma) {

    double m1 = abs_gaussian_moments(1);

    if (gamma <= 0) {

        return m1 * sigma;
    } else if (sigma <= 0) {

        return gamma / std::sqrt(2.0);
    }
    else {

        return m1 * sigma + (gamma / std::sqrt(2)) * erfcx(sigma / gamma); 
    }

}

double bipower(const dmat& arr, size_t l=1, size_t r=1, size_t idx_diff=0) {

    if (arr.n_elem == 0) {
        return arma::datum::nan;
    }
    else if (! arr.is_finite()) {
        return arma::datum::nan;
    }
    else if (arr.n_elem == 1) {
        return std::pow(arma::as_scalar(arr), l);
    }

    if ((l + r) <= 0) {
        std::string err_msg = "The powers (l{"s + std::to_string(l) + "} + r{"s + std::to_string(r) 
                    + "}) must sum to a positive number."s;
        throw std::invalid_argument(err_msg);
    }

    if (idx_diff >= arr.n_elem) {
        std::string err_msg = "idx_diff ("s + std::to_string(idx_diff) + ") must be less than the number of"s +
            " elements in arr"s + std::to_string(arr.n_elem);
        throw std::invalid_argument(err_msg);
    }

    size_t n_middle_elem = arr.n_elem - idx_diff;
    dvec x = arma::abs(arma::vectorise(arr));
    double norm_factor = std::pow(x.n_elem, (((l + r) / 2.0) - 1.0));
    double bipower = arma::dot(arma::pow(x.head_rows(n_middle_elem), l),
            arma::pow(x.tail_rows(n_middle_elem), r)); 

    double pre_constant = abs_gaussian_moments(l) * abs_gaussian_moments(r);

    return bipower / pre_constant * norm_factor;

}


double est_local_diffusion_vol(const dmat& arr, size_t infill_dim, double threshold_const=arma::datum::sqrt2)
{

    if (arr.n_elem == 0) {
        return arma::datum::nan;
    }

    double oldvol = bipower(arr, 1,1, 1) * (static_cast<double>(infill_dim) / arr.n_elem);

    /* You want to start with the volatitlity above its true value and iterate downwards, 
     * because you can get stuck at zero. */
    double newvol = 1.25 * oldvol; 
    size_t n_iter = 0;

    while (std::abs(oldvol - newvol)  > (1e-8 + 1e-5 * std::abs(oldvol))) {

        double threshold = threshold_const * std::sqrt(newvol * std::log(std::log(infill_dim)) / infill_dim);
        oldvol = newvol;
        
        double tmpvol = 0;

        for(const auto& x : arr) {
            int trunc = std::abs(x) <= threshold;
            tmpvol += std::pow(x * trunc, 2) * (static_cast<double>(infill_dim) / arr.n_elem);
        }

        ++n_iter;
        if (n_iter > 5e3) {
            std::cerr << "I am breaking out of the diffusion volatility estimation loop without convergence."
                << std::endl;
            break;
        }
        newvol = tmpvol;
    }

    return newvol;

}

double est_local_abs_vol(const dmat& arr, size_t infill_dim) {

    if (arr.n_elem == 0) {
        return arma::datum::nan;
    }

    double vol = arma::as_scalar(arma::mean(arma::abs(arr))) * std::sqrt(infill_dim);

    return vol;
    
}

dmat excess_rtn_density_forecast(const dvec& regressor, const dvec& intercept, const dmat& innov_cov, const dmat&
        coeff, const aw::iuvec& regressand_coeff_indices, const aw::iuvec& rxt_coeff_indices, size_t
        rxt_innov_index, bool log_prop_form, size_t H, size_t forecast_dim=1) {
    
    dmat predictions(H, forecast_dim);

    aw::iuvec rxt_innov_indices = aw::iuvec{rxt_innov_index};
    
    for(size_t n=0; n<predictions.n_cols; ++n) {
        predictions.col(n) = laplacejumps::har_density_forecast(regressor, intercept, innov_cov, coeff,
            regressand_coeff_indices, rxt_innov_indices, rxt_coeff_indices, log_prop_form,
            H).col(rxt_innov_index);
    }

    return predictions;
    
}


double jump_vol_error(double abs_vol, double gamma, double sigma, double standardize_val=1) {

    return std::abs(abs_vol / standardize_val - mean_abs_vol(sigma / standardize_val, gamma));

}

PYBIND11_MODULE(liblaplacejumps, m) {

    m.def("_simulate_cir", &laplacejumps::simulate_cir, stream_redirect(), 
            "Simulates the Cox-Ingersoll_Ross Model", "kappa"_a, "omega"_a, "theta"_a, "time_dim"_a=250,
            "infill_dim"_a=250, "alpha"_a=0.05);

    m.def("abs_gaussian_moments", &abs_gaussian_moments, stream_redirect(), 
            "Computes the centered abolute gaussian moment of order p.", "power"_a, "scale"_a=1); 

    m.def("bipower", &bipower, stream_redirect(), "Computes the l,r bipower of the array.", "arr"_a, "l"_a,
            "r"_a, "idx_diff"_a);

    m.def("real_vol", [] (const dmat& arr) {return bipower(arr, 2, 0, 0);}, stream_redirect(), 
            "Computes the realized volatlity of the series or return NaN if the series is empty", "arr"_a);

    m.def("est_local_diffusion_vol", &est_local_diffusion_vol, stream_redirect(), 
           "Uses a truncation formula based on the law of the iterated logarithm to estimate the local "
           "diffusion volatility.", "arr"_a, "infill_dim"_a, "threshold_const"_a=arma::datum::sqrt2);

    m.def("est_local_abs_vol", &est_local_abs_vol, stream_redirect(), "Estimates the local absolute volatility",
            "x"_a, "infill_dim"_a);

    m.def("mean_abs_vol", &mean_abs_vol, stream_redirect(),
            "Computes the expectation of the absolute value of the sum of mean-zero  Gaussian and"
            "Laplace variables.", "sigma"_a, "gamma"_a);

    m.def("_laplace_rvs", &laplacejumps::laplace_rvs, stream_redirect(),
            "Generates standard laplace random variables." "n_elem"_a=1);

    m.def("_multivariate_normal_rvs", &laplacejumps::multivariate_normal_rvs, stream_redirect(), 
            "Generates multivariate normal innovations.", "innov_cov"_a, "size"_a=1);

    m.def("_har_density_forecast", &laplacejumps::har_density_forecast, stream_redirect(), "Provides a vector"
            " forecast of a HAR model", "regressor"_a, "intercept"_a, "innov_cov"_a, "coeff"_a, 
            "reggressand_coeff_indices"_a, "rxt_innov_indices"_a, "rxt_coeff_indices"_a, 
            "log_prop_form"_a=true, "H"_a=1);

    m.def("_har_forecast", &laplacejumps::har_forecast, stream_redirect(), "Provides a the mean of the h-step " 
            " forecast of a HAR model", "regressor"_a, "intercept"_a, "coeff"_a, "reggressand_coeff_indices"_a,
            "rxt_innov_indices"_a, "rxt_coeff_indices"_a, "H"_a=1);

    m.def("_excess_rtn_density_forecast", &excess_rtn_density_forecast, stream_redirect(), "Provides a vector" 
            "forecast of rxt variable from a HAR model", "regressor"_a, "intercept"_a, "innov_cov"_a,
            "coeff"_a, "reggressand_coeff_indices"_a, "rxt_coeff_indices"_a,"rxt_innov_index"_a=0, 
            "log_prop_form"_a=true, "H"_a=1, "forecast_dim"_a=1);

    m.def("_constrained_log_like", &laplacejumps::constrained_loglike, stream_redirect(), 
            "Calculates the pseudo-likelihood of a constrained linear regression.", "endog"_a, "exog"_a,
            "params"_a, "root_cov"_a, "weights"_a, "constrained_indices"_a);

    m.def("jump_vol_error", &jump_vol_error, stream_redirect(),
            "Calculates the jump volatilty erorr.", "abs_vol"_a, "gamma"_a, "sigma"_a, "standardize_val"_a=1);

    m.def("simulate_ar", &laplacejumps::simulate_ar, stream_redirect(), 
            "Simulates a Gaussian autoregression, which starts at the mean.", "coeff"_a, "mean"_a, "innov_var"_a); 
}

