\documentclass[smaller, handout, aspectratio=169]{beamer}
\usetheme{Boadilla}
\usecolortheme{rose}
\input{slides_preamble.tex}


\title[Realized Densities]{Jumps, Realized Densities, and News Premia} 
\author[Sangrey]{Paul Sangrey}
\institute{\large \textit{University of Pennsylvania} \vspace{.25\baselineskip} \protect\newline
\href{mailto:paul@sangrey.io}{\textsf{paul@sangrey.io}}}
\date{April 21st, 2018}


\begin{document}


\begin{frame}[plain, noframenumbering]
	\maketitle
\end{frame}


\begin{frame}[c]{Return Dynamics}
    \centering

    \begin{figure}[t]
            \centering
            \caption*{\usebeamercolor[fg]{frametitle}\large S\&P 500 Log-Return}
            \begin{minipage}{.49\textwidth}
                \centering
                \subcaption*{$1$-Second}
            \end{minipage}%
            \begin{minipage}{.49\textwidth}
                \centering
                \subcaption*{Daily}
            \end{minipage}
            \includegraphics[width=\textwidth, height=.5\textheight]{spy_log_rtn.pdf}
            \caption*{\centering  \textcolor{red}{Red} lines are large jumps.} 
     \end{figure}%
     \vspace{-.5\baselineskip}

     \large 

     How do \red{jumps} affect the investors' \green{time-varying risk}? 

     \vspace{\baselineskip}

\end{frame}



\begin{frame}[c]{Jumps $\equiv$  Discontinuities in the Price Process. }

    \vspace{\baselineskip}
    {\usebeamercolor[fg]{frametitle} What are Jumps? }
    \vspace{.5\baselineskip}

    \begin{itemize}
        \item Price changes caused by discrete (possibly small) releases of information. 
            \vspace{.25\baselineskip}
        \item They may be observed by only a few investors. 
           \vspace{.25\baselineskip}
       \item{They may come at unpredictable times.}
           \vspace{.25\baselineskip}
       \item \textbf{Examples:}
            \vspace{.25\baselineskip}
            \begin{itemize}
                \item[-] FOMC Announcements.
                \item[-] A startup announcing a new product line. 
                \item[-] Effectively anything in a Bloomberg or Associated Press feed relevant for asset pricing.
                \item[-] Private communications between investors. 
            \end{itemize}
    \end{itemize}

    \pause

    \vspace{\baselineskip}
    {\usebeamercolor[fg]{frametitle} Jumps Matter.}
    \vspace{.5\baselineskip}

    \begin{enumerate} 
        \item CAPM fails when preferences are not time-separable if prices jump \newline
            \parencite[WP]{ai2018risk, tsai2017pricing}.  
            \vspace{.25\baselineskip}
        \item Jumps are very frequent / infinitely-active \parencite{aitsahalia2012analyzing}.
            \vspace{.25\baselineskip}
        \item Jumps matter for pricing. \parencite{backus2011disasters, todorov2011econometric}.
    \end{enumerate} 

\end{frame}


\begin{frame}[c]{Contributions}                                                                                       
    \begin{enumerate}
        \item[\large 1.] Propose a measure of \red{\qt{jump volatility}} 
            \begin{enumerate} 
                \item[a)] that yields tractable, nonparametric expression for the return's time-varying
                    distributions 
                \item[b)] measures jumps in investor's information \green{(news)}       
                \item[c)] is the \textquote{other} priced factor in a CAPM with time-inseparable preferences.
            \end{enumerate}
            \vspace{.5\baselineskip}

            \pause
            
        \item[\large 2.]  
            \begin{enumerate}
                \item[a)] Show that by conditioning on the return's volatilities, no-arbitrage implies this
                    expression holds exactly.
                \item[b)]Build an estimation and forecasting framework for the return's volatilities and
                    distributions. 
            \end{enumerate} 
            \vspace{.5\baselineskip}

            \pause

        \item[\large 3.]
            \begin{enumerate}
                \item[a)] 
                    Show that this representation works well in practice, providing excellent tail-risk
                    estimators. 
                \item[b)]   
                    Show that news risk commands a lower risk premium than diffusive risk does.
            \end{enumerate} 
            \vspace{\baselineskip}
    \end{enumerate}
\end{frame}                                                                                                        



\begin{frame}[c]{Data Generating Process}

    \centering
    \begin{figure}[h]
        \caption*{\usebeamercolor[fg]{frametitle} \large Price Process \only<1-2|handout:0>{(Stochastic
            Volatility} \only<2|handout:0>{Jump} \only<1-2|handout:0>{Diffusion)} \only<3-|handout:1>{(My
        Representation)} \vspace{\baselineskip}}
%
        \begin{tikzpicture} 
            \node (a) at (-1.27,1) {\large $ \displaystyle \purple{\dif p(t)} = \mu(t) \dif t \, + \!\!\!
                \underbrace{\blue{\sigma(t) \dif W(t)}}_{\text{Diffusion Part}\ \blue{(\dif p^D)}}$};
            \node (j) at (a.north west) [above right = 2.35em and 2em] {\footnotesize Drift};
            \draw[->, thick] (j)  -- ([xshift=-3em] a.north); 
            \node (e) at (a.north) [above=2.2em] {\footnotesize Diffusion Volatility};
            \node (f) at ([xshift=-2em] a.north east) [above=1em] {\footnotesize Wiener Process};
            \draw[->, thick] (e)  -- ([xshift=2em, yshift=-.2em] a.north); 
            \draw[->, thick] (f)  -- ([xshift=-3em] a.north east); 
            \onslide<2|handout:1>{
                \node (b) at (a.east) [right=0em]  {\large $\displaystyle \!\!\! + \, \underbrace{\red{\int_{X}
                    \delta(t,x) (\mu - \nu) (\dif t, \dif x)}}_{\text{Jump Part}\ \red{(\dif p^J)}}$};
                \node (c) at ([xshift=2.5em] b.north west) [above=1.5em] {\footnotesize Jump Magnitude Function};
                \draw[->, thick] (c.south) -- ([xshift=-3.8em, yshift=-.5em] b.north); 
                \node (d) at (b.north east) [above=1.5em] {\footnotesize Demeaned Jump Locations};
                \draw[->, thick] (d.south west)  -- ([xshift=1em, yshift=-1em]b.north); 
            }
            \onslide<3-|handout:0>{
                \node (g) at (a.east) [anchor=right, above right = -2.6em and 0em]  {\large $\displaystyle \!\!\! +
                        \, \Ccancel[hookersgreen]{\red{\frac{\gamma(t)}{\sqrt{2}} \dif
                        \Lap(t)}}{\underbrace{\int_{X} \delta(t, x) (\mu - \nu) (\dif t, \dif
                        x)}_{\text{Jump Part}\ \red{(\dif p^J)}}}$};
                \node (h) at (g.north) [above=1em] {\footnotesize Jump Volatility};
                \node (i) at (g.north east) [above=1em] {\footnotesize Variance-Gamma Process};
                \draw[->, thick] (h.south east) -- ([xshift=4.5em] g.north); 
                \draw[->, thick] (i.south) -- ([xshift=-2em, yshift=-.25em] g.north east); 
            }
        \end{tikzpicture} 
    \end{figure}

    \onslide<4-|handout:1> {
        \begin{figure}[h]
            \caption*{\usebeamercolor[fg]{frametitle} \large Returns }
            \begin{tikzpicture}  
                \node at (1,1) {\large $\displaystyle \purple{r_t} \equiv \int_{t-1}^t \dif p(s), \qquad \left. 
                    \purple{r_t} \mvert \F_{t-1} \right. \sim h\left(\purple{r_t} \mvert \F_{t-1}\right)$};
            \end{tikzpicture}
        \end{figure}
    }
\end{frame}



\begin{frame}[c]{What are Jump Volatility and the Realized Density?}
 
    \begin{table}[t]
        \caption*{\usebeamercolor[fg]{frametitle} \large Two Kinds of Volatility:}

        \begin{tabularx}{\textwidth}{X X} Diffusion Volatility & \onslide<2->{Jump Volatility} \\ \\
            $\blue{\sigma^2(t)} \equiv \lim\limits_{\Delta \to 0} \frac{1}{\Delta} \E\left[
               \abs*{\int_{t}^{t + \Delta} \dif p^{D} (s) }^2\, \mvert \F_{t}\right]$ 
            & \onslide<2->{$\red{\gamma^2(t)} \equiv \lim\limits_{\Delta \to 0} \frac{1}{\Delta} \E\left[
              \abs*{\int_{t}^{t + \Delta} \dif p^{J} (s) }^2\, \mvert \F_{t} \right]$}
        \end{tabularx}
    \end{table}

    \begin{figure}[h]
        \onslide<3->{\caption*{\usebeamercolor[fg]{frametitle} \large Realized Density:}}
        \begin{tikzpicture}
            \onslide<3->{
                \node at (-6.3,0) {\purple{$RD_t$}}; 
                \node at (-3,-.22) {$\displaystyle \equiv f\left(r_t\mvert \sigma^2_t, \gamma^2_t \right) =
                        f\Bigg\rvert_{x_t = \begin{bmatrix} \int_{t-1}^t \sigma^2(s) \dif s \\ \int_{t-1}^t
                    \gamma^2(s) \dif s \end{bmatrix}}$}; 
            }
            \onslide<4->{
                \node at (3.7, 1.2) {\footnotesize Convolution};
                \draw[->, thick] (3.7, .95) -- (3.7, .25); 
                \node at (1.7, 0) {$\displaystyle = \blue{\N}\left(0, \int_{t-1}^t \sigma^2(s) \dif
                    s\right)$}; 
                \node at (3.7, 0) {$\ast$}; 
                \node at (5.4,0) {$\displaystyle \red{\Lap}\left(0, \int_{t-1}^t \gamma^2(s) \dif
                    s\right)$}; 
                \node at (1.7, -.9) {\footnotesize Laplace Distribution};
                \draw[->, thick] (3.1, -.8) -- (4, -0.2); 
            }
        \end{tikzpicture}

        \vfill \hfill \hyperlink{decomposition_of_gamma}{\beamerbutton{Details}}
    \end{figure}

\end{frame}

   

\begin{frame}[c, label=gamma_estimator_intuition]{Estimating $\blue{\sigma_t^2}$, $\red{\gamma^2_t}$, and
    $\purple{RD_t}$.}
    
    \vspace{.5\baselineskip}
    \begin{itemize}
        \item Estimation Strategy:
        \begin{enumerate}
            \item Adapt a truncation-based estimator from the literature to estimate \blue{$\sigma^2(t)$}.
            \item Use representation-implied moment conditions to estimate $\red{\gamma^2(t)}$.
            \item Solve for the implied  $\blue{\sigma_t^2}$, $\red{\gamma^2_t}$, and $\purple{RD_t}$.
        \end{enumerate}

        \pause

        \vspace{.5\baselineskip}
        \item Data:
        \begin{itemize}
            \item[--] \green{SPY S\&P 500 ETF}  : January 2003 -- September 2017
            \item[--] Tick data from TAQ sampled at \num[scientific-notation=false]{180} observations per minute
                ($\approx 300$ million observations).
        \end{itemize}
    \end{itemize}

    \pause

    \begin{figure}[t]
        \vspace{-.75\baselineskip}
        \includegraphics[width=.75\textwidth, height=.5\textheight]{volatilities.pdf}
    \end{figure}%

     \hyperlink{gammaestimate}{\beamerbutton{Theorems}}

\end{frame}


\begin{frame}[c, label=DensityForecast]{Density Forecast (The Model Works Great in Practice!)}

    \begin{figure}[h]
        \begin{subfigure}[t]{.49\textwidth}
            \caption*{\centering \qquad Jump-Diffusion Forecast \newline \vspace{-\baselineskip}}
            \includegraphics[width=\textwidth, height=.35\textheight]{ar_tv_lp_forecast.pdf}  
        \end{subfigure}% 
        %      
        \begin{subfigure}[t]{.49\textwidth}
            \caption*{\centering \textcite{bollerslev2009discrete} \newline (Slightly Simplified)
            \vspace{-\baselineskip}}
            \includegraphics[width=\textwidth, height=.35\textheight]{boller2009_forecast.pdf} 
        \end{subfigure}
        \begin{subfigure}[t]{.49\textwidth}
            \includegraphics[width=\textwidth, height=.35\textheight]{ar_tv_lp_qq.pdf} 
        \end{subfigure}%
        \begin{subfigure}[t]{.49\textwidth}
            \includegraphics[width=\textwidth, height=.35\textheight]{boller2009_qq.pdf} 
        \end{subfigure}
    \end{figure}

    \hyperlink{ForecastingModel}{\beamerbutton{Details}}
 
\end{frame}




\begin{frame}[c, label=contemp_jump_prop_graph]{$\purple{\rxt} = \beta_0 + \beta_1\, \mathbb{I}(\green{FOMC}) +
    \beta_2 \purple{\log \left(\sigma^2_t + \gamma^2_t\right)} + \beta_3 \red{\log
    \left(\frac{\gamma^2_t}{\sigma^2_t + \gamma^2_t}\right)} + \epsilon_t$}
    
    \begin{table}[htb]
        \caption*{\large Coefficients}
        \sisetup{table-align-text-pre=false, table-align-text-post=false, round-mode=places,
                 scientific-notation=false, table-space-text-pre=\lbrack, table-space-text-post=\rbrack}
        \centering
    
        \begin{tabularx}{5.25in}{*{3}{S[table-format=2.2, round-precision=2]} S[table-format=1.3,
            round-precision=3] | *{3}{S[table-format=2.2, round-precision=2]}}
            \toprule
            {\green{FOMC}} & {$\purple{\log \left(\sigma^2_t + \gamma^2_t\right)}$} & {$\red{\log
            \left(\frac{\gamma^2_t}{\sigma^2_t + \gamma^2_t}\right)}$} & {$\bar{\R}$} & {\green{FOMC}} &
            {$\purple{\log \left(\sigma^2_t + \gamma^2_t\right)}$} & {$\red{\log
            \left(\frac{\gamma^2_t}{\sigma^2_t + \gamma^2_t}\right)}$} \\     
            \midrule
            \multicolumn{4}{c}{Weighted Least Squares} & \multicolumn{3}{c}{Instrumental Variables Regression} \\
            \midrule
            & -0.2576 &  & 0.0135 & & 0.3056  \\  {}
            & [-5.5825] & &  & & [6.0429]  \\ 
            & -0.2306 & 0.5065 & 0.0156  & & & -3.1153  \\ {}
            & [-4.7953] & [2.3644] & & & & [-6.5727] \\
            0.4447 & {\cellcolor{matplotlib_purple!50} \num{-0.2385}} & {\cellcolor{red!50} \num{0.5292}} & 0.0174
                   & 0.0889 & {\cellcolor{matplotlib_purple!50} \num{0.1854}} & {\cellcolor{red!50} \num{-2.2089}}
                     \\  
            {\cellcolor{green!50} [\num{4.8561}]} & [2.4720] & [2.6157] &  &  {\cellcolor{green!50} [\num{.5264}]}
                                                  & [3.2835] & [-4.4535] \\ 
            \bottomrule
        \end{tabularx}
    \end{table}

\end{frame}




\begin{frame}[c]{Recap}                                                                                       
    \begin{enumerate}
        \item[\large 1.] Propose  $\jumpvolt$ .
            \begin{enumerate} 
            \item[a)] It yields tractable, nonparametric expression for $h\left(\rxt \mvert \F_{t-1}\right)$.
                \item[b)] It measures jumps in investor's information \green{(news)}       
                \item[c)] It is the \textquote{other} priced factor in a CAPM with time-inseparable preferences.
            \end{enumerate}
            \vspace{.5\baselineskip}

        \item[\large 2.]  
            \begin{enumerate}
                \item[a)] Show no-arbitrage implies this expression holds exactly.
                \item[b)] Build an estimation and forecasting framework for \jumpvolt, \diffvolt, and 
                    $\purple{RD_t}$. 
            \end{enumerate} 
            \vspace{.5\baselineskip}

        \item[\large 3.]
            \begin{enumerate}
                \item[a)] 
                    Show that using the method works well in practice, providing excellent tail-risk estimators. 
                \item[b)]   
                    Show \jumppropt\ commands a negative risk premium. 
            \end{enumerate} 
            \vspace{\baselineskip}
    \end{enumerate}
\end{frame}                                                                                                        
                                                                                                                   
\appendix                                                                                                                    
                                                                                                                    
\begin{frame}[c, label=decomposition_of_gamma]{Decomposition of $\gamma^2_t$ in Compound Poisson Case}

    \begin{alignat*}{2}
        & \red{\gamma^2_t} &&= \lim_{\Delta \to 0} \frac{1}{\Delta} \E\left[\,\abs*{\red{\int_{t}^{t +
    \Delta} \dif p^{J} (s)^2}} \mvert \F_{t}\right]  \\ \vspace{.5\baselineskip}
        & &&= \lim_{\Delta \to 0} \frac{1}{\Delta} \E\left[ \E\left[ \sum_{s \in (t, t+\Delta]}(\text{jump
          magnitude})^2(s)) \mvert \F_{t},  s\ \text{is a jump} \right]\right] \\ \vspace{.5\baselineskip}
        & &&= \lim_{\Delta \to 0} \frac{1}{\Delta} \E\left[(\text{jump magnitude})^2\right]\E\left[\#\ \text{of
          jumps in}\ (t, t+\Delta\right]
        \label{eqn:jump_variance_decomposition}
    \end{alignat*}

    \hyperlink{first_two_moments}{\beamerbutton{Return}}
\end{frame}


\begin{frame}[c, label=timechangecorollaries]{Time-Changed Laplace Corollaries}
        \begin{corollary}
            Let $p(t)t$ be a purely-discontinuous It\^{o} semimartingale that is locally-square integrable and has
            infinite-activity jumps. 
            Then $p(t) = \frac{1}{\sqrt{2}}\int_0^t \gamma(s) \dif\Lap(s)$, where $\Lap$ is a standard variance-gamma
            process, and $\gamma(s) $ is predictable function.  
        \end{corollary}
        \begin{corollary} 
            Let $p(t)$ be a purely discontinuous, locally-square integrable martingale that can be represented as $H
            \ast (\mu - \nu)$ where $H(t)$ is a predictable process, $\mu$ a Poisson random measure, and $\nu$ its
             predictable compensator with Lebesgue base Levy measure $\lambda$. 
             Further assume that it has no predictable jumps. 
             Then $p(t)$ time-changed by its predictable quadratic variation is a mixture of the $0$ process
             $\delta_0$ and the standard variance-gamma process where the mixing weights are determined by the
         intensity of the jump process. 
    \end{corollary}
    \hyperlink{continuous_time_model}{\beamerbutton{Return}}
\end{frame}

                                
\begin{frame}[c, label=gammaestimate]{Estimating $\gamma^2(t)$} 
     \begin{theorem}
    
        Let $p(t)$ be a locally-square integrable infinite-activity It\^{o} semimartingale whose characteristics
        have locally-square integrable c\`{a}dl\`{a}g densities.
        Let $k_n \to \infty$, $k_n \sqrt{\Delta_n} \to 0$, and $0 < T < \infty$ be a deterministic time.
        Define $i_n = i - k_n -1$. 
        Let $\hat{\sigma}_{T-}$ converge in probability to $\sigma_{T-}$. 
        Let $\gamma(t) > 0$ for all $t$, then we have the following.
    
        \begin{equation*}
            \hat{\gamma} \equiv \argmin_{\gamma} \abs*{\frac{1}{k_n \sqrt{\Delta}}
            \sum_{m=0}^{k_n-1} \abs*{\Delta^n_{i_n +m} X}  - \E\abs*{\N(0,1)} \hat{\sigma}_{T-} -
            \frac{\gamma}{\sqrt{2}} \erfcx\left(\frac{\hat{\sigma}_{T-}}{\gamma}\right)} \pto \gamma_{T-}
        \end{equation*}

     \end{theorem}
     \hyperlink{gamma_estimator_intuition}{\beamerbutton{Return}}
\end{frame}


\begin{frame}[c, label=localabsvar]{Local Absolute Variation}
     \begin{theorem}
        Let $p(t)$ be a locally-square integrable infinite-activity It\^{o} semimartingale whose characteristics
        have locally-square integrable c\`{a}dl\`{a}g densities.
        Let $k_n \to \infty$ and $k_n \sqrt{\Delta_n} \to 0$, and $O < T < \infty$ be a deterministic time.
        Define $i_n = i - k_n -1$.

        \begin{equation*}
            \frac{1}{k_n \sqrt{\Delta_n}} \sum_{m=0}^{k_n-1} \abs{r_{i_n, \Delta^n}}  \pto \E\abs*{\N(0,1)}
            \sigma_{T-} + \erfcx\left(\frac{\sigma_{T-}}{\sqrt{2} \gamma_{T-}}\right) \gamma_{T-} 
        \end{equation*}

    \end{theorem}
    \hyperlink{gamma_estimator_intuition}{\beamerbutton{Return}}
\end{frame}


\begin{frame}[c, label=news_premia_thm]{News Premium Puzzle}

    \begin{theorem}
        Let $f(Z)  \coloneqq \mathcal{I}\left[\frac{V(Z)}{\E\left[ D I(V(Z) D V(Z) \mvert \F_{t-1}\right]}\mvert
            \F_{t-1}\right]$ be the normalized certainty equivalence functional.
        Assume that $f$ is twice-continuously differentiable. 
        Let $m(t)$ and $m_{*}(t)$ be the implied SDF and A-SDF.
        Let consumption be a continuous process and utility be a smooth function of consumption.
        Then we have the following, where $\tilde{Z} = \sum_{i} Z_i$, and $\int_0^t \mu(s) \dif s$ is a drift.

       \begin{equation*}
           \mathcal{I}(t) =  \int_0^t b(s) \dif s + \E\left[\int_0^t M(s) \dif \tilde{Z}^D(s) +\int_0^t  M(s)
           M_{*}(s) \dif \tilde{Z}^J(s) \mvert \F_{t-} \right] 
       \end{equation*}
    \end{theorem}

    \vspace{.5\baselineskip}

    \begin{corollary}[Generalized Risk Sensititivy $\implies$ News Premia]
        
        Under some technical conditions, the jump risk premium is proportional to the following. 

        \begin{equation*}
            \mu(t) = \left[D^2 \mathcal{I}(V(\tilde{Z}(t-))) D V(t-) + D \mathcal{I}( V(\tilde{Z}(t-))) D^2
            V(\tilde{Z}(t-))\right] \gamma^2(t) - \left[D^2 V(\tilde{Z}(t-)) \right] \sigma^2(t) 
        \end{equation*}

    \end{corollary}

\vfill \hfill \hyperlink{news_premia_claim}{\beamerbutton{Return}}
\end{frame}

\begin{frame}[c, label=representation_thm]{Representation Theory}

    \begin{theorem}
        Let $p(t)$ be a purely discontinuous, infinite-activity, locally-square integrable martingale that
        can be represented as $H \ast (\mu - \nu)$ where $H(t)$ is a predictable process, $\mu$ a Poisson random
        measure, and $\nu$ its predictable compensator with Lebesgue base L\'{e}vy measure $\lambda$. 
        Further assume that it has no predictable jumps. 
        Then $p(t)$ time-changed by its predictable quadratic variation is a standard variance-gamma process.  
        In other words, $p(\langle p \rangle^{-1}(t)) \lequals \Lap(t)$.  
    \end{theorem}

\vfill \hfill \hyperlink{timechangetheorem}{\beamerbutton{Return}}
\end{frame}


\begin{frame}[c, label=ForecastingModel]{Forecasting Model}
        \sisetup{
            table-align-text-pre=false,
            table-align-text-post=false,
            round-mode=places,
            scientific-notation=false,
            table-space-text-pre=\lbrack,
            table-space-text-post=\rbrack,
        }

    \begin{block}{}
        \begin{equation*}
            \begin{matrix} \log \left( \totalvol{t} \right) \\ \logit \left( \jumppropt\right) \end{matrix} =
            \begin{pmatrix}  \num{-0.418941} \\ \num{-0.3457870} \end{pmatrix} + \begin{pmatrix} \num{0.884878} &
            \num{-0.063308} \\ \num{-0.106548} & \num{0.317035} \end{pmatrix} \begin{matrix} \log
            \left(\totalvol{t-1}\right) \\ \logit \left(\jumpprop{t-1}\right)  \end{matrix} + \begin{pmatrix}
            \eta_t \\ \epsilon_t \end{pmatrix}
        \end{equation*}
    \end{block}
    \pause

    \begin{block}{}
        \begin{equation*}
            \rxt = \exp\left(\num{-3.143628} + \num{0.288774} \log \left(\totalvol{t-1}\right) - \num{0.630475}
            \logit \left(\jumpprop{t-1}\right)\right)  + \blue{\sigma_t} u_t + \frac{\red{\gamma_t}}{\sqrt{2}} v_t 
        \end{equation*}
    \end{block}
    \pause

    \begin{minipage}{.48\textwidth}
        \begin{block}{}
            \vspace{-.5\baselineskip}
            \begin{equation*}
                \begin{pmatrix} \eta_t \\ \epsilon_t \\ u_t \end{pmatrix} 
                \sim \N\left(\begin{pmatrix} 0 \\ 0 \\ 0 \end{pmatrix},
                \begin{pmatrix} \num{0.193083} & \num{-0.020788} & \num{-0.141714} \\ \num{-0.020788} &
                    \num{0.092860} & \num{0.031830} \\ \num{-0.141714} & \num{0.031830} & \num{0.719267}
                \end{pmatrix}\right) 
            \end{equation*}
        \end{block}
    \end{minipage}\hfill
   % 
    \begin{minipage}{.47\textwidth}
        \begin{block}{}
            \centering
            \begin{equation*} v_t \sim \Lap(0,1) \end{equation*}
            \vspace{.75\baselineskip}
        \end{block}
    \end{minipage}

\vfill \hfill \hyperlink{DensityForecast}{\beamerbutton{Return}}

\end{frame}

\end{document}

