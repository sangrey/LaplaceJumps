"""This module provides a series of functions to construct regressions."""
from itertools import product, combinations
import pandas as pd
import numpy as np
import sklearn.preprocessing as skprocess
import statsmodels.api as sm


def bin_data(data, quantiles, exog, endog):
    """
    Split the data into bins of bases on the quantiles applied to the exog variable.

    For example, quantiles = [.5, 1], splits the data into 4-bins based upon the column-wise medians in the
    columns specified by exog.

    Parameters
    ----------
    data: pandas dataframe
    quantiles : array-like
    exog : array-like of str
    endog : str

    Returns
    ------
    pandas dataframe

    """
    column_dim = data.shape[1]
    data_to_append = np.atleast_2d([np.inf] * column_dim)

    quantile_data = data.quantile(quantiles)
    appended_quantile_data = np.append(-data_to_append, np.append(quantile_data.values, data_to_append, axis=0),
                                       axis=0)
    appended_quantile_data = pd.DataFrame(appended_quantile_data, columns=data.columns,
                                          index=([0] + list(quantiles) + [1]))

    return_list = []
    arg0_iterator = zip(appended_quantile_data[exog[0]].iteritems(),
                        appended_quantile_data[exog[0]].iloc[1:].iteritems())
    arg1_iterator = zip(appended_quantile_data[exog[1]].iteritems(),
                        appended_quantile_data[exog[1]].iloc[1:].iteritems())

    eqn1 = """(@low_arg0 < {x}) and (@high_arg0 >= {x}) """.format(x=exog[0])
    eqn2 = """and (@low_arg1 < {y}) and(@high_arg1 >= {y})""".format(y=exog[1])

    for arg0, arg1 in product(arg0_iterator, arg1_iterator):
        _, low_arg0 = arg0[0]
        arg0_idx, high_arg0 = arg0[1]

        _, low_arg1 = arg1[0]
        arg1_idx, high_arg1 = arg1[1]

        return_list.append((arg0_idx, arg1_idx, data.query(eqn1 + eqn2)[endog].mean()))

    rtn_data = pd.DataFrame.from_records(return_list, columns=exog + [endog]).pivot_table(
        columns=exog[0], values=endog, index=exog[1])

    return rtn_data


def poly_column_iterator(data, max_degree=2):
    """
    Create the iterable of the tensor-product of the columns in the data.

    Parameters
    ---------
    data: pandas series
    max_degree: optiona, int

    Yields
    ------
    pandas series

    """
    return_data = skprocess.PolynomialFeatures(max_degree, include_bias=True).fit_transform(pd.DataFrame(data))

    columns_it = (val for r in range(max_degree + 1) for val in combinations(np.arange(1, max_degree + 1), r))

    for indices in columns_it:
        index_arr = np.array([0] + list(indices))
        yield index_arr, return_data[:, index_arr]


def create_polynomial_regressors(data, powers):
    """
    Create the polynomial regressors using the data with the specified powers.

    Parameters
    ---------
    data : arraylike
    powers : arraylike

    Returns
    ------
    ndarray

    """
    arr = np.atleast_2d(data.T).T
    return np.column_stack([np.prod([arr[:, n]**row[n] for n in range(len(row))], axis=0) for row in powers])

    arr = np.asanyarray(data)
    return np.column_stack([np.prod([data[:, n]**row[n] for n in range(len(row))], axis=0) for row in powers])


def get_minimum_ic_regressors(endog, exog, degree=3, weights=None, criterion='bic'):
    """
    Compute the indices of the the columns that give the minimum information critertion value.

    Parameters
    --------
    endog : pandas dataframe
    exog : pandas dataframe
    weights : optional, array-like
        The the weights in a weighted least squares regression.
    criterion : optiona, str
        The information criterion to use. It must be supported by statsmodels linear leaast squares estiamtors.

    Returns
    ------
    dataframe:
        Returns the columns from the tensor product that give the minimum information criterion value.
    array:
        The powers used.

    """
    ic = []
    for indices, regressors in poly_column_iterator(exog, max_degree=degree):
        if weights is None:
            model = sm.OLS(endog=endog, exog=regressors).fit()
        else:
            model = sm.WLS(endog=endog, exog=regressors, weights=weights).fit()

        ic.append((indices, getattr(model, criterion)))

    ic_vals = [val for _, val in ic]
    indices = ic[np.argmin(ic_vals)]

    powers = skprocess.PolynomialFeatures(degree, include_bias=True).fit(exog).powers_[indices[0], :]
    return_data = create_polynomial_regressors(exog, powers)

    return return_data, powers
