\section{News Premia Theorems}

\phantomsection
\addcontentsline{toc}{subsection}{An \Ito's Formula for the Expectation of a Square Integrable Semimartingale}

\genItosLemma*

\begin{proof}

The argument below is a standard application of It\^{o}'s formula for non-continuous processes applied to processes of bounded variation.  In addition, the notation below should be interpreted in vector form.  For example, $\dif \tilde{Z}(t)$ is the vector of $\dif Z_i$ for all $i$, and $\langle \tilde{Z}^D \rangle$ is a matrix.  We start by writing expanding the differential inside the expectation using It\^{o}'s formula for non-continuous semimartingales, \parencite[Theorem 6.46]{medvegyev2007stochastic}:  
%
    \begin{alignat}{2}
        \phantom{=} \dif \E\left[ f(\tilde{Z}(t)) \mvert \F_{t-}\right]  
%
        & &&= \dif \E\left[ \sum_{i=1}^d \frac{\partial f}{\partial z_i} \tilde{Z}(t-)) \dif \tilde{Z}_i(t) 
            + \frac{1}{2} \sum_{i,j} \frac{\partial f}{\partial z_i \partial z_j}
            f(\tilde{z}(t-)) \predQV*{\tilde{z_i}^D, \tilde{z_j}^D}(t)  \right. \\ 
        & &&\quad + \left. \left(\Delta f(\tilde{Z}(t))  - \sum_{i=1}^d \frac{\partial f}{\partial z_i}
          f(\tilde{Z}(t-)) \Delta \tilde{Z}_i(t)\right)  \mvert \F_{t-} \right].  \nonumber \\   
%
         \intertext{Rearranging and combining terms, we have.}
%
        & &&= \dif \E\left[  f'(\tilde{Z}(t-)) \dif \tilde{Z}(t) + \left(\Delta f(\tilde{Z}(t))  + f'(\tilde{Z}(t-)) \Delta \tilde{Z}(s)\right) \right.  \\
%
        & &&\quad - \left. \frac{1}{2} f''(\tilde{Z}(t-)) \predQV*{\tilde{Z}^D}(t) \mvert \F_{t-} \right]. \nonumber \\
%
        \intertext{Then by Taylor's theorem, canceling terms and noting that continuity implies bounded for the
            derivatives of $f$ as long as $\tilde{Z}$ is bounded:}
%
        & &&= \dif \E\left[f'(\tilde{Z}(t-)) \dif \tilde{Z}(t)  + \frac{1}{2} f''(\tilde{Z}(t-)) \dif \predQV*{\tilde{Z}^D} (t) \mvert \right]  \\
%
        & && \quad+ \frac{1}{2} \dif \E\left[f ''(\tilde{Z}(t-)) \Delta \tilde{Z}(t)^2 + O((\Delta \tilde{Z}(t)^3) \mvert \F_{t-} \right]. \\
%
        \intertext{Since the quadratic variation and the predictable quadratic variation coincide for continuous processes:}  
%
        & &&= \dif \E\left[f'(\tilde{Z}(t-)) \dif \tilde{Z}(t) + \frac{1}{2} f''(\tilde{Z}(t-)) \dif [\tilde{Z}] (t) \mvert \F_{t-1}\right] \\
        & &&+ \E\left[ O((\Delta \tilde{Z}(t)^3) ) \mvert \F_{t-} \right]. \\ 
%
        \intertext{By the Davis-Burkholder-Gundy inequality, for some constant $c$:}
%
        & &&= \dif \E\left[f'(\tilde{Z}(t-)) \dif \tilde{Z}(t) \mvert \F_{t-1}\right] + \frac{1}{2} \E\left[ f''(\tilde{Z}(t-)) \dif [\tilde{Z}] (t) \mvert \F_{t-} \right] \\
%
        & &&\quad + \E\left[ c_1 O([\tilde{Z}]^{3/2} ) \mvert \F_{t-} \right]. \nonumber \\
%
        \intertext{Since we are considering local changes in time,}
%
        & &&= \dif \E\left[f'(\tilde{Z}(t-)) \dif \tilde{Z}(t) \mvert \F_{t-}\right]  
        + \frac{1}{2} f''(\tilde{Z}(t-)) \dif \predQV*{\tilde{Z}} (t). 
%
    \end{alignat}

\end{proof}

\phantomsection
\addcontentsline{toc}{subsection}{Asset-Pricing Equation}

\assetPricingEqn*

\begin{proof}

Define the discounted price: $\PP(t) \coloneqq \exp(-\kappa t) P(t)$.  This is a concave maximization problem and so first-order conditions characterize the optimum.  Assume, for now, that the investor can only adjust his portfolio at a discrete grid of points $t, t+\Delta, t+2\Delta, \ldots$ Then consumption and prices are effectively constant within each period, and the investor is faced with the following problem:
%
    \begin{gather}
        V(W(t)) = \max_{\Xi(t), C(t)} u(C(t)) + \exp(-\kappa \Delta)
        \phi^{-1}\left(\left[\phi\left(V(W(t+\Delta))\right) \mvert \F_{t}\right]\right)   \\ %
        C(t) + \sum_{i} P_i(t) \xi_{i}(t) = W(t) \\
%
        W(t+\Delta) = \sum_i P_i(t+\Delta) \xi_{i}(t)
    \end{gather}

    Submitting in the constraints gives 
%
    \begin{equation}
        \label{eqn:simplified_max_problem}
        V(W(t)) = \max_{\Xi(t)} u(W(t) - \sum_{i} P_{i}(t+\Delta) + \exp(-\kappa \Delta)
            \phi^{-1}\left(\left[\phi\left(V(\sum_i P_i(t+\Delta) \xi_{i}(t))\right) \mvert \F_{t}\right]\right).
    \end{equation}
%
%
The discounted and original prices coincide at $t$, and we can equate $\exp(-\kappa \Delta) P_i(t+\Delta)$  with $\PP_i(t+\Delta0$.  Hence, by using chain rule, and the formula for the derivative of an inverse, the first-order condition for \cref{eqn:simplified_max_problem} is 
%
    \begin{equation}
        \label{eqn:FOC}
        u'(c(t)) \PP_i(t) = \E\left[\frac{\phi'(V\left(W(t+\Delta)\right)
            }{\phi'\left(\phi^{-1}\left(\E\left[\phi(V(W(t+\Delta))) \mvert \F_{t}\right]\right)\right)}
        V'\left(W(t+\Delta)\right) \PP_i(t+\Delta) \mvert \F_{t} \right],
    \end{equation}
%
    at the optimal level of consumption and optimal asset shares.
    % Note, the first term is $\E[\phi'(V(t+\Delta)) \ivert \F_t]$ divided through by $\phi'(\E[V(t+\Delta) \ivert
    % \F_t])$, by the definition of $V$ as long as the choice variables are chosen appropriately.
    We can rearrange \cref{eqn:FOC} as: 
%
    \begin{equation}
        \label{eqn:total_sdf_1}
        \PP_i(t) = \E\left[\frac{\phi'(V\left(W(t+\Delta)\right)
            }{\phi'\left(\phi^{-1}\left(\E\left[\phi(V(W(t+\Delta))) \mvert \F_{t}\right]\right)\right)}
        \frac{V'\left(W(t+\Delta)\right)}{u'(c(t))} \PP_i(t+\Delta) \mvert \F_{t} \right].
    \end{equation}
%
    If we plug in the risk-free rate $\PP_f(t)$ in to \cref{eqn:total_sdf_1}, the prices on each side of
    the equal side are the same, and we can divide through by them. 
    This gives 
%
    \begin{equation}
        1  = \E\left[\frac{\phi'(V\left(W(t+\Delta)\right)
            }{\phi'\left(\phi^{-1}\left(\E\left[\phi(V(W(t+\Delta))) \mvert \F_{t}\right]\right)\right)}
        \frac{V'\left(W(t+\Delta)\right)}{u'(c(t)) } \mvert \F_{t} \right].
    \end{equation}
%
    In other words, the two terms in the inside the expectation are a martingale.
    Consequently, prices are a martingale with respect to the change of measure they induce.
    We can take limits as $\Delta \to 0$ in \cref{eqn:total_sdf_1},  which gives
%
    \begin{equation}
        \PP_i(t) = \E\left[\frac{\phi'(V\left(W(t)\right) }{\phi'\left(\phi^{-1}\left(\E\left[\phi(V(W(t))) \mvert
        \F_{t-}\right]\right)\right)} \frac{V'\left(W(t)\right)}{u'(c(t-))} \PP_i(t) \mvert \F_{t-} \right]
    \end{equation}
%
    Now, we want to separate  these two terms into a pure jump component and the remainder. 

    To do this, multiply and divide the first expression by $\phi'(V\left(W(t-)\right) $:
%
    \begin{equation}
        \PP_i(t) = \E\left[\frac{\phi'(V\left(W(t)\right)}{\phi'(V\left(W(t-)\right) }
                \frac{\phi'(V\left(W(t-)\right) }{\phi'\left(\phi^{-1}\left(\E\left[\phi(V(W(t))) \mvert
            \F_{t-}\right]\right)\right)} \frac{V'\left(W(t)\right)}{u'(c(t-))} \PP_i(t) \mvert \F_{t-} \right]
    \end{equation} %
    Note, the first term here is simply $M^{UP}(t)$.
    Claim: $M^{UP}(t)$  is purely discontinuous.
    By \textcite[Theorem 1]{ai2018risk}, we know that the value function is a differentiable, and hence
    continuous, function of wealth.
    In addition, I am taking limits locally in time, and  $\phi'$ is strictly positive.
    Consider $\lim_{\Delta  \to 0} M^{UP}(t - \Delta)$.

    \begin{align}
        \lim_{\Delta \to 0}
        \frac{\phi'\left(V\left(W(t-\Delta)\right)\right)}{\phi'\left(V\left(W((t-\Delta)-)\right)\right)}
%
        &= \frac{\lim_{\Delta \to 0} \phi'\left(V\left(W(t-\Delta)\right)\right)}{\lim_{\Delta \to 0}
        \phi'\left(V\left(W((t-\Delta)-)\right)\right)}
%
        = \frac{\phi'\left(V\left(\lim_{\Delta \to 0} W(t-\Delta)\right)\right)}{\phi'\left(\lim_{\Delta \to 0}
        V\left(W((t-\Delta)-)\right)\right)}  \\
%
        &= \frac{\phi'(V\left(W(t-)\right)}{\phi'(W(t-))}
%
        = 1 \nonumber
    \end{align}
%I

    This implies that $M^{UP}(t)$ is a pure-jump process because its continuous part is identically one.
    In addition, I assumed there were no-predictable jumps, hence any drift (finite-variation, predictable) terms
    in the environment must be continuous.
    Consequently, $M^{UP}(t)$ is a pure-jump martingale.

\end{proof}

\riskPremia*

\begin{proof}

The goal here is to replace the asset pricing equation in \cref{thm:asset_pricing_eqn} with a stochastic logarithm of $P_{i}(t)$.  Let $\MM(\tau) = \exp(-\kappa(\tau)) M(\tau)$ be the discounted stochastic discount factor.  In this derivation, it is more useful to place the deterministic discounting into the discount factor than into the prices.

    Then the asset-pricing equation is:
%
    \begin{align}
        \widetilde{P}_i(t) &= \E\left[ \MM(\tau) M^{UP}(\tau) \widetilde{P}(\tau) \mvert \F_{t} \right]. \\
%
        \intertext{Since $M(t) M^{UP}(t)$ given $\F_t$ equal $1$, we can pre-multiply by it,} 
%
        \label{eqn:asset_pricing_martingale}
        \MM(t) M^{UP}(t) \PP(t) &= \E\left[ \MM(\tau) M^{UP}(\tau) \PP(\tau) \mvert \F_{t} \right]. 
%
    \end{align}

In other words, $\MM(t) M^{UP} P(t)$ is a martingale. This is the standard SDF type result.  Discounted prices are martingales.  I now take the stochastic logarithm of both sides.  Taking the stochastic logarithm (as opposed to the regular logarithm) is useful because it preserves the martingale property.  (The stochastic logarithm --- $\sLog(X)$ --- is the inverse of the Dol\'{e}ans-Dade exponential.)

Before, I do this, it is useful to consider a few of the stochastic logarithms' properties.  First, the following holds: $\sLog(X \cdot Y) = \sLog(X)  + \sLog(Y) + \QV{\sLog(X), \sLog(Y)}$.  We can also handle triple-products.  You just need to apply the expression twice, and note that finite-variation terms do not affect the quadratic variation.
%
    \begin{align}
        \label{eqn:stochastic_log_product}
        \sLog(X \cdot Y \cdot Z) &= \sLog(X) + \sLog(Y) + \sLog(Z) + \QV{\sLog(X), \sLog(Z)} + \QV{\sLog(X),
        \sLog(Z)}  \\
        &\quad + \QV{\sLog(Y), \sLog(Z)} \nonumber
   \end{align}

As noted above, since \cref{eqn:asset_pricing_martingale} is a martingale its stochastic logarithm is as well.
%
    \begin{align}
        0 &= \E\left[ \int_t^{\tau} \dif \sLog\left(M M^{UP} P\right)(s) \mvert \F_{t} \right]. \\
    %
        \intertext{We can expand this equation using \cref{eqn:stochastic_log_product}. We can also replace the
        integrals with differentials without loss of generality because $\tau$ is arbitrary:}
%
        \implies 0   &= \E\Bigg[ \dif \sLog(\MM)(t) + \dif \sLog(M^{UP})(t) + \dif \sLog(P)(t) \\
%
                     &+ \dif \QV{\sLog(\MM), \sLog(P)}(t) + \dif \QV{\sLog(M^{UP}), \sLog(P)}(t)  + \dif \QV{\sLog(\MM), \sLog(M^{UP})}(t) \Bigg\vert \F_{t-} \Bigg] \nonumber \\
    %
        \intertext{The stochastic logarithm equals the regular logarithm up to finite-variation terms:}
    %
        &= \E\left[ \dif \sLog(\MM)(t) + \dif \sLog(M^{UP})(t) + \dif \sLog(P)(t)  \right. \\
%
        &\quad \left. + \dif \QV{\log(\MM), \log(P)}(t) + \dif \QV{\log(M^{UP}), \log(P)}(t)  + \dif \QV{\sLog(\MM), \sLog(M^{UP})}(t) \mvert \F_{t-} \right]. \nonumber \\
%
        \intertext{We can combine $M$ and $M^{UP}$ together:}
%
        \label{eqn:asset_pricing_decomp1}
%
        &= \E\Bigg[ \dif \sLog\left(M  \cdot M^{UP}\right)(t) + \dif \sLog(P)(t) + \dif \QV{\log(\MM), \log(P)}(t) \\
%
        &\quad + \dif \QV{\log(M^{UP}), \log(P)}(t)  \Bigg\vert \F_{t-} \Bigg]. \nonumber
    \end{align}

    The stochastic logarithm satisfies the following stochastic differential equation: 
%
    \begin{equation}
        \sLog(X)(t) = \int_0^t \frac{1}{X(s-)} \dif X(s).
    \end{equation}
%
    Consequently, we can rewrite \cref{eqn:asset_pricing_decomp1} as follows, where I replace the quadratic
    variation terms with predictable quadratic variation terms,
%
    \begin{equation}
        0 = \E\left[ \frac{\dif \left(M  \cdot M^{UP}\right)(t) }{\MM(t-) M^{UP}(t-)} + \frac{\dif P(t)}{P(t-)} \mvert
        \F_{t-} \right] + \dif \predQV{\log(\MM), \log(P)}(t) + \dif \predQV{\log(M^{UP}), \log(P)}(t).
    \end{equation} 

    If $M^{UP}(t)$ is identically $1$, the all of the terms containing it disappear, which gives the standard asset
    pricing equation:
%
    \begin{equation}
        \label{eqn:standard_asset_pricing_eqn}
        \E\left[  \frac{\dif P(t)}{P(t-)} + \frac{\dif \MM(t)}{\MM(t-)} \mvert \F_{t-} \right] = -\dif \predQV*{ m,
        p}(t), 
    \end{equation}
    %
    where $m = \log(M)$. We can ignore the discounting because it only cases a mean shift, and so will not affect
    quadratic covariation terms.

    In the recursive case with jumps through, it is more complicated.
    An announcement SDF term is a pure-jump process so it only have non-zero covariation with the jump part of the
    prices:
%
    \begin{equation}
        \label{eqn:risk_premia_eqn}
        \E\left[ \frac{\dif P(t)}{P(t-)} + \frac{\dif \left(\MM  \cdot M^{UP}\right)(t) }{\MM(t-) M^{UP}(t-)} \mvert
        \F_{t-} \right] = - \dif \predQV{m, p}(t) - \dif \predQV{m^{UP},p}(t), 
    \end{equation}
%
    where $m^{UP}(t) = \log(M^{UP}(t))$.
    Since \cref{eqn:risk_premia_eqn} prices all assets, if we consider a risk-neutral asset, we have all of the of
    the quadratic variation terms equaling  zero:
%
    \begin{equation}
        \frac{\dif P_{f}(t)}{P_{f}(t-)} =  -\E\left[\frac{\dif \left(\MM  \cdot M^{UP}\right)(t) }{\MM(t-)
        M^{UP}(t-)} \mvert \F_{t-}\right].
    \end{equation}
%
    Consequently, the risk premium on a asset $i$ with discounted price $P_i$ is 
% 
    \begin{equation}
        \label{eqn:asset-price_drift}
        \frac{\dif P_{i}(t)}{P_{i}(t-)} - \frac{\dif P_{f}(t)}{P_{f}(t-)} = - \dif \predQV{m, p}(t) - \dif
        \predQV{m^{UP},p}(t)  
    \end{equation}
%
    Since $M^{UP}(t)$ and hence $m^{UP}(t)$ are purely discontinuous processes, the second quadratic variation does
    not depend upon $\diff{p}(t)$.
    That is 
%
    \begin{equation}
        \E\left[\frac{\dif P_{i}(t)}{P_{i}(t-)} - \frac{\dif P_{f}(t)}{P_{f}(t-)} \mvert \F_{t-}\right]  = -
        \dif \predQV{m, \diff{p} + \jump{p}}(t) - \dif \predQV{m^{UP},\jump{p}}(t).  
    \end{equation}
     

\end{proof}

