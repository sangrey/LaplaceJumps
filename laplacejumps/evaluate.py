"""This module provides functions to evaulate density forecasts."""
import numpy as np
import pandas as pd
from functools import partial
import matplotlib as mpl
from scipy import stats
import seaborn as sns
import statsmodels.api as sm


def pit_plot(data, ax=None, pct=.95, **kwargs):
    """
    Plot the data which are assumed to be realizations of a probability integral transform (PIT).

    It also plots the pointwise confidence intervals under the null that the PIT variates are U[0,1].

    Parameters
    ----------
    data : pandas dataframe
    ax : matplotlib axes
    pct : float in [0,1]
    kwargs : parameters to be passed to the seaborn distplot function.

    """
    if ax is None:
        _, ax = mpl.pyplot.subplots()

    # From Scott (1979), it optimal in the Gaussian case.
    opt_num_bins = int(np.round(np.std(data)**(-1) * data.size**(1 / 3) / (2 * 3**(1 / 3) * np.pi**(1 / 6))))
    bins = np.linspace(0, 1, opt_num_bins)
    pit_ci_bottom, pit_ci_top = stats.binom.interval(alpha=pct, n=data.size, p=opt_num_bins**(-1))

    sns.distplot(data, norm_hist=True, kde=False, ax=ax, bins=bins, **kwargs)

    ax.axhline(pit_ci_bottom * (opt_num_bins / data.size), color='red', linestyle='dashed')
    ax.axhline(pit_ci_top * (opt_num_bins / data.size), color='red', linestyle='dashed')
    ax.axhline(1, color='black')
    ax.set_ylim([0, 2])
    ax.set_xlim([0, 1])
    ax.yaxis.set_major_locator(mpl.ticker.FixedLocator([0, .5, 1, 1.5, 2]))


def pit_acf_plot(data, ax=None, nlags=50, pct=.95, fill_between_kwds={}, acf_kwds={}, origin_opts=None):
    """
    Plot the autocorrelation function of the data and the associated Barlett bands.

    Under the null there is no dependence in the data and so the Barlett bands' width is constant.

    Parameters
    ----------
    data : pandas dataframe
    ax : matplotlib axes, optional
    pct : float in [0,1]
    nlags : positive int
        The number of lags to plot
    fill_between_kwds : dict, optional
        Paramters to be passed to Matplotlib's fill_between function to plotting the error bars.
    acf_kwds : dict, optional
        Paramters to be passed to Matplotlib's bar function for plotting the acf lags.
    origin_opts : dict, optional
        This function controls whether to plot the line at the origin. If None, it does not print a line at the
        origin, if it True, it prints it with the default paramters, if a dictionary is passed, it prints one with
        the given options.

    """
    if ax is None:
        _, ax = mpl.pyplot.subplots()

    if not fill_between_kwds:
        fill_between_kwds = {'color': 'black', 'alpha': .25}

    if not acf_kwds:
        acf_kwds = {'width': 0.25, 'color': 'red', 'alpha': 1}

    if origin_opts is True:
        origin_opts = {'color': 'black', 'alpha': .75}

    ax.bar(np.arange(1, nlags), sm.tsa.acf(x=data, unbiased=True, nlags=nlags-1)[1:], **acf_kwds)

    pit_ci_bottom, pit_ci_top = stats.norm.interval(alpha=pct, scale=data.size**-.5, loc=0)
    ax.fill_between(x=np.arange(1, nlags), y1=pit_ci_bottom, y2=pit_ci_top, **fill_between_kwds)

    if origin_opts is not None:
        ax.axhline(0, **origin_opts)

    ax.set_ylim([-1, 1])
    ax.set_xlim([0, nlags - 1])
    ax.xaxis.set_major_locator(mpl.ticker.MaxNLocator(integer=True, nbins=4))
    ax.yaxis.set_major_locator(mpl.ticker.MaxNLocator(nbins=4))


def fan_plot(data, ax, percentiles, cm=None, alpha=None, labels=None, **kwargs):
    """
    Create a fan plot using matplotlib and pandas.

    Parameters
    ----------
    ax : Axes
        The axes to draw to
    data : dataframe
        The data to plot.
    percentiles : tuple of positive ints
        The edges of the confidence bands.
    alpha : float, optional
        The alpha 'intensity' parameters.
    labels : tuple of strings, optional
        The labels of the artists.
    kwargs :
        Values to pass to the plot function.

    """
    cm = cm if cm is not None else mpl.cm.get_cmap()

    data_quantiles = pd.DataFrame(np.nanpercentile(data, percentiles, axis=1).T, index=data.index)
    num_sections = len(percentiles) // 2 + 1

    if len(percentiles) % 2 != 0:
        if labels is None:
            ax.plot(data_quantiles.iloc[:, num_sections - 1], **kwargs, zorder=num_sections + 1)
        else:
            ax.plot(data_quantiles.iloc[:, num_sections - 1], label=labels[0], **kwargs, zorder=num_sections + 1)

    if labels is None:
        for idx in range(num_sections):
            ax.fill_between(data_quantiles.index, data_quantiles.iloc[:, idx], data_quantiles.iloc[:, -(idx + 1)],
                            color=cm(idx / (num_sections - 1), alpha=alpha), zorder=idx)
    else:
        for label, idx in zip(reversed(labels[1:]), reversed(range(num_sections - 1))):
            ax.fill_between(data_quantiles.index, data_quantiles.iloc[:, idx], data_quantiles.iloc[:, -(idx + 1)],
                            color=cm(idx / (num_sections - 1), alpha=alpha), label=label, zorder=idx)


def probability_integral_transform(reference_data, evaluated_data, progress_bar=None):
    """
    Compute the probability integral transform of the data using the empirical cdf of the predicted data.

    The reference_data is assumed to be of the form (horizon, data) dimensions where evaluated_data is of
    length evaluated_data.

    Parameters
    ----------
    reference_data : 2d ndarray or DataFrame
    evaluated_data : 1d ndarray or DataFrame
    progress_bar : tqdm instance
        One of the tqdm iterator wrappers.

    Returns
    -------
    return_points : DataFrame

    """
    if progress_bar:
        iterator = zip(progress_bar(np.asanyarray(reference_data)), np.asanyarray(evaluated_data))
    else:
        iterator = zip(np.asanyarray(reference_data), np.asanyarray(evaluated_data))

    return_points = np.fromiter((empirical_cdf(data_day, prediction) for data_day, prediction in iterator),
                                dtype=np.float64, count=evaluated_data.shape[0])

    if hasattr(evaluated_data, "index"):
        return_df = pd.DataFrame(return_points, index=evaluated_data.index, columns=['PIT'])
    else:
        return_df = pd.DataFrame(return_points, columns=['PIT'])

    return return_df


def empirical_cdf(data, eval_points):
    """
    Compute the empirical cdf of the data.

    Parameters
    ----------
    data : array-like
    eval_points : array-like
    Returns
    -------
    return_vals : ndarray

    """
    data = np.sort(data).reshape(data.size)
    eval_points = np.ravel(eval_points)
    return_vals = np.asarray([np.mean(np.less_equal(data, point), axis=0) for point in eval_points])

    return return_vals


def _cont_ranked_prob_score_in(point, power=2):
    """Compute the continously ranked probability score at one point."""
    (_, data), val = point
    cdf = partial(empirical_cdf, data=data)

    min_val = np.minimum(val, data.min())
    max_val = np.maximum(val, data.max())

    cdf = partial(empirical_cdf, data=data)

    unif_cdf_vals = np.linspace(start=min_val, stop=max_val, num=data.size)
    val_idx = np.searchsorted(unif_cdf_vals, val, side='right')

    cdf_vals = cdf(eval_points=unif_cdf_vals)

    diff = (max_val - min_val) / data.size
    return_val = diff * (np.sum(np.abs(cdf_vals[:val_idx])**power) + np.sum(np.abs(1 - cdf_vals[val_idx:])**power))

    return return_val


def cont_ranked_prob_score(forecast_data, true_values, power=2):
    """
    Compute the continuously ranked probability score.

    Parameters
    --------
    forecast_data : dataframe
    true_values : iterable

    Returns
    -------
    float

    """
    forecast_data = pd.DataFrame(forecast_data)
    true_values = np.ravel(true_values)

    iterator = zip(forecast_data.iterrows(), true_values)

    crps = np.mean([_cont_ranked_prob_score_in(arg, power=power) for arg in iterator])

    return crps
