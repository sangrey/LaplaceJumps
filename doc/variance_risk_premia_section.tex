
\subsection{Volatility Premia \& Return Predictability}
\label{sec:vol_premia}

Another major avenue of research recently regarding the risk return trade-off is the Variance premia literature. 
These papers regress returns on the deviation between the $VIX_t$ (Implied Volatility) and realized volatility and
find that this difference predicts returns. 
This is interesting because the relationship between volatility itself and returns has been more contentious.
The contemporaneous relationship is usually negative, likely arising from a contemporaneous negative correlation.
However, in forecasting regressions it is usually insignificant or had the wrong sign (was negative).

Various authors, such as \textcite{todorov2010variance, drechsler2011whats}, have argued that this can be explained
by considering the role of jumps.
I show the jump and diffusion volatilities explain most of the variation in this item.
I then consider predicting the return using the jump and diffusion volatilities themselves.

If we consider regressions run over the entire sample, we see that both volatilities predict returns in similar
fashion at both the daily and monthly frequencies.
When  I consider joint regressions and polynomial expansions in them, we see that higher terms and their
interactions also matter. 
I then switch to expanding regressions to make the implied trading strategy feasible.
In this case, the monthly results are no longer interesting because I do not have a very long sample. 
(There are only \num{144} months in the entire sample, and doing expanding regressions shortens the effective
sample even more.)
In the expanding daily regressions, I show that there exists a strategy with very high risk adjusted return, in
the sense of very high Sharpe ratios, but the ratios where you just condition on one of the volatilities or
both of them are effectively equivalent.
In other words, the increased difficulty in estimating the additional parameters and the increased benefit
effectively cancel out. 

\subsubsection{Explaining the Variance Risk Premium}

In the table below, I regress  the log-return on the parameters mentioned.  
I also report whether the $\bar{\R}^2$ are statistically significant by running a Wald test that all of the
coefficients except for the intercept are zero,i.e\@ the standard $F-test$.
By doing this I can respect the heteroskedasticity in the problem, and thereby get valid inference. 
The $\R^2$ being statically significantly different from $0$ is equivalent to this test.

First, I claimed that most of the variation in the variance risk premium is determined by the two volatilities. 
In most of the literature, they computed the variance risk premium as the difference between the $VIX_t$ and the
realized volatility $RV_t$. 
In what I show below, I modify the decision slightly in that I compute the difference between $VIX_t$ and the
total volatility, $\sigma^2_t + \gamma^2_t$. 
This is slightly more consistent with the general framework I have been using because I do not have to introduce a
slightly different of the same underlying objects.
Effectively, $RV_t$ is $\sigma^2_t + \gamma^2_t$ plus some noise.  
I used the standard transformation to $VIX_t^2$ so that it is in the same units as the volatilities. 

What I did is fairly simple. 
I took the variance risk premium and regressed it on a quadratic expansion of the two volatilities, including
interaction terms. 
If we do this at the daily frequency, we get an $\R^2$ of $\SI{99.58}{\percent}$ and if we do it at the monthly
frequency, we get an $\R^2$ of \SI{93.11}{\percent}.


%TODO explain the weighting that I am doing.

In the next table, I report the average coefficients from running expanding regressions.
As can be seen here, we have that at the daily frequency, most of the coefficients except for the $\log VIX_t^2$
are statistically significant, and since these values are elasticities, economically significant as well. 
Even a $\num{.1}$ basis point increase in returns is highly relevant when we are considering daily returns. 
In addition, the regressions as a whole are highly statistically significant.\footnote{I used the Bonferroni
    correction to deal with the multiple testing through the expanding regressions and am reporting the p-value
that at least one of the tests rejects the null of no relationship.}
Once we move the monthly frequency, this is no longer the case, for the most part
However, this is likely because I have a total of only \num{144} months, and when we do expanding regressions it
falls even further.  

\begin{table}[t]
    \sisetup{scientific-notation=true, round-precision=2}
    \caption{$\E\left[r_{t+1} \mvert \sigma^2_t, \gamma^2_t, VIX_{t}\right]$}
    \label{tbl:return_predictability}
    \begin{tabularx}{\textwidth}{r *{4}{X} c}
        \toprule
        & $\log {\sigma^2_t}$ & $\log {\gamma_t^2}$ &  $\log {\sigma_t^2} \cdot \log {\gamma_t^2} $ & $\log
        VIX^2_t$ & P$(\hat{F})$ \\
        \midrule 
        % Diffusion
        D & \num{0.000858}*** & & & &  \num[round-precision=3, scientific-notation=false]{.000000580} \\
        M & \num{0.0049} & &  & &  \num[round-precision=3, scientific-notation=false]{0.236} \\
        \midrule
        % Jumps
        D & & \num{0.000528}*** & & & \num[round-precision=3, scientific-notation=false]{0.00452} \\ 
        M & & \num{0.0028}  & & & \num[round-precision=3, scientific-notation=false]{0.453} \\ 
        \midrule
        % Jumps & Diffusion
        D & \num{0.0013}*** & \num{-0.000524}*  & &  & \num[round-precision=3,
        scientific-notation=false]{.000000906} \\
        M & \num{0.0163}** & \num{-0.0126}**  & & &  \num[round-precision=3, scientific-notation=false]{0.0424} \\
        \midrule
        % Jumps, Diffusion & Interaction
        D & \num{-0.000234} & \num{-0.002313}  &  \num{-0.000166} &  & \num[round-precision=3,
        scientific-notation=false]{.000000554} \\
        M & \num{-0.0283}*** & \num{-0.0708}***  & \num{-0.0076}***  & & \num[round-precision=3,
        scientific-notation=false]{0.00193} \\
        \midrule
        % Jumps, Diffusion & VIX
        D & \num{0.001118}*** & \num{-0.000609}**  & &  \num{0.000483}  & \num[round-precision=3,
        scientific-notation=false]{.000000878} \\
        M & \num{0.013266} & \num{-0.013595}**  & & \num{0.005225} & \num[round-precision=3,
        scientific-notation=false]{0.0225} \\
        \bottomrule
    \end{tabularx}
\end{table}


One potentially confusing issue with \cref{tbl:return_predictability} is that the coefficient on the jumps is now
negative.
One might think that this implies that the jumps command a negative premium.
However, this is not the case.
If we consider the discussion in  \cref{sec:decomosition_of_certainty_equivalance_functional},  as discussed there
we see that it is only over short intervals generalized risk sensitivity implies jump premia.
However, as discussed in \cref{sec:vol_dynamics}, the diffusion volatility is more predictable than the jump
volatility and so it is entirely possible that the signs of the coefficients change as we increase the distance
between the returns and the volatilities. 
They seem to do so here.
In fact, one might expect them to have exactly this behavior.

%TODO Discuss  how I obtained the max Sharpe ratios.

\begin{table}
    \caption{Max Attainable Sharpe Ratio (Annualized)}
    \sisetup{round-precision=2, scientific-notation=false}
    \begin{tabularx}{\textwidth}{*{3}{X}}
        \toprule
        $\log {\gamma_t^2} + {\sigma_t^2}$ & $\log {\gamma_t^2}$ &  $VIX_t^2 -
            \left({\gamma_t^2} + {\sigma_t^2}\right) $ \\
        \midrule
        \num{1.6062} & \num{1.6038} & \num{1.5990} \\
        \bottomrule
    \end{tabularx}
\end{table}




