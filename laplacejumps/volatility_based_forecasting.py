"""Predict the volatilities and the densities by using the Laplace-Diffusion realized density representation."""
import os
import numpy as np
import pandas as pd
from scipy import stats
from statsmodels.tsa import api as tsa
from functools import partial
from dask import bag
from dask.diagnostics import ProgressBar
from laplacejumps import probability_integral_transform, cont_ranked_prob_score

# Parameters
nsims = int(1e4)
horizon = 1


def _forecast_var_jump_model_in(y, vols_forecaster, steps=1, size=1):
    """Combine the volatility forecasts for the VAR forecsating."""
    vol_forecast = np.sqrt(np.exp(vols_forecaster(y=y, steps=steps)))
    jump_rvs = stats.laplace.rvs(size=steps * size).reshape((steps, size))
    cont_rvs = np.random.standard_normal((steps, size))

    cont_part = vol_forecast[:, 0].reshape((steps, 1)) * cont_rvs
    discont_part = (vol_forecast[:, 1].reshape(
        (steps, 1)) / np.sqrt(2)) * jump_rvs

    return np.squeeze(np.sum(cont_part + discont_part, axis=0))


def forecast_model_with_jumps(data, forecast_model, steps=1, size=1):
    """Forecast the jump-diffusion model."""
    p = forecast_model.coefs.shape[0]
    thing, starting_point = divmod(data.shape[0], p)
    ending_point = p * (thing - 1)
    windowed_data = (
        data.iloc[idx:idx + p:].values for idx in range(starting_point, ending_point))
    forecaster = partial(_forecast_var_jump_model_in, vols_forecaster=forecast_model.forecast,
                         steps=steps, size=size)
    transformed_thing = pd.DataFrame(
        np.stack([forecaster(chunk) for chunk in windowed_data], axis=0))
    transformed_thing.index = data.index[-len(transformed_thing):]

    return transformed_thing


def forecast_ar_gaussian_model(data, model, horizon=1):
    """Forecast the AR gaussin model."""
    returnval = 0
    nextval = data
    for h in range(1, horizon + 1):
        lastval = nextval
        nextval[:-1] = lastval[1:]
        nextval[-1] = (model.params['const'] + model.params[1:].dot(lastval.T) + np.sqrt(model.sigma2) *
                       np.random.standard_normal(1))

        returnval += nextval[-1]

    return returnval


def forecast_ar_model_densities(data, model, size=1000, steps=1):
    """Forecast the AR data with the model governing the volatiities  and Student-t distributed errors."""
    df = stats.t.fit(model.resid)[0]
    ar_model_forecaster = partial(
        forecast_ar_gaussian_model, model=model, horizon=1)
    volatilities = data.dropna().rolling(model.params[1:].size).apply(
        ar_model_forecaster).dropna().apply(np.exp).apply(np.sqrt)

    cont_rvs_shape = (steps, size, volatilities.shape[0])
    cont_rvs = stats.t.rvs(df=df, loc=0, scale=1, size=np.prod(
        cont_rvs_shape)).reshape(cont_rvs_shape)

    return pd.DataFrame(np.sum(cont_rvs * volatilities.values, axis=0).T, index=volatilities.index)


def forecast_var_model(data):
    """Estimate and forecast the VAR model."""
    model = tsa.VAR(data[['diffusion', 'jumps']]).fit(maxlags=10, ic='bic')

    return forecast_model_with_jumps(data[['diffusion', 'jumps']], model, steps=horizon, size=nsims).iloc[-1, :]


def forecast_rv_model(data):
    """Estimate and forecast the AR model."""
    model = tsa.AR(data, freq='D').fit(maxlags=10, ic='bic')

    return forecast_ar_model_densities(data, model=model, steps=horizon, size=nsims).iloc[-1, :]


if __name__ == "__main__":

    vol_store_path = os.path.normpath(os.path.abspath(os.path.dirname(__file__)) +
                                      '/../results/volatility_estimates.hdf')

    with pd.HDFStore(vol_store_path) as volatility_store:
        log_vol = volatility_store['log_vol']
        daily_rtn = volatility_store['daily_rtn']

    # Jump-Diffusion Model

    with ProgressBar():
        jump_diffusion_forecast = pd.concat(bag.from_sequence(range(200, log_vol.shape[0])).map(
            lambda n: forecast_var_model(log_vol.iloc[:n, :])).compute(), axis=1).T

    jump_diffusion_forecast_merged = pd.concat(
        [daily_rtn, jump_diffusion_forecast], axis=1, join='inner')

    jump_diffusion_pit = probability_integral_transform(jump_diffusion_forecast_merged.values[:, 1:],
                                                        jump_diffusion_forecast_merged['daily_rtn'])

    jump_diffusion_crps = cont_ranked_prob_score(jump_diffusion_forecast_merged.iloc[:, 1:],
                                                 jump_diffusion_forecast_merged['daily_rtn'], progressbar=True)
    # Diffusion Model

    with ProgressBar():
        diffusion_forecast = pd.concat(bag.from_sequence(range(200, log_vol['quad'].size)).map(
            lambda n: forecast_rv_model(log_vol['quad'].iloc[:n].squeeze())).compute(), axis=1).T

    diffusion_forecast_merged = pd.concat([daily_rtn, diffusion_forecast], axis=1, join='inner')

    diffusion_pit = probability_integral_transform(diffusion_forecast_merged.values[:, 1:],
                                                   diffusion_forecast_merged['daily_rtn'])

    diffusion_crps = cont_ranked_prob_score(diffusion_forecast_merged.iloc[:, 1:],
                                            diffusion_forecast_merged['daily_rtn'], progressbar=True)
    # Saving the Results

    forecast_store_path = os.path.normpath(os.path.abspath(os.path.dirname(__file__)) +
                                           '/../results/{}_day_density_forecasts.tmp.hdf'.format(horizon))

    with pd.HDFStore(forecast_store_path) as forecast_store:

        forecast_store['jump_diffusion'] = jump_diffusion_forecast
        forecast_store['diffusion'] = diffusion_forecast
        forecast_store['jump_diffusion_pit'] = jump_diffusion_pit
        forecast_store['diffusion_pit'] = diffusion_pit
        forecast_store['jump_diffusion_crps'] = jump_diffusion_crps
        forecast_store['diffusion_crps'] = diffusion_crps
