#ifndef LAPLACE_JUMPS_H
#define LAPLACE_JUMPS_H 

#include <cmath>
#include <algorithm>
#include <stdexcept>
#include <vector>
#include <iterator>
#include <cassert>
#include <array>
#include <armadillo>
#include "arma_wrapper.h"


namespace laplacejumps {

using aw::npulong;
using aw::npdouble;
using aw::dmat;
using aw::dvec;
using aw::iuvec; 
using arma::uvec;
using drow = arma::Row<npdouble> ;


/* 
 * Initializes a random generator in a thread_save way to be used globally througout the entire library. 
 */
inline
std::mt19937_64& initialize_mt_generator() { 

    thread_local std::random_device source{};
    thread_local std::mt19937_64 engine{source()};
    
    return engine; 

}

/*
 * This function simply averages the matrix with its tranpose.
 */
inline
dmat make_symmetric(const dmat &matrix) {
    return .5 * (matrix + matrix.t());
}

/*
 * This function simply averages the first matrix with tranpose of the second.
 */
inline
dmat make_symmetric(const dmat &matrix1, const dmat matrix2) {
    return .5 * (matrix1 + matrix2.t());
}

/*
 * Calculates the lower triangular root of a matrix.  If the matrix is positive-definite. it uses the cholesky
 * decomposition since that method is very fast and numerically stable. However, it uses the SVD decomposition 
 * to handle rank-deficient matrices.  To get a triangular root from the SVD decomposition, you need to use the 
 * QR factorization after the SVD decomposition, and so that is done as well. 
 */
inline dmat lt_root_mat(const dmat& psd_mat, bool zero_upper_triangle=true) {

    dmat root_mat(arma::size(psd_mat));
    bool factored_mat = arma::chol(root_mat, psd_mat, "lower");

    if (!factored_mat) {
        dmat u_mat(arma::size(psd_mat));
        dmat v_mat(arma::size(psd_mat));
        aw::dvec singular_vals(psd_mat.n_rows);
        dmat q_mat(arma::size(psd_mat));

        factored_mat = arma::svd(u_mat, singular_vals, v_mat, make_symmetric(psd_mat));
        if (!factored_mat) {
            throw std::runtime_error("The matrix could not be SVD decomposed.");
         }

        dmat right_root = arma::diagmat(arma::sqrt(singular_vals)) * make_symmetric(u_mat, v_mat);
        factored_mat = arma::qr(q_mat, root_mat, right_root);

        /* We want the entries on the diagonal to be positive as the are in the Cholesky decomposition. */
        if (root_mat(0) < 0) {
            root_mat = -1 * root_mat;
        }

        if (!factored_mat) {
            throw std::runtime_error("We could not QR decompose the matrix.");
        }

        /* We want the left root. */ 
        arma::inplace_trans(root_mat); 
     }

    if (zero_upper_triangle) {
        root_mat = arma::trimatl(root_mat);
    }

    return root_mat;
}




/* 
 * Simulates the Cox-Ingersoll-Ross Model.
 */
inline
dvec simulate_cir(npdouble kappa, npdouble omega, npdouble theta, npulong time_dim=250, npulong infill_dim=250,
        npdouble alpha=0.5 ) {

    if (kappa <= 0 || omega <= 0 || theta <= 0 || alpha <= 0) {
        
        throw std::invalid_argument("All of the parameters must be strictly positive.");

    }

    dvec data(time_dim * infill_dim);
    dvec innovations(time_dim * infill_dim, arma::fill::randn);

    auto lclamp = [](auto val) {return std::max(val, 0.0);};
    npdouble inc = 1.0 / infill_dim;
    auto v_prev = theta;

    for(size_t t=0; t<time_dim*infill_dim; ++t) {

        auto v_next = (v_prev + kappa * inc * (theta - lclamp(v_prev)) + omega * std::pow(lclamp(v_prev) * inc,
                    alpha) * innovations(t));
       
        data(t) = lclamp(v_next);
        v_prev = v_next;

    }

    return data;

}

/* Draws a standard normal random variable using the std library's distribution and the given generator. */
inline
aw::dmat multivariate_normal_rvs(const dmat& innov_cov, const aw::npulong size) {
    
    npulong data_dim = innov_cov.n_cols;
    thread_local std::normal_distribution<aw::npdouble> n_dist(0,1);        
    thread_local auto generator = initialize_mt_generator();  
    aw::dmat variates = aw::dmat(size, data_dim).imbue([] () {return n_dist(generator);}); 
    dmat covariance_factor = arma::trimatl(lt_root_mat(innov_cov));

    aw::dmat multivariate_variates = variates * covariance_factor.t();

    return multivariate_variates; 
}

inline 
aw::dvec laplace_rvs(const aw::npulong n_elem) {

    thread_local auto generator = initialize_mt_generator();  
    thread_local std::exponential_distribution<aw::npdouble> e_dist(1);        

    aw::dvec variates = aw::dvec(n_elem).imbue([] () {return e_dist(generator) - e_dist(generator);}); 

    return variates;

}

inline
uvec get_rxt_indices(const uvec& total, const uvec& subset)  {

    std::vector<arma::uword> diff;
    std::set_difference(total.begin(), total.end(), subset.begin(), subset.end(), std::inserter(diff,
                diff.begin()));
    uvec return_indices  = arma::conv_to<uvec>::from(diff); 

    return return_indices;
}

inline npdouble log_logisitc(npdouble x) {

    return -std::log1p(std::exp(-x));

}

std::tuple<npdouble, npdouble> extract_root_vol(const dmat& vol_data, bool log_prop_form=true) {
    /*
     * Extracts the root diffusion and jump vols from the data. 
     *
     * If log_prop_form is True, it assumes vol_data's first two columns are log total vol and log jump
     * proportion. Otherwise, it assumes that vol_data's first two columns are diffusion vol and jump vol.
     */
    
    npdouble root_jump_vol, root_diff_vol;

    if (log_prop_form) {
        npdouble log_jump_prop = log_logisitc(arma::as_scalar(vol_data.col(0))); 
        npdouble log_total_vol = arma::as_scalar(vol_data.col(1));
        npdouble log_jump_vol = log_jump_prop + log_total_vol;

        double total_vol = std::exp(log_total_vol);
        double jump_prop = std::exp(log_jump_prop);

        assert (jump_prop <= 1);

        root_jump_vol = std::exp(.5 * log_jump_vol);
        root_diff_vol = std::sqrt((1-jump_prop) * total_vol);

    } else {

        root_diff_vol = std::exp(arma::as_scalar(.5 * vol_data.col(0)));
        root_jump_vol = std::exp(arma::as_scalar(.5 * vol_data.col(1)));

    }

    return std::make_tuple(root_diff_vol, root_jump_vol); 

}


inline
dmat har_density_forecast(const dvec& regressor, const dvec& intercept, const dmat& innov_cov, const dmat& coeff,
        const iuvec& regressand_coeff_indices, iuvec& rxt_innov_indices, const iuvec& rxt_coeff_indices,
        bool log_prop_form, size_t H) {
    /*
     * This function computes h-horizon forecasts using a har model. 
     * It iterates the coefficient and intercept forward appropriaately.
     * The innovations to the volatities are Gaussian, but the innovations to the excess_rtn 
     * laplace-gaussian convolutions with the appropriate weights.
     */

    uvec arma_regressand_coeff_indices = arma::sort(arma::conv_to<uvec>::from(regressand_coeff_indices));
    uvec arma_rxt_innov_indices = arma::sort(arma::conv_to<uvec>::from(rxt_innov_indices));
    uvec arma_rxt_coeff_indices = arma::sort(arma::conv_to<uvec>::from(rxt_coeff_indices));

    npulong regressand_dim = regressand_coeff_indices.n_elem;
    npulong rxt_dim  = rxt_coeff_indices.n_elem;

    uvec arma_regressor_innov_indices = get_rxt_indices(arma::regspace<uvec>(0, 1, regressand_dim-1),
            arma_rxt_innov_indices);
    uvec arma_regressor_coeff_indices = get_rxt_indices(arma_regressand_coeff_indices, arma_rxt_coeff_indices); 

    dmat predictions(H, regressand_dim);
    dvec previous_val = regressor;
    drow pred;

    npdouble root_diff_vol, root_jump_vol; 
    dmat laplace_innovations = arma::reshape(laplace_rvs(H * rxt_dim), H, rxt_innov_indices.n_elem);
    dmat gaussian_innovations = multivariate_normal_rvs(innov_cov, H); 

    for(size_t h=0; h<H; ++h) {
       pred = (intercept + coeff * previous_val).t(); 
       pred(arma_rxt_coeff_indices) = arma::exp(pred(arma_rxt_coeff_indices));
       /* std::cout << "The prediction is" << pred(arma_rxt_coeff_indices)<< std::endl; */

       /* If log_prop is true, we need to ensure that the realizations of that variable are negative. */
       drow gaussian_innov = gaussian_innovations.row(h);
       pred(arma_regressor_coeff_indices) += gaussian_innov(arma_regressor_innov_indices); 

       std::tie(root_diff_vol, root_jump_vol) = extract_root_vol(pred(arma_regressor_coeff_indices).t(),
               log_prop_form);

       drow scaled_gasussian_innovations = root_diff_vol * gaussian_innov(arma_rxt_innov_indices);
       drow scaled_laplace_innovations = (root_jump_vol / std::sqrt(2)) * laplace_innovations.row(h);

       pred(arma_rxt_coeff_indices) += (scaled_gasussian_innovations + scaled_laplace_innovations); 
       /* std::cout << "The scaled_gaussian_innovations are  " << scaled_gasussian_innovations << */ 
           /* " and the laplace innovations are  " << scaled_laplace_innovations << std::endl; */ 
         
        previous_val = pred.t();
        predictions.row(h) =  pred(arma_regressand_coeff_indices).t();

    }

    return predictions;


}




inline
dmat har_forecast(const dvec& regressor, const dvec& intercept, const dmat& coeff, 
        const iuvec& regressand_coeff_indices, iuvec& rxt_innov_indices, const iuvec& rxt_coeff_indices,
        size_t H) {
    /*
     * This function computes h-horizon forecasts using a har model. 
     * It iterates the coefficient and intercept forward appropriaately.
     */

    uvec arma_regressand_coeff_indices = arma::sort(arma::conv_to<uvec>::from(regressand_coeff_indices));
    uvec arma_rxt_innov_indices = arma::sort(arma::conv_to<uvec>::from(rxt_innov_indices));
    uvec arma_rxt_coeff_indices = arma::sort(arma::conv_to<uvec>::from(rxt_coeff_indices));

    npulong regressand_dim = regressand_coeff_indices.n_elem;

    uvec arma_regressor_innov_indices = get_rxt_indices(arma::regspace<uvec>(0, 1, regressand_dim-1),
            arma_rxt_innov_indices);
    uvec arma_regressor_coeff_indices = get_rxt_indices(arma_regressand_coeff_indices,
            arma_rxt_coeff_indices); 

    dmat predictions(H, regressand_dim);
    dvec previous_val = regressor;
    drow pred;

    for(size_t h=0; h<H; ++h) {
       pred = (intercept + coeff * previous_val).t(); 
       pred(arma_rxt_coeff_indices) = arma::exp(pred(arma_rxt_coeff_indices));

       previous_val = pred.t();
       predictions.row(h) =  pred(arma_regressand_coeff_indices).t();

    }

    return predictions;


}

inline
npdouble constrained_loglike(const dmat& endog, const dmat& exog, const dmat& params, const dmat& root_cov, 
                             const dmat& weights, const iuvec& constrained_indices) { 
    /* The log likelihood for the har model. */

    if (endog.n_rows != exog.n_rows){
        throw std::invalid_argument("The time dimension is inconsistent.");
    }
    if (params.n_rows != exog.n_cols)  {
        throw std::invalid_argument("The regressor dimension is inconsistent.");
    }

    bool consistent_regressand_dim = ((endog.n_cols == root_cov.n_rows) && (root_cov.n_rows == params.n_cols) &&
            root_cov.is_square());
    if (! consistent_regressand_dim)  {
        throw std::invalid_argument("The regressor dimension is inconsistent.");
    }

    size_t time_dim = endog.n_rows;

    /* I do not have to multiply by .5 because I am using the root covariance matrix. */
    dmat pred_mean = exog * params;
    dmat resid;

    if (constrained_indices.n_elem > 0) {
       uvec indices = arma::sort(arma::conv_to<uvec>::from(constrained_indices));
       pred_mean.cols(indices) = arma::exp(pred_mean.cols(indices));
       resid = endog - pred_mean;
       resid.cols(indices) =  arma::sqrt(weights) % resid.cols(indices);
    } else {
       resid = endog - pred_mean;
    }
    
    dmat weighted_resid;

    /* I multiply the residuals by the inverse root covariance. */
    bool solve_system = arma::solve(weighted_resid, arma::trimatl(root_cov), resid.t()); 
    /* std::cout << "The weighted_resid shape is " << arma::size(weighted_resid) << std::endl; */

    if (! solve_system) {
        return -arma::datum::inf;
    }

    /* Calculate the log-determinant of a Gaussian random variable. */
    npdouble neg_log_det = -1.0 *  time_dim * arma::accu(arma::log(root_cov.diag()));
    npdouble returnval = -.5 * arma::accu(arma::square(weighted_resid)) + neg_log_det;

    return returnval;

}



/*
 * Simulates a Gaussian autoregression with stochastic volatility. 
 */
dvec simulate_ar(double coeff, double mean, const dvec& innov_var) {

    std::normal_distribution<double> dist(0,1);
    size_t time_dim = innov_var.size();
    dvec data(time_dim + 1);
        
    data(0) = mean;

    for (size_t t=1; t <= time_dim; ++t) {
        double root = std::sqrt(std::max(innov_var(t-1), 0.0));
        data(t) = mean * (1 - coeff) + coeff * data(t-1) + root * dist(initialize_mt_generator());
    }

    return data.tail(time_dim);

}

}  /* end namespace laplacejumps */

#endif /* LAPLACE_JUMPS_H */
