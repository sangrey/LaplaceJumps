I believe the results below are true, but there are some things I don't understand. In particular, the way that rescaling time and space in the variance-gamma process works.
Hence, I wouldn't want to stake my professional reputation on it.

\section{Examples}


\subsection{Gamma Martingales are Time-Changed Variance-Gamma Processes} \label{app:gamma_as_time_changed_var_gamma}

To aid in understanding \cref{theorem:timechangedlaplace}, I construct a nontrivial example of this time-change. The gamma process a compelling case because it is one of the most commonly used infinite-activity, finite-variance L\'{e}vy processes in its own right and a special case of the tempered stable class. Constructing a time-change that converts a variance-gamma process into the martingale part of a gamma process is nontrivial because the gamma process is an increasing process. We need to choose a particular \textquote{rescaling} that converts a process whose innovations are symmetric around zero into a process with strictly positive innovations. 

I start by constructing the exponential special-case of a gamma martingale. We know that the absolute value of a Laplace random variable is exponentially distributed. Consequently, if $X(t)$ is a standard variance-gamma  process, then $\abs{X(t)}$ is an exponential process.  Per \Cref{theorem:timechangedlaplace}, there exists a time-change of $X(t)$ to coincide with the martingale part of $\abs{X(t)}$. I construct such a process now.

Define $Z(t) \coloneqq \int_0^t \sgn(X(t-)) \dif X(s)$. Claim: over intervals $A$ with $A$ small the distribution of the increments coincide. This claim implies the martingale components equal in distribution because it implies the martingale parts of the processes behave the same as integrators.

We show equality in distribution by showing that the expectations of polynomials of the increments coincide. Let $f$ be a polynomial, $g_n$ be a series of twice-differentiable convex functions converging to $\abs{\cdot}$, $h_n \coloneqq f(g_n(\cdot))$, and $t, \tau$ be the endpoints of $A$.  Consider
%
\begin{align}
    \E\left[h_n(X(t))  - h_n(X(\tau))\right]  - \E\left[f(Z(t)) - f(X(\tau))\right]. 
\end{align}
%
Applying \Ito's lemma for non-continuous functions, \parencite[Theorem 6.46]{medvegyev2007stochastic} gives 
%
\begin{align}
    \label{eqn:expanded_difference}
    &= \E\bigg[\int_t^{\tau} h_n'(X(s-)) \dif X(s) + \sum_{t < s \leq \tau} \left(\Delta f_n(X(s)) - f_n'(X(s-)) \Delta X(s) \right)  \\
%
    &\quad - \int_t^{\tau} f'(Z(s-)) \dif Z(s) + \sum_{t < s \leq \tau} \left(\Delta f'(Z(s))  - f'(Z(s-)) \Delta Z(s)\right) \bigg] \notag.
\end{align}
%
Applying Taylor's theorem to the second term in \cref{eqn:expanded_difference} gives 
%
\begin{align}
    & \E\left[ \sum_{t < s \leq \tau} \left(\Delta f_n(X(s)) - f_n'(X(s-)) \Delta X(s) \right) \right] \\
%
    = &\E\left[\sum_{t < s \leq t} f_n'(X(s-)) \Delta X(s) - f_n'(X(s-)) \Delta X(s) + \frac{1}{2} f_n''(X(s-)) (\Delta X)^2\right].  \notag
\end{align}
%
The second expression is $O_p(\tau -t)$ because $f_n$ is a polynomial and the expression is a predictable quadratic variation.  Consequently, if we take $n \to \infty$ and then $t \to \tau$, it converges to zero. The same argument applies the last term in \cref{eqn:expanded_difference}, and so it also converges to zero.

The \textquote{differentiation} and integration in the definition of $Z(t)$ cancel, and so this implies \cref{eqn:expanded_difference} equals  
%
\begin{equation}
    \label{eqn:integrand_difference}
    = \E\left[\int_t^{\tau} (h_n'(X(s-))  - f'(Z(s-)) \sgn(X(s-)) \dif X(s) \right] + O_p(\tau - t)
\end{equation}
%
Because $X(t)$ and $Z(t)$ coincide by assumption, we can choose $Z(s-)$ to have the same distribution as $X(s-)$.  Consequently, by taking $n \to \infty$, \cref{eqn:integrand_difference} is  $O_p(\tau - t)$.  This implies the martingale parts of $Z(t)$ and $\abs{X}(t)$ coincide in distribution.  In other words, $Z(t)$ is the martingale part of an exponential process, i.e., its increments have exponential laws up to drift terms.

To convert an exponential process into an arbitrary gamma process note that the marginal distribution of the gamma process is $f(x, t, \gamma, \lambda) = \lambda^{\gamma t} / \Gamma(\gamma t) x^{\gamma t - 1} \exp(- \lambda x)$, which is sufficient to specify the process because it is does not have any dynamics. The $\gamma$ and $t$ only enter as the product so by rescaling time in $Z(t)$ by $1 / \gamma$, we can introduce arbitrary $\gamma$. We can introduce arbitrary $\lambda$ by multiplying  $Z(t)$, $1 / \lambda$. Recall that rescaling space and rescaling time for Laplace processes are equivalent, even though they are not for exponential processes: 
%
    $\int_0^t  \dif \Lap(s) = \Lap(0, t) = \frac{\sqrt{t}}{\sqrt{2}} \Lap(0,1) = \frac{\sqrt{t}}{\sqrt{2}}  \int_0^1 \dif \Lap(s)$.
 
We constructed $Z(t)$ as a stochastic volatility integral with respect to $X(t)$, and so combining the various rescalings completes the argument. Note, I am not claiming you can construct a gamma process from a standard variance-gamma process with a single-time change, only that you can construct the martingale part of the gamma process. To recover a gamma process, we also must rescale the jump analog to local time. It is not obvious we can do this with only a single time-change.

