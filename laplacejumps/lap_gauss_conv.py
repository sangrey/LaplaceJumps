import numpy as np
from scipy import stats, optimize, special, integrate

class lapgaussconv:
    """ This class provides a scipy stats distribution class for the sum of a Laplace and Gaussian variables."""

    @staticmethod
    def rvs(size=1, loc=0, sigma=1, gamma=1, random_state=None):
        """ I use the sum representation to draw random variables."""

        data = (loc + stats.laplace.rvs(size=size, scale=gamma/(2**.5), random_state=random_state)  
               + stats.norm.rvs(size=size, scale=sigma, random_state=random_state))

        return data

    @staticmethod
    def logpdf(x, loc=0, sigma=1, gamma=1):
        """ I compute the logpdf with a formula that is derived in the accompanying documentation. """

        term1 = - np.log(gamma)
        
        # I exploit symmetry in the density to avoid numerical stability issues. 
        inner_term1 = (np.abs(x) / (np.sqrt(2) * sigma)) + sigma / gamma
        inner_term2 = (np.abs(x) / (np.sqrt(2) * sigma)) - sigma / gamma
    
        if inner_term2 > 0:
            # erfcx is numerically stable for large values.
            term2 =  np.log(np.exp(-inner_term2**2) * (special.erfcx(inner_term1) - special.erfcx(inner_term2)) +
                            2)
        else: 
            # erf is close to zero for small values.
            term2 =  np.log(np.exp(-inner_term2**2) * special.erfcx(inner_term1) - special.erf(-inner_term2) + 1)
            
        term3 = - 1.5 * np.log(2) 
        term4 = - (np.sqrt(2) * np.abs(x) - sigma**2 / gamma) / gamma
        
        return term1 + term2 + term3 + term4

    @staticmethod
    def pdf(x, loc=0, sigma=1, gamma=1):
        """ Computes the pdf by taking the exponential of the logpdf """

        return np.exp(lapgaussconv.logpdf(x, loc=loc, sigma=sigma, gamma=gamma))

    @staticmethod
    def cdf(x, loc=0, sigma=1, gamma=1):
    
        if x < loc:
            return 1 - lapgaussconv.cdf(-(x-loc), loc=0, sigma=sigma, gamma=gamma)
        else:    
            return .5 + integrate.quad(lambda x: lapgaussconv.pdf(x,loc=loc,sigma=sigma,gamma=gamma), a=0, b=x)[0]

    @staticmethod
    def ppf(q, loc=0, sigma=1, gamma=1, x0=0):
        """ Computes the survival function by numerically inverting the cdf """ 

        std_vals = np.asarray([sigma, gamma]) / np.minimum(sigma, gamma)
        opt_result = optimize.minimize(lambda x: (lapgaussconv.cdf(x, sigma=std_vals[0], gamma=std_vals[1])-q)**2,
                                       x0=x0+loc).x 
        return loc + opt_result * np.minimum(sigma, gamma) 

    @staticmethod
    def var(loc=0, sigma=1, gamma=1):
    
        return sigma**2 + gamma**2

    @staticmethod
    def mean(loc=0, sigma=1, gamma=1):

        return loc

    @staticmethod
    def std(loc=0, sigma=1, gamma=1):

        return np.sqrt(sigma**2 + gamma**2)


def diffusion_cond_var(z, sigma=1, gamma=1):
    """ 
    Computes the variance of the diffusion part given that the total value equals z. 
    That is it computes E[X**2 | X+Y = z], where X is Gaussian and Y is Laplace

    I use abs(z) instead of z because all of the relevant distributions are symmetric, and this increases the
    numerical stability of the algorithm.
    The if/else part calculates the same value using two different methods in order to preserve numerical
    stability.
    The equation itself is in the accompanying documentation.

    Parameters
    ----------
    z: float
    sigma : positive float, optional
        The standard deviation of the Gaussian part.
    gamma : positive float, optional
        The standard deviation of the Laplace part.

    Returns
    -------
    positve float 
    """
    
    std_ratio = sigma / gamma
    inner_plus = abs(z) / (2**.5 * sigma) + std_ratio
    inner_minus = abs(z) / (2**.5 * sigma) - std_ratio
    sigma_z_ratio = abs(z)**2 / (2 * sigma**2)
    
    plus_mult_val = sigma * std_ratio * np.exp(-sigma_z_ratio) * 2**(-1.5)       
    exponential_term = - np.sqrt(2 / np.pi) * sigma * std_ratio**2 * np.exp(-sigma_z_ratio) 
    
    if inner_minus < 0:
        minus_mult_val = sigma * std_ratio * np.exp(-sigma_z_ratio) * 2**(-1.5)
        minus_part = special.erfcx(-inner_minus) * minus_mult_val * (1 + 2 * std_ratio**2)
    else:
        val1 =  np.log(1 + special.erf(inner_minus))
        val2 = np.log(1 + 2 * std_ratio**2)
        val3 = -sigma_z_ratio + np.log(sigma * std_ratio * 2**(-1.5))
        minus_part = np.exp(inner_minus**2 + val1 + val3 + val2)
    
    plus_part = special.erfcx(inner_plus) * plus_mult_val *  (1 + 2 * std_ratio**2)
    
    return plus_part + exponential_term + minus_part



def jump_cond_var(z, sigma=1, gamma=1):
    """ 
    Computes the variance of the jump part given that the total value equals z. 
    That is it computes E[Y**2 | X+Y = z], where X is Gaussian and Y is Laplace

    I use abs(z) instead of z because all of the relevant distributions are symmetric, and this increases the
    numerical stability of the algorithm.
    The if/else part calculates the same value using two different methods in order to preserve numerical
    stability.
    The equation itself is in the accompanying documentation.

    Parameters
    ----------
    z: float
    sigma : positive float, optional
        The standard deviation of the Gaussian part.
    gamma : positive float, optional
        The standard deviation of the Laplace part.

    Returns
    -------
    positve float 
    """

    sum_part = gamma * abs(z) + np.sqrt(2) * sigma**2
    minus_part = gamma * abs(z) - np.sqrt(2) * sigma**2
    coeff_lambda = lambda x: gamma**2 * sigma**2 + x**2
    
    terma = lambda x: np.sqrt(np.pi) * (2 * gamma * sigma * x) 
    termb = lambda x: np.sqrt(2) * np.pi * coeff_lambda(x) * special.erfcx(x / (np.sqrt(2) * gamma * sigma))
    
    first_term = np.exp(-z**2 / (2 * sigma**2)) / (4 * np.pi * gamma**3)
    
    return first_term * (terma(minus_part) - terma(sum_part) + termb(-minus_part) + termb(sum_part))
                                  

def jump_var_proportion(z, sigma=1, gamma=1):
    """ 
    Computes the propotion of the variance driven by the jump part locally around z. 

    Parameters
    ----------
    z: float
    sigma : positive float, optional
        The standard deviation of the Gaussian part.
    gamma : positive float, optional
        The standard deviation of the Laplace part.

    Returns
    -------
    positve float 
    """

    jump_var = jump_cond_var(z, sigma, gamma)
    diffusion_var = diffusion_cond_var(z, sigma, gamma)

    return jump_var / (jump_var + diffusion_var)


def es_diffusion_variance(z, sigma=1, gamma=1):
    """ 
    Computes the variance of the diffusion part given that the sum  of the jumps and diffusion is greater than z 

    i.e.  E[ diffusion**2 | abs(diffusion + jumps) > z]

    Parameters
    ----------
    z: float
    sigma : positive float, optional
        The standard deviation of the Gaussian part.
    gamma : positive float, optional
        The standard deviation of the Laplace part.

    Returns
    -------
    positve float 
    """
    
    z=abs(z)   
    a1 = gamma*z - np.sqrt(2) * sigma**2
    a2 = gamma*z + np.sqrt(2) * sigma**2

    c1 = z / (np.sqrt(2) * sigma)
    b1 = a1 / (np.sqrt(2) * gamma * sigma)
    b2 = a2 / (np.sqrt(2) * gamma * sigma)
    inner_quad_part = np.sqrt(2) * z / gamma
    ratio = sigma/gamma
        
    if b1 > 0:
        diff = np.exp(ratio**2) * (np.exp(-inner_quad_part) * special.erfc(-b1)  - np.exp(inner_quad_part) *
                                   special.erfc(b2))
    else:
        diff = np.exp(-c1**2)  * (special.erfcx(-b1) - special.erfcx(b2))
    
    return sigma**2 * ((.5 + ratio**2) * diff + special.erfc(c1))
    

def es_jump_variance(z, sigma=1, gamma=1):
    """ 
    Computes the variance of the jump part given that the sum  of the jumps and diffusion is greater than z 

    i.e.  E[ jumps**2 | abs(diffusion + jumps) > z]

    Parameters
    ----------
    z: float
    sigma : positive float, optional
        The standard deviation of the Gaussian part.
    gamma : positive float, optional
        The standard deviation of the Laplace part.

    Returns
    -------
    positve float 
    """
    z=abs(z)   
    a1 = gamma*z - np.sqrt(2) * sigma**2
    a2 = gamma*z + np.sqrt(2) * sigma**2

    c1 = z / (np.sqrt(2) * sigma)
    b1 = a1 / (np.sqrt(2) * gamma * sigma)
    b2 = a2 / (np.sqrt(2) * gamma * sigma)
    d1 = a1**2 + gamma**2*sigma**2
    d2 = a2**2 - gamma**2*sigma**2
        
    part1 = gamma**2 * (2 * special.erfcx(c1) - special.erfcx(b2))      
    part2 = special.erfcx(b2) * (np.sqrt(2)*gamma*z - d2/gamma**2)
    part3 = 2*z * sigma * np.sqrt(2/np.pi)
    factor = (np.sqrt(2)*a1 + d1/gamma**2 + gamma**2) /2 

    if b1 < 0:
        part4 = np.exp(-c1**2) * special.erfcx(-b1) * factor
    else:
        part4 = np.exp(b1**2-c1**2) * special.erfc(-b1) * factor
    
    returnval =  part4 + (np.exp(-c1**2) / 2) * (part1 + part2 + part3)
  
    return returnval    


def es_covariance_term(z, sigma=1, gamma=1):
    """ 
    Computes the covariance of the jumps and gaussian parts given that their sum is greater than z 

    i.e.  E[ jumps * diffusion | abs(diffusion + jumps) > z]

    Parameters
    ----------
    z: float
    sigma : positive float, optional
        The standard deviation of the Gaussian part.
    gamma : positive float, optional
        The standard deviation of the Laplace part.

    Returns
    -------
    positve float 
    """
    
    z=abs(z)   
    a1 = gamma*z - np.sqrt(2) * sigma**2
    a2 = gamma*z + np.sqrt(2) * sigma**2

    c1 = z / (np.sqrt(2) * sigma)
    b1 = a1 / (np.sqrt(2) * gamma * sigma)
    b2 = a2 / (np.sqrt(2) * gamma * sigma)
    inner_quad_part = np.sqrt(2) * z / gamma
    ratio = sigma/gamma
    
    part1 = np.exp(-c1**2) *  special.erfcx(b2) * (inner_quad_part /2  + ratio**2)
    
    factor = (inner_quad_part / 2 - ratio**2)
    
    if b1 > 0:
        part2 = np.exp(-c1**2) * special.erfcx(-b1) 
    else:
        part2 = np.exp(b1**2 - c1**2) * special.erfc(-b1)  
        
    return sigma**2 * (part1 +part2 * factor)


def es_jump_variance_prop(x, sigma=1, gamma=1):
    """ 
    Computes the proportion of the variability drive by the jumps in the return given the return exceeds x in
    absolute value.

    Parameters
    ----------
    x: float
    sigma : positive float, optional
        The standard deviation of the Gaussian part.
    gamma : positive float, optional
        The standard deviation of the Laplace part.

    Returns
    -------
    positve float 
    """
    
    jump_var = es_jump_variance(x, sigma, gamma)
    diffusion_var = es_diffusion_variance(x, sigma, gamma)
    covariance_term = es_covariance_term(x, sigma, gamma)
    
    returnval = np.exp(np.log(jump_var) - np.log(jump_var + diffusion_var + 2 * covariance_term))

    return returnval 


def es_jump_variance_decomposition(q, sigma=1, gamma=1):
    """ 
    Computes the proportion of the variability drive by the jumps in the return given the return is at least q
    percent away from its mean. 

    Parameters
    ----------
    q: positive float
        The percentile that you should exceed.
    sigma : positive float, optional
        The standard deviation of the Gaussian part.
    gamma : positive float, optional
        The standard deviation of the Laplace part.

    Returns
    -------
    positve float 
    """

    xval = lapgaussconv.ppf(q, sigma=np.asscalar(sigma), gamma=np.asscalar(gamma))
    return_val = es_jump_variance_prop(xval, sigma=np.asscalar(sigma), gamma=np.asscalar(gamma))

    return return_val 
