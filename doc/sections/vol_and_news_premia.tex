
\section{News Premia: Empirics}\label{sec:news_premia_empirics}

\subsection{News Premia}\label{sec:return_and_vol_risk_premia}

In practice, $\diffvolt$ and $\jumpvolt$ are very heavily correlated (\SI[round-precision=0]{74}{\percent}) and so regressing on them does not lead to robust results. Moreover, interaction terms in those regressions are often significant.  To isolate the effect of the jumps, I reparameterize the process in terms of $\jumppropt$ and $\totalvolt$.  To make the innovations closer to Gaussian-distributed and avoid the need for interaction terms, I report elasticities, i.e., I apply a log transformation.  Hence, the preferred specification is 
%
\begin{equation}
    \label{eqn:regression_specification}
    \rxt = \beta_0 + \beta_1 \log\left(\totalvolt\right) + \beta_2 \log\left(\jumppropt\right) +
    \epsilon_t.
\end{equation}
%
I report the OLS and robustness results in \cref{app:news_premia_empirical}. The identified results in the other specifications either agree with the main specification or are insignificant. The OLS results are consistent with the literature. 

The analysis below uses the daily excess return, $\rxt$, to make the results more easily comparable with those in the literature.  I construct $\rxt$  by taking $\rtnt$ and subtracting the log yield on the \num{10} year treasury bill, which I obtain from FRED.  I annualize $\rxt$ (multiplied it by \num{252}) to make the results more interpretable.  I use Newey-West heteroskedasticity and autocorrelation (HAC) robust standard errors and report $t$-statistics in square brackets.  I use Bartlett's kernel with the optimal bandwidth,  per \textcite{newey1994automatic}. 

Risk premia are forward-looking by definition, and so we must isolate the predictable variation in the regressors.  Intuitively, we want to regress returns on expected volatilities. If we interpret coefficients from a contemporaneous regression, like those reported in \cref{tbl:contemporaneous_relationship_return_and_vol} (which is in the appendix), as risk premia, we have the classic endogenous regressors problem because the leverage effect is the correlation between the regressors and error terms.

The most common way to handle endogenous regressors is using instrumental variables, which is what I do.  In particular, I use the lagged regressors as instruments. The lagged volatilities are valid instruments.  First, they explain a large amount of the variation in the regressors. I adopt an approximate heterogeneous autoregressive (HAR) specification to choose lags used as instruments, \parencite{corsi2009simple}.  To be precise, I use $\totalvol{t-l}$ and $\jumpprop{t-l}$  for $l \in \lbrace 1, 2, 5, 25 \rbrace$.  I report the results from the first-stage regressions in \cref{tbl:iv_first_stage_regression}.  The $F$-statics reported there are far outside the week-instrument region. Second, they are predetermined, and hence by definition independent of the date-$t$ innovation. 

I consider two specifications.  The leading specification uses $\log(\totalvolt)$ as my first regressor and $\log(\jumppropt)$ as my second regressor.  I also consider a specification with $\log(\diffvolt)$ as the first regressor and $\log(\totalvolt)$ as the second regressor.

\begin{table}[htb]
    \centering
    \caption{News Premia Estimates}
    \label{tbl:jump_premia_estimates}

    \sisetup{
        table-align-text-pre=false,
        table-align-text-post=false,
        round-mode=places,
        round-precision=2,
        table-format=2.2,
        table-space-text-pre=\lbrack,
        table-space-text-post=\rbrack,
    }

    \begin{tabularx}{.85\textwidth}{Z *{6}{S}}
        \toprule
        Regressors & \multicolumn{6}{c}{Specifications} \\
%
        \midrule
        \multirow{2}{*}{Intercept}      & 2.9514    & -2.4500   & -5.035    & 3.2713    & 2.9535    & 2.3218    \\
                                        & [6.6129]  & [-5.1232] & [-0.5821] & [7.2467]  & [6.0714]  & [4.1461]  \\
%
        
        \rowcolor{row_background}       & 0.2424    &           & 0.1366    &           &           &           \\
        \rowcolor{row_background} \multirow{-2}{*}{$\log\left(\totalvolt\right)$ }
                                        & [6.6129]  &           & [2.6811]  &           &           &           \\
%
        \multirow{2}{*}{$\log\left(\jumppropt\right)$}          &           & -5.0088   & -4.1512   \\
                                                                &           & [-5.8559] & [-4.9267] \\
%
        \rowcolor{row_background}       &           &           &           & 0.2506    &           & 1.8565    \\                                 
        \rowcolor{row_background} \multirow{-2}{*}{$\log(\diffvolt)$} & & & & [6.5324]  &           & [5.1820]  \\                                 
%
        \multirow{2}{*}{$\log\left(\jumpvolt\right)$}    &  &  &            &           & 0.2309    & -1.7403   \\                                 
                                        &           &           &           &           & [5.4013]  & [-4.5294] \\                                 
%
        \bottomrule
    \end{tabularx}
\end{table}

The results are highly statistically significant. Contrary to much of the previous literature, I find returns are highly predictable at the daily level. As theory predicts, I find that the predictable parts of $\log(\totalvolt)$, $\log(\diffvolt)$, and $\log(\jumpvolt)$  have strong positive relationships with returns in univariate regression. Conversely, when we run a bivariate regression the coefficient associated with jumps changes sign. \Cref{tbl:log_vol_correlations} shows that $\log(\diffvolt)$ and $\log(\jumpvolt)$ are highly positively correlated.  Consequently, it should not be surprising that $\log(\jumpvolt)$ has different signs in the univariate and bivariate regressions. The regression on the jump proportion has similar results because it also adjusts for movements in $\totalvolt$.  

We want to interpret the magnitude of the coefficients, not just the sign.  Since I regress annualized excess log-return on the $\log(\totalvolt)$  and $\log(\jumppropt)$, the estimates are elasticities.  These elasticities are highly statistically and economically significant. Consider the first column.  The elasticity of $\rxt$ with respect to $\totalvolt$ equals \num{0.2424}.  In other words, a \SI{1}{\percent} increase in $\totalvolt$ for an entire  year increases the expected yearly return by \SI{0.2424}{\percent}.\footnote{The reason that I only considered a \SI{1}{\percent} change is that the approximation of log-differences as percent differences only holds for small changes.} For comparison, the average year-to-year difference in average $\totalvolt$ in my sample is $\approx\SI{50}{\percent}$.  It increased by $\approx\SI{150}{\percent}$ between 2007 and 2008.

The average annual absolute difference in $\jumppropt$ is lower, equaling \SI{6.1266}{\percent}, but the regression coefficient is substantially larger. A $\SI{1}{\percent}$ change in $\jumppropt$ for an entire year changes expected returns by $\SI{-5.0088}{\percent}$. In both cases, the implied movements in risk premia from year to year are substantial.  I am not the first researcher to find movements in risk premia that are this large.  \Textcite{martin2017what} reports changes of similar magnitude.

As we would expect, when risk as measured by $\diffvolt$, $\jumpvolt$, or $\totalvolt$ increases, expected returns increase. Surprisingly, once we control for $\diffvolt$  the jump (news) premium is negative. It is always smaller than the diffusion volatility premium. This stylized fact is rather difficult to explain using standard finance models for two reasons. First, it means that two risk factors move at the daily frequency or faster. Second, it means that the shocks that are large relative to the amount of time over which they occur command a smaller premium than shocks which are small relative to the amount of time over which they occur. I am not the first person to find counter-intuitive relationships between risk premia and the size of shocks. See, for example, \textcite{dewbecker2019hedging}. I also consider several other specification and setups to this problem in the appendix. The results either agree or are statically insignificant.

