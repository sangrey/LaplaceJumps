\documentclass[11pt]{amsart}
\pagestyle{plain}
\input{.default_tex_env.tex}
\input{.laplace_tex_env_extra.tex}
\addbibresource{density_estimation.bib}

\title{Daily Return Density Estimation}
\date{\today}
\author{Paul Sangrey}
\thanks{\textit{Email}: sangrey@sas.upenn.edu}


\begin{document}

\maketitle

In the literature on continuous-time diffusion models, various authors show you can represent the short-horizon
dynamics of the price process in both discrete- and continuous-time in terms of the instantaneous diffusion
 volatility $\sigma(t)$ under some minor regularity conditions. 
However, when jumps are added to these models to match the data's dynamics, these representations break down,
and the representations used instead are rarely identified and often rather intractable.
I show under some economically innocuous assumptions that a similar tractable representation holds for the jumps
in terms of the instantaneous jump volatility, which I define precisely below. 
This representation provides closed-form expressions for the discrete- and continuous-time dynamics in terms of
these volatility functions.    
I estimate these volatilities and the implied discrete- and continuous-time densities using high-frequency data. 

To make concepts explicit, consider the following simple model, where we use $\mathcal{J}$ to refer to the set of
jumps.

\begin{equation}
    \dif Y_t = \sigma_t  \dif W_t + J_s 1\lbrace s \in \mathcal{J} \rbrace
    \label{eqn:simplemodel}
\end{equation}

The only source of dynamics in the diffusion part of the process above comes through $\sigma_t$ because the
increments of Brownian motion are i.i.d. Gaussian.
In the jump part however, there are ostensibly two sources of variation variation in the jump magnitudes and when
do the jumps occur. 
In other words, what is the size of $J_s$, and what times are in $\mathcal{J}$?
To understand the dynamics of the diffusion part, we have to precisely define the instantaneous volatility
$\sigma_t$.
The diffusion volatility locally governs the scale of the continuous part of process.
Let $\F_t$ be the filtration of information available at time $t$ and superscript $c$ denote the continuous part
of the process.

\begin{equation}
    \sigma^2_t \coloneqq \lim_{\Delta \to 0}\ \frac{1}{\Delta} \E\left[\abs*{Y^C_{t + \Delta} - Y^C_t}^2
     \middle\vert \F_{t}\right] 
\end{equation}

In the jump part, we can define the completely analogous instantaneous jump volatility. 
Let superscript $j$ refer to the jump part of the process.

\begin{equation}
    \kappa^2_t \coloneqq \lim_{\Delta \to 0} \frac{1}{\Delta} \E\left[\abs*{Y^J_{t + \Delta} - Y_t^J }^2
     \middle\vert \F_t \right]
\end{equation}

Now, $\kappa^2_t$ is neither the jump intensity nor the jump magnitude.
Since it is a variance, it contains elements of both sorts of variation.   
To refer back to the simple model in \cref{eqn:simplemodel} $Y^j_{t + \Delta} - Y^j_t$ is the sum of jumps in the
interval $[t, t + \Delta]$.


\begin{equation}
    Y_{t+ \Delta} - Y_t = \sum_{t < s \leq t+\Delta} J_s 1\lbrace s \in \mathcal{J}\rbrace
\end{equation}

To further specialize the model, assume that the jumps are i.i.d, and the magnitudes and jump locations are
independent.
In that case, we can decompose the variance of the jump part as follows. 

\begin{alignat}{2}
    & \kappa^2_t &&= \lim_{\Delta \to 0} \frac{1}{\Delta} \E\left[\abs*{Y_{t + \Delta} - Y_t}^2 \middle\vert
        \F_{t}\right]  \\
    & &&= \lim_{\Delta \to 0} \frac{1}{\Delta} \E_{\sigma(t \in \mathcal{J})}\left[ \E\left[\abs*{Y^J_{t +
          \Delta} - Y_t}^2 \middle\vert \F_{t}, \sigma(t \in \mathcal{J}) \right]\right] \\
    & &&= \lim_{\Delta \to 0} \frac{1}{\Delta} \E_{\sigma(t \in \mathcal{J})}\left[ (\# t \in \mathcal{J}) 
      \E\left[ \abs*{J_s}^2\right]\right] \\ 
    & &&= \lim_{\Delta \to 0} \frac{1}{\Delta} \E[(\# t \in \mathcal{J})] \E[\abs*{J_s}] 
      \label{eqn:jump_variance_decomposition}
\end{alignat}
   
In other words, the variance of the jump process is the mean of the intensity multiplied by the variance of the
magnitude, rescaled appropriately.
For example, if the arrival times are Poisson distributed with arrival rate $\lamdba = \Delta$, and the
magnitude are i.i.d $\N(0,1)$, then $\kappa^2_t = 1$. 
Specifically, we are composing the variation from the jump locations and the jump magnitude into one parameter.

Considering \cref{eqn:jump_variance_decomposition}, if we change either the intensity of the jumps or the
variance of the jump magnitudes, the variance of $Y^J_t$ changes in exactly the same way.
This implies that $\kappa_t$ is a sufficient statistic for the variance of the jump part of the process.
In addition, since the jump part is a martingale, we can pin down the first moment of the process as well.
So the question becomes when do the first two moments of pin down all of the dynamics of $Y^J_t$?
As an aside, the other useful feature of variances is that  they are often identified from high-frequency data,
unlike many of the process's other statistics,   

When we take the standard nonparametric representation for the jumps used in the literature and add a few
economically innocuous assumptions, we can represent the dynamics of the jump part using $\kappa_t$  
Specifically, the model considered above is equivalent to the following model where $\Lap(t)$ is a Laplace
(Variance-Gamma) process.

\begin{equation}
    \dif Y_t = \sigma_t \dif W_t + \frac{\kappa_t}{\sqrt{2}} \dif \Lap_t   
\end{equation}  

A Laplace process is a very common process used in the literature to represent processes with a great deal of
jumps. 
For example, various authors have derived closed-form formula for pricing options when the underlying process is a
Laplace process.
Its increments are i.i.d. Laplace random variables, completely analogously to how the increments of a Wiener
process are i.i.d Gaussian random variables.  

In addition, it is the process that you get if you have a jump process i.i.d Gaussian magnitudes and a Poisson
arrival rate when you take the arrival rate of the process to infinity in a way that preserves finite variance of
the original process.    
Requiring the process and its square to be well-defined constrains the relationship between  magnitudes and
their intensities.
This additional constraint along with the mean and variance of the process pins down all of the higher moments of
the process.

You can also show that a finite activity process has the representation above in intervals where it has jumps.
Otherwise, the jump part of the process is simply zero. 
Consequently, completely analogously to how we can represent the dynamics of the diffusion part of the process in
terms of $\sigma_t$ and estimate $\sigma_t$ from high-frequency data, we can represent the dynamics of the jump
part of the process in terms of $\kappa_t$ and estimate it from high-frequency data.

\end{document}


