
\section{Volatility: Empirics} \label{sec:vol_empirics} 

I separate this empirical part into two subsections. The first section characterizes the static properties of the volatilities, and the second characterizes their dynamic properties. In particular, it shows that both volatilities are highly persistent, displaying long-memory. 

\subsection{Statics}\label{sec:vol_statics}

The results concerning $\diffvolt$ are broadly consistent with previous work on the topic. Since this paper introduces $\jumpvolt$, the stylized facts regarding its features are new. Thankfully, in practice, the volatilities have very similar dynamics, and so much of the intuition regarding $\diffvolt$ can be directly translated to \jumpvolt. 

As can be seen in \cref{fig:root_volatilites}, the volatilities are very closely related; their correlation equals \num{0.92858}. They both significantly increase during crises. Interestingly, $\diffvolt$ spiked more than $\jumpvolt$ did during the Financial Crisis and seems to spike more during other recessions as well. 

\begin{figure}[htb]
  \caption{Volatilities}
  \begin{subfigure}[t]{.49\textwidth}
    \caption{Time Series}
    \label{fig:root_volatilites}
    \centering

    \includegraphics[width=\textwidth, height=.6\textwidth]{volatilities.pdf}
 \end{subfigure}
%
 \hfill
%
  \begin{subfigure}[t]{.49\textwidth}
    \caption{Log Densities}
    \label{fig:log_vol_densities}
    \includegraphics[width=\textwidth, height=.6\textwidth]{log_vol_distributions.pdf}
  \end{subfigure}
\end{figure}

\Cref{fig:log_vol_densities} plots the two marginal log-volatility distributions along with their joint distribution. As can be seen from the graph, both marginal distributions are skewed right.\footnote{The only reason that the diffusion density might appear to be skewed left is that it is plotted sideways.} This skewness implies that the volatilities are more likely to take on abnormally large values than take on abnormally small ones. This fact is particularly noteworthy as these are distributions of log-volatilities, and taking the logarithm already removes a large amount of the skewness. 

\Textcite{andersen2001distributionstock} argue that realized volatilities are approximately log-Gaussian. One might expect this to continue to hold in this case. The black lines in \cref{fig:log_vol_densities} are Gaussian densities fit to the data for comparison purposes. At a qualitative level, the log-volatilities are roughly log-Gaussian. They are slightly skewed and slightly kurtotic, even after taking logs, which we can also see in \cref{tbl:vol_summary_stats}.

\begin{table}[htb]
 \centering
 \caption{Volatility Summary Statistics}
 \label{tbl:vol_summary_stats}
 \setlength{\tabcolsep}{2.5pt}

 \sisetup{
  detect-mode,
  tight-spacing   = true,
  group-digits   = false,
  input-symbols   = {(}{)},
  input-open-uncertainty = ,
  input-close-uncertainty = ,
  round-mode    = places,
  round-precision   = 2,
  table-align-text-pre = false,
  table-align-text-post = false,
  table-alignment   = center,
 }

 \begin{tabularx}{\textwidth}{Z | *{7}{S}}
  \toprule
  & {\diffvolt} & {\jumpvolt} & \jumppropt & {$\log(\diffvolt)$} & {$\log(\jumpvolt)$} &
   {$\log\left(\totalvolt\right)$} & {$\log\left(\jumppropt\right)$} \\
  \midrule
%
  Mean & {\num[scientific-notation=true]{4.468e-5}} & {\num[scientific-notation=true]{3.6780e-5}} &
  0.5644 & -10.9100 & -10.6357 & -13.1517 & -2.1736 \\
%
  \rowcolor{row_background} Std.\@ Dev.\@ & {\num[scientific-notation=true]{1.5203e-4}} &
  {\num[scientific-notation=true]{9.1199e-5}} & 0.1155 & 1.1303 & 0.9843 & 1.0259 & 0.2222 \\ 
%
  Skew.\@ & 15.6487 & 11.8053 & -0.1786 & 0.7077 & 0.5464 & 0.7185 & -0.9475 \\ 
%
  \rowcolor{row_background} Kurt.\@ & 376.5451 & 250.2284 & 2.9225 & 4.1221 & 3.8055 & 4.1012 & 4.8770 \\
%
  \bottomrule
 \end{tabularx} 

\end{table}

We are interested not just in the volatilities' univariate dynamics, but also in their interrelationships. We know from \cref{fig:log_vol_densities} that the volatilities move together. To investigate this further, \cref{tbl:vol_correlations} reports the correlations between the various volatility measures and daily excess returns.

\begin{table}[htb]

 \caption{Volatility Correlations}

 \label{tbl:vol_correlations}
 \centering

 \begin{tabularx}{.9\textwidth}{X | *{5}{S}}
  \toprule
  & {\diffvolt} & {\jumpvolt} & {\totalvolt} & {\jumppropt} & {$\rxt$}\\
  \midrule
%
  \diffvolt & 1.000  & 0.7420 & 0.9634 & -0.2880 & -0.1144 \\
%
  \rowcolor{row_background}
  \jumpvolt & 0.7420 & 1.000  & 0.8946 & -0.0989 & -0.1315 \\
%
  \totalvolt & 0.9634 & 0.8946 & 1.000  & -0.2315 & -0.1289 \\
%
  \rowcolor{row_background}
  \jumppropt & -0.1144 & -0.1315 & -0.1289 & 1.0000 & 0.1243 \\
%
  \bottomrule
 \end{tabularx}
%
\end{table}

\Cref{tbl:vol_correlations} replicates standard results regarding the relationship between volatility and returns.
We also see that jump and diffusion volatility and highly positively correlated.

\subsection{Dynamics}\label{sec:vol_dynamics}

This section studies the dynamics of log-volatilities because they are closer to Gaussian, and so the true conditional expectations are likely closer to approximately linear.  
\Cref{tbl:var1_specification} reports independent $AR(1)$ regressions for each volatility and a joint $VAR(1)$ regression to gain some high-level understanding of the dynamics. Both series are quite persistent and predictable. However, we still have economically significant innovations.\footnote{This section's results come with the significant caveat that I am using estimated regressors and do not correct for this in my statistical results. For the most part, the evidence is so overwhelming the conclusions should not be affected, but, in some of the more borderline cases, it may be an issue.}

\begin{table}[htb]
 \centering 

  \caption{Autoregressive Models}
  \label{tbl:var1_specification}

  \begin{tabularx}{\textwidth}{l | Y *{3}{X} c} 
%
    \toprule 
              & Intercept             & $\log(\diffvol{t-1})$     & $\log(\jumpvol{t-1})$     & Innovation Variance  & $\R^2$          \\
    \midrule 
    & \multicolumn{5}{c}{AR(1)} \\
    \midrule 
%
    $\log(\diffvolt)$  & \num{-1.6340}           & \num{0.8503}         &                & \num{0.31}      & \formatRSquared{72.206}  \\
              & (\num{-1.8210}, \num{ -1.4485})  & (\num{0.8333}, \num{ 0.8673}) &                &            &              \\
    \rowcolor{row_background}
    $\log(\jumpvolt)$  & \num{-1.7789}           & \num{0.8329}         &                & \num{0.25}      & \formatRSquared{69.322}  \\
    \rowcolor{row_background}
              & (\num{-1.9694}, \num{-1.5884})  & (\num{0.8151}, \num{ 0.8507}) &                &            &              \\
%
    \midrule 
    & \multicolumn{5}{c}{VAR(1)} \\
    \midrule 
%
    $\log(\diffvolt)$  & \num{-0.844099}          & \num{0.556361}        & \num{0.375805}        & \num{0.3311}     & \\
              & (\num{-1.0446}, \num{-0.6436})  & (\num{0.5192}, \num{0.5935}) & (\num{0.3331}, \num{0.4185})                         \\
    \rowcolor{row_background}
    $\log(\jumpvolt)$  & \num{-1.8001}           & \num{0.3417}         & \num{0.4804}         & \num{0.2702}     & \formatRSquared{72.2961} \\
    \rowcolor{row_background}
              & (\num{-1.9811}, \num{-1.6190})  & (\num{0.3082}, \num{0.3753}) & (\num{0.4418}, \num{0.5189})  &            &              \\
    \bottomrule
  \end{tabularx}

\end{table}

\subsection{Realized Density Evaluation}\label{sec:realized_density_estimation}

\Cref{sec:estimator_performance} showed that the estimators work well in simulations. It would be useful to know if they worked well in the data as well. Besides, perhaps the assumptions justifying the integrated-Laplace representation do not hold in practice. Thankfully, \Cref{thm:realized_density_representation} is a valid conditional density, and we can consistently estimate the conditioning variables. Consequently, techniques developed to analyze conditional densities work well here.

\begin{figure}[htb]
     \centering
     \caption{Realized Density Evaluation}
     \label{fig:real_density_eval}
    
     \begin{subfigure}[t]{.49\textwidth}
      \caption{PIT}
      \label{fig:real_density_pit}
      \includegraphics[width=\textwidth, height=.2\textheight]{jump_diff_realized_density_pit_paper.pdf} 
     \end{subfigure}
%
     \hfill%
%
     \begin{subfigure}[t]{.49\textwidth}
      \caption{PIT ACF}
      \label{fig:pit_acf}
      \includegraphics[width=\textwidth, height=.2\textheight]{realized_density_jump_diffusion_pit_acf_paper.pdf} 
     \end{subfigure}
    %
\end{figure}


Each day, I take the $\widehat{\diffrootvol{}}_t^2$ and $\widehat{\jumprootvol{}}_t^2$ and compute $\widehat{\RDt}$. I can draw from $\widehat{\RDt}$ easily, and so I compute the probability integral transform (PIT) of the demeaned daily return using simulation.   This procedure jointly evaluates the density representation, the time-aggregation procedure, and the estimation of the parameters.


As can be seen in \cref{fig:real_density_eval}, the PIT is close to uniform. The only deviation is in the far right tail. I did not correct for the skewness in the data when I computed $\RDt$, i.e., the volatility and returns are not independent at the daily level. We can see this in the graph. However, the deviation is not large, and for most risk-measures, we are far more concerned about the left-tail. This procedure estimates that tail almost perfectly. It is also worth noting that I needed to assume this symmetry in the discrete-time representation, but not the continuous-time one. The deviations here do not invalidate that representation at all. Furthermore, \cref{fig:pit_acf} shows the deviations are most perfectly uncorrelated across time. This lack of correlation implies that the densities' dynamics are estimated quite well.
