
\section{Literature Review}\label{sec:lit_review}

Since questions concerning volatility, news, and risk-return trade-offs are central to finance and economics, a few different literatures study the questions considered in this paper.  Consequently, I cannot hope to survey the literature adequately.  I can only cover a few of the closest related papers.  

\subsection{Jumps in Asset Prices}\label{sec:jumps_in_prices}

The first literature that I build upon is the econometrics literature that studies jumps in asset prices.  \Textcite{barndorff2006econometrics} develop the bipower variation estimator  to disentangle jumps and diffusive variation.  Since then, several authors have shown that jumps are both frequent and economically important, including \textcites{andersen2007rouphing, bollerslev2008risk, aitsahalia2009testing}.  The critical difference between my estimates of jump variation and previous bipower variation estimates is that I measure ex-ante jump variation, while previous papers measure ex-post variation.  This difference is essential for two reasons.  First, my density characterization relies upon an ex-ante characterization.  Second, investors price ex-ante risk, and so my measure is a core object in pricing, while ex-post jump variation cannot be priced.  Other authors have argued they are not just statistically significant, but economically as well.  For example, we also need them to price derivatives, such as \parencites{pan2002jumprisk, branger2008optimal, todorov2010variance, todorov2011econometric}. 


In \cref{sec:datas_stylized_features}, I discuss the literature that measures the magnitude of jump variation and the jump intensities.  I will not repeat that discussion here except to recall the twofold consensus.  First, asset prices contain a vast number of jumps.  Jumps are likely infinitely-active, or, at a minimum,  have a very high intensity.  Second, jumps constitute an economically and statistically significant portion of the price variation.

I rely on these results in three ways.  First, as motivation for the project.  Second, as evidence that my empirical results are reasonable.  Third, and most importantly, I rely heavily on these empirical facts in that I assume prices have infinitely-active jumps.  This assumption is somewhat unusual, but not unique.  For example, \textcite{gallant2018exact} considers a similar class of processes.

\Textcite{gallant2018exact} is arguably the closest related paper in the econometrics literature.  It is the only other paper that nonparametrically relates jump variation to the distribution of returns.  It is a fascinating paper and provides useful estimates for the intensity of jump processes.  However, their representation relies on \textcite{todorov2014limit} and so can only handle small jumps.

\subsection{Representing Price Processes}\label{sec:representing_stochastic_processes}

The second literature that this paper builds upon is the stochastic process representation literature.  The main contribution to this literature is \cref{theorem:timechangedlaplace} and its corollaries. This theorem provides general conditions under which jump processes are stochastic volatility variance-gamma processes.  The variance-gamma process is a L\'{e}vy process first introduced by \textcite{madan1998variance}.

The first main time-change method for representing price processes is the Dambis, Dubins \& Schwarz theorem, \parencites{dambis1965decomposition, dubins1965continuous}.  \Cref{theorem:timechangedlaplace} is the jump analog of that theorem.  \textcite{epps1976theory} and various subsequent authors relate this time-change to \textquote{business-time,} that is the speed at which information gets released into the market, creating the mixture-of-distributions hypothesis.  

Various authors partially extend these results to the jump case.  \Textcite{monroe1978process} shows that any semimartingale can be embedded into Brownian motion, but did not construct this embedding explicitly.  \Textcite{geman2002stochastic} shows that this embedding is not identified.  More recently, \textcite{todorov2014limit} show how to embed the jump processes' infinitesimal jumps into an $\alpha$-stable process using bipower-variation. Infinitesimal means, here, that the maximum jump size approaches zero as the increment size approaches zero. By using an ex-ante measure of jump variation, instead of an ex-post one like \textcite{todorov2014limit} do, I can handle large jumps as well.

In \cref{sec:realized_density}, I time-aggregate these continuous-time representations to discrete-time under some additional assumptions.  In doing this, I follow \textcites{barndorff2002econometric,andersen2003modeling} who provide analogous results for diffusive processes.  \Textcite{barndorff2010change} further analyze these representations, providing a useful survey of the current state of the literature.

\subsection{Measuring Risk-Return Trade-off}\label{sec:asset_pricing_recursive_utility}

The curvature in investors' preferences, i.e., their risk appetite, implies a negative relationship between expected returns and volatility.  Consequently, many different papers estimate this relationship, and  I cannot comprehensively survey this literature.  Surprisingly, the empirical evidence has proven much less conclusive than the theory.  \Textcites{harvey1989timevarying, ghysels2005there, lettau2010measuring} find a positive relationship between expected returns and volatility.  \textcites{campbell1987stock, brandt2004relationship} actually find a negative relationship.  Besides, many authors argue that the instantaneous correlation, which is often called a \textquote{volatility-feedback} or leverage effect is negative, both in continuous-time \parencites{bandi2012timevarying, aitsahalia2013leverage} and in discrete-time \parencites{engle1993measuring, yu2005leverage}.  This negative sign is likely the main reason why estimating the risk premium has proven difficult.  The researcher must disentangle two different relationships, risk-premia and volatility feedback, that have opposite signs. 


