\section{Simulations}\label{sec:estimator_performance}

One of my representation's key advantages is that we can simulate from it easily whenever we can simulate the instantaneous volatilities. Perhaps the most commonly used model for the diffusion volatility is the Cox-Ingersoll-Ross (CIR) process, also known as the square-root process. (A diffusion model whose volatility follows a CIR process is the Heston model.) It has the following form $\dif x(t) = \kappa (\theta - (x(t)) + \omega \sqrt{x(t)} \dif W(t)$, where $\theta$ is the asymptotic mean, $\kappa$ is the mean-reversion rate, and $\omega$ is a scale parameter. 

One nice feature of this model is that the volatility itself has volatility, but we only need to simulate one process. The qualitative features of the jump and diffusion volatilities are quite similar in practice, and so I adopt this model for the jump volatility. 

\subsection{Simulation Data Generating Process}\label{sec:simulation_dgp}

I simulate a CIR process for both $\jumpvolf{t}$ and $\diffvolf{t}$ using the full-truncation scheme of \textcite{lord2010comparsion}. The parameters are given in \cref{tbl:simulation_vol_params}. I chose the specific parameter values displayed below to match the discrete-time dynamics of the data. 

\begin{table}[htb]
    \centering
    \caption{Volatility Parameters}
    
    \label{tbl:simulation_vol_params}
    
    \begin{tabularx}{.9\textwidth}{l *{3}{c} X} 
        \toprule
                        & $\theta$      & $\kappa$  & $\omega$      & Asymptotic Standard Deviation     \\
        \midrule
%
      \rowcolor{row_background} 
        \diffvolf{t}    & \num{5.0e-5}  & 1         & \num{2.1e-3}  & \num{4.6e-4}                      \\
        \jumpvolf{t}    & \num{5.0e-5}  & 1         & \num{2.1e-3}  & \num{4.6e-4}                      \\
%
      \bottomrule
    \end{tabularx}

\end{table}

Once I obtain $\diffvolf{t}$ and $\jumpvolf{t}$, I plug them into $\logpricef[\dif]{t} = \diffrootvol{(t)} \dif W(t) + \frac{\jumprootvol{(t)}}{\sqrt{2}} \dif \Lap(t)$. This gives me a sequence of prices, which I use to estimate the volatilities.

\subsection{Simulation Results}

This section focuses on the daily volatility results as they form sufficient statistics for all of the daily objects, which the applications study. This section also reports results for the truncation-based estimator used by \textcite{li2017jump}, (LTT); the bipower estimator of \textcites{barndorff2004power, podolskij2009bipower}, (Bipower); and bipower estimators computed on \num{5}-minute data (5 Minute) to provide a point of comparison. In the jump case, these estimators do not converge to $\jumpvolt$ but rather to the jumps' quadratic variation. However, since $\jumpvolt$ is the jumps' predictable quadratic variation, these estimators should still be asymptotically unbiased for $\jumpvolt$. 

I first estimate the model using the estimation procedure described in \cref{sec:estimation_framework} without the microstructure correction described there.  I report averages over \num{250} days. It is worth noting that the daily estimates are independent, i.e., I do not smooth across days.

\begin{table}[htb]
  \centering
  \caption{Relative Simulation Error without Microstructure}
  \label{tbl:simulate_results}
  \setlength{\tabcolsep}{3pt}

  \begin{tabularx}{\textwidth}{X | *{4}{S} | *{4}{S}}
    \toprule
%
    Obs.\@ per Min.\@ &
    \multicolumn{4}{c}{$\frac{\sqrt{\E[\left(\widehat{\diffrootvol{_t}} -
    \diffrootvol{_t}\right)^2]}}{\E[\diffrootvol{_t}]}$} &
%
    \multicolumn{4}{c}{$\frac{\sqrt{\E[\left(\widehat{\jumprootvol{_t}} - %
    \jumprootvol{_t}\right)^2]}}{\E[\jumprootvol{_t}]}$} \\
    \midrule
    & {BNS} & {LTT} & {5 Min.} & {Proposed} & {BNS} & {LTT} & {5 Min.} & {Proposed} \\ 
%
    \midrule
    $\approx 2$ & {\num{0.3737}} & 0.3990 & 0.4028 & 0.4559 & 0.7181 & 1.0080 & 0.8022 &
    {\num{0.7181}} \\
%
    \rowcolor{row_background} 
    $\approx 12$ & 0.3790 & 0.4041 & 0.4150 & {\num{0.1564}} & 0.6974 & 1.0074 & 0.8266 &
    {\num{0.2114}} \\
%
    $\approx 60$ & 0.4041 & 0.4341 & 0.4494 & {\num{0.0504}} & 0.6822 & 1.0079 & 0.8741 &
    {\num{0.0700}} \\
%
    \rowcolor{row_background} 
    $\approx 180$ & 0.3851 & 0.4139 & 0.4284 & {\num{0.0662}} & 0.6880 & 1.0075 & 0.8522
           & {\num{0.0668}} \\
    \bottomrule
  \end{tabularx}
\end{table}

The proposed estimators, however, perform quite well at this frequency outperforming the other estimators by approximately an order of magnitude. \Cref{tbl:simulate_results} reports the average root mean square errors of 250 days worth of various estimators.

\Cref{app:simulations} shows that the results are robust. \Cref{tbl:simulate_results_micro} reports a simulation with microstructure noise, which is adapted from \textcite{christensen2014fact}. The results are similar. The previous methods perform better when estimating $\diffvolt$, but the proposed method still substantially outperforms in estimating $\jumpvolt$. I also simulate a process where we have Poisson jumps and an average of one jump per day. Even though this dramatically violates the infinite-activity assumption, the proposed method still works well (\cref{tbl:simulate_results_poisson_jumps}).  

\section{Data} \label{sec:data}

The methods developed in this paper require high-frequency data. For the analysis to be interesting, we need a dataset that faces a dense stream of relevant news. I chose SPY (SPDR S\&P 500 ETF Trust) which is an exchange-traded fund that mimics the S\&P 500. I obtain it from the Trade and Quotes (TAQ) database at Wharton Research Data Services (WRDS).  

Since this paper only use one asset, and SPY is one of the most liquid assets traded, we can essentially choose the frequency at which we want to observe the underlying price. In order to balance market-microstructure noise, computational cost, and efficiency of the resulting estimators, I sample at the \num{1}-second frequency. The data used starts in 2003 and ends in September 2017. Since the asset is only traded during business hours, this leads to \num{3713} days of data with an average of $\approx\num{24000}$ observations per day. The dataset takes up about \SI[round-precision=1]{4.4}{\gibi\byte} of memory. It is also worth noting that SPY is by far the most liquid exchange-traded fund, especially in recent years, reducing the effect of market microstructure, such as bid-ask spreads, bounces, and rounding error. 

