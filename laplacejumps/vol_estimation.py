"""This module provides a series of functions to estimate the stochastic volatilities."""
import numpy as np
import pandas as pd
from scipy.optimize import minimize
from functools import lru_cache
from liblaplacejumps import jump_vol_error, est_local_abs_vol, bipower, mean_abs_vol
from liblaplacejumps import est_local_diffusion_vol, real_vol
from tqdm import tqdm_notebook


def est_jump_vol(sigma, abs_vol, standardize_val=1e-2, **kwargs):
    """
    Compute the jumps' volatility from diffusion and absolute volatilities.

    Parameters
    --------
    sigma : positive float
        The scale of the Brownian motion.
    abs_vol : positive float
        The mean of the absolute deviation.
    standardize_val : positive float
        A value to standardize the values so that the empirical procedure works better.
    kwargs :
        Additional paramters to pass to the minimizer

    Returns
    ------
    jump_vol : positive float

    """
    # The standarize_val must be positive.
    standardize_val = abs(standardize_val)

    if 'x0' not in kwargs:
        x0 = float(sigma) / standardize_val
    # This is the lower bounds for the root jump vol.
    if 'lb' not in kwargs:
        lb = 1e-8 / standardize_val

    if lb < 0:
        raise ValueError('The lower bound must be positive.')

    bounds = (lb, None)

    # The minimization finds the gamma associated with abs_vol / standardize_val, so we have to rescale the
    # final result.
    root_jump_vol = minimize(lambda x: jump_vol_error(abs_vol, x, sigma, standardize_val=standardize_val),
                             bounds=[bounds], x0=x0, **kwargs).x * standardize_val

    return float(root_jump_vol)**2


def bipower_for_df(data, freq='D', noise_vol=None):
    """
    Compute the bipower estimator of the data.

    Paramters
    --------
    x  : dataframe
    power1  : positive int
    power2  : positive int
    idx_diff : postive int, optional
    freq : str, optional

    Returns
    ------
    dataframe

    """
    quad_data = data.squeeze().groupby(pd.Grouper(freq=freq)).agg(bipower, 2, 0, idx_diff=0)
    diff_data = data.squeeze().groupby(pd.Grouper(freq=freq)).agg(bipower, 1, 1, idx_diff=1)
    if noise_vol is not None:
        noise_vol = pd.Series(np.ravel(noise_vol), index=noise_vol.index)
    else:
        noise_vol = pd.Series(0, index=diff_data.index)

    bipower_vol = pd.concat([quad_data, noise_vol, diff_data], axis=1, join='outer')
    bipower_vol.columns = ['quad', 'noise', 'diffusion']
    bipower_vol['quad'] -= bipower_vol['noise']
    bipower_vol['diffusion'] -= bipower_vol['noise']
    bipower_vol['jumps'] = pd.Series(bipower_vol['quad'] - bipower_vol['diffusion'])

    bipower_vol.clip_lower(0, inplace=True)

    return bipower_vol


def mean_abs_vol_from_df(vol_df, kappa=250):
    """
    Compute the expectation of the absolute value of the log_rtn given its jump and diffusion volatility.

    Parameters
    ---------
    vol_df : dataframe
        This must contain two columns 'diffusion' and 'jumps' containing the standard deviations of the jump
        and diffusion volatility. Note: these are in terms of standard deviations.
    kappa : positive int
        The number of observations in each bin.

    Returns
    -------
    vol : dataframe

    """
    data = vol_df.applymap(np.sqrt).iloc[::kappa].agg(lambda x: mean_abs_vol(x.diffusion, x.jumps), axis=1)
    vol = pd.DataFrame(data, columns=['absolute'])

    return vol


@lru_cache()
def gfun(x):
    """Compute the pre-averaging weights."""
    return (1 - (2 * x - 1)**2) * (x >= 0) * (x <= 1)


@lru_cache()
def gprimefun(x):
    """Compute the derivative of the pre-averaging weights."""
    return -4 * (2 * x - 1) * (x >= 0) * (x <= 1)


def preaverage_data(log_returns, preaverage_dim):
    """
    Compute the preaveraged data given log returns.

    The preavaraged data has a different mean, but the same volatility as the original data.
    It does the differencing each day, thereby dropping the first observation.

    Parameters
    -----------
    log_returns : dataframe
    preaverage_dim : positive int

    Returns
    ------
    dataframe

    """
    dim = preaverage_dim

    psi2 = compute_psi_vals(dim)[1]
    g_weights = np.asarray([gfun(val / dim) for val in np.arange(1, dim + 1)])
    preaveraged_data = log_returns.groupby(pd.Grouper(freq='D')).rolling(window=dim, min_periods=dim).apply(
        lambda x: x.dot(g_weights), raw=True).reset_index(level=0, drop=True)

    return preaveraged_data.dropna() / np.sqrt(preaverage_dim * psi2)


def compute_noise_vol(arr, kappa, theta=0.5, scale=False):
    """
    Compute the volatility of the noise.

    Parameters
    --------
    arr: arraylike
    kappa : pre-averaging dim
    theta : proportion of observations used for the pre-averaging, optional

    Returns
    -------
    noise_vol : float

    """
    psi1, psi2 = compute_psi_vals(kappa)
    noise_vol = .5 * np.mean(arr**2)
    if scale:
        noise_vol *= (psi1 / (psi2 * theta**2))

    return noise_vol


@lru_cache()
def compute_psi_vals(preaverage_dim):
    """
    Compute the psi values used in the pre-averaging.

    Parameters
    ---------
    preaverage_dim : positive int

    Returns
    -------
    tuple of floats

    """
    psi1_n = np.sum([(gfun((i + 1) / preaverage_dim) - gfun(i / preaverage_dim)) ** 2
                     for i in range(preaverage_dim)]) * preaverage_dim
    psi2_n = np.sum(gfun(i / preaverage_dim)**2 for i in range(1, preaverage_dim)) / preaverage_dim

    return psi1_n, psi2_n


def jump_regressions_estimator(data):
    """
    Compute the jump volatilty estimated by Li, Todorov, and Tauchen (2017).

    Paramters
    --------
    data: dataframe

    Returns
    -------
    diff_vol : dataframe
    jump_vol : dataframe

    """
    bipower_est = data.groupby(pd.Grouper(freq='D')).agg(lambda x: (x.size / (x.size - 1)) *
                                                         bipower(x, 1, 1, idx_diff=1))
    trunc_threshold = (3 * np.sqrt(bipower_est).T * (data.groupby(pd.Grouper(freq='D')).count()**(-.49)).T).T

    day_it = data.groupby(pd.Grouper(freq='D'))

    def compute_vol(data, thresh, jumps=True):

        if jumps:
            bool_indx = (abs(data) > float(thresh))
        else:
            bool_indx = (abs(data) < float(thresh))

        vol = real_vol(data[bool_indx].dropna())

        return vol

    jump_vol = pd.DataFrame.from_records([(date, compute_vol(data, thresh)) for (date, data), (_, thresh) in
                                          zip(day_it, trunc_threshold.iterrows())])

    diff_vol = pd.DataFrame.from_records([(date, compute_vol(data, thresh, False)) for (date, data), (_, thresh)
                                          in zip(day_it, trunc_threshold.iterrows())])

    jump_vol.rename(columns={0: 'datetime', 1: 'jumps'}, inplace=True)
    jump_vol.set_index(keys='datetime', drop=True, inplace=True)
    diff_vol.rename(columns={0: 'datetime', 1: 'diffusion'}, inplace=True)
    diff_vol.set_index(keys='datetime', drop=True, inplace=True)
    diff_vol = diff_vol.astype(np.float)
    jump_vol = jump_vol.astype(np.float)

    vol_data = pd.concat([diff_vol, jump_vol[np.isfinite(diff_vol)].fillna(0)], axis=1).astype(np.float)

    return vol_data.dropna()


def compute_instantaneous_vol(data, infill_dim, kappa, noise_vol=None, std_val=1e-2, tqdm=True, **kwargs):
    """
    Compute the instantaneous volatilities.

    Parameters
    --------
    data : dataframe
    infill_dim : scalar
        The number of observations per day.
    kappa : scalar
        The local period to average over.
    noise_vol : dataframe, optional
        The volatility of the noise to subtract from the diffusion volatility.
    std_val : scalar, optional
        The value to standardize the value the jump vol estimation by.
    tqdm: bool, optional
        Whether to use progress bars from tqdm.
    **kwargs
        Additional arguments to pass to the minimization algorithm.

    Returns
    ------
    return_vol : dataframe
        The diffusion, absolute, jump, and noise volatilities.

    """
    if tqdm:
        it1 = tqdm_notebook(np.array_split(data.values, data.size // kappa), total=data.size // kappa)
        it2 = tqdm_notebook(np.array_split(data.values, data.size // kappa), total=data.size // kappa)
    else:
        it1 = np.array_split(data.values, data.size // kappa)
        it2 = np.array_split(data.values, data.size // kappa)

    index = data.index[::kappa][:int(data.size // kappa)]

    vol_est = pd.DataFrame([est_local_abs_vol(x, infill_dim=int(infill_dim)) for x in it1],
                           columns=['absolute'], index=index)

    vol_est['diffusion'] = pd.Series([est_local_diffusion_vol(10 * x, infill_dim=int(infill_dim))
                                      for x in it2], index=vol_est.index).astype(np.float) / 100

    if noise_vol is not None:
        vol_est = pd.merge_ordered(vol_est.reset_index(), noise_vol.reset_index(), right_on='date_time',
                                   left_on='date_time', fill_method='ffill').set_index('date_time')

        vol_est.rename(columns={pd.DataFrame(noise_vol).columns[0]: 'noise'}, inplace=True)

        vol_est['diffusion'] -= vol_est['noise']

    vol_est.clip(lower=0, inplace=True)

    if tqdm:
        row_it = tqdm_notebook(vol_est.dropna().itertuples(), total=vol_est.diffusion.count())
    else:
        row_it = vol_est.dropna().itertuples()

    jumps = pd.DataFrame.from_records([(row.Index, est_jump_vol(sigma=row.diffusion**(.5), abs_vol=row.absolute,
                                                                standardize_val=std_val, **kwargs))
                                       for row in row_it])

    return_val = pd.merge(vol_est, jumps, left_index=True, how='outer', copy=False,
                          right_on=0).set_index(0).rename(columns={1: 'jumps'})

    return return_val.dropna()
