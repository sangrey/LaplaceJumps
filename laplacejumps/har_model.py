import numpy as np
import pandas as pd
from statsmodels.regression import linear_model
from functools import cmp_to_key
from scipy import linalg as scilin
from patsy import dmatrix
import re
from scipy.optimize import minimize
from collections import defaultdict, OrderedDict
from functools import reduce
from laplacejumps import _har_density_forecast, _excess_rtn_density_forecast, _har_forecast


def tree():
    """Create a tree data structure."""
    return defaultdict(tree)


class HarResults(linear_model.RegressionResultsWrapper):

    def __init__(self, exog_formula, endog_formula, constrained_vol_premium=False, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self._exog_formula = exog_formula
        self._endog_formula = endog_formula
        self.estimates = None
        self.constrained_vol_premium = constrained_vol_premium

    @property
    def regressand_names(self):
        return list(self.model.regressand_names)

    @property
    def regressor_names(self):
        return list(self.model.regressor_names)

    @property
    def regressand_dim(self):
        return self.model.regressand_dim

    @property
    def regressor_dim(self):
        return self.model.regressor_dim

    @property
    def params(self):
        return self._params

    @params.setter
    def params(self, x):
        self._params = pd.DataFrame(x)
        x.columns = self.regressand_names
        x.index = self.regressor_names

    @property
    def exclude(self):
        return self.model.exclude

    @property
    def lags(self):
        """Compute the dictionary representation of the lags from the _exog_formula property."""
        return self.model.lags

    @property
    def regression_coefficient(self):
        """Compute the regression coefficient representation."""
        coeff = convert_lag_dict_to_coeff(self.lags, self.regressand_names, self.params)

        return coeff

    @property
    def regressors(self):
        """Return the regressors in a matrix format, i.e. X from OLS."""
        return self.model.regressors

    @property
    def mean(self):
        """Return the regressand's mean."""
        mean = self.predict(h=2000).iloc[-1]

        return mean

    @property
    def weights(self):
        return self.model.weights

    def predict(self, exog=None, h=None):
        """
        Forecast the data.

        A generalization of the predict method from the superclass to enable multi-step forecasts from the end
        of the data.  One of exog and h must be specified.

        Parameters
        ---------
        exog: dataframe, optional
        h: positive int, optional

        Returns
        -------
        dataframe

        """
        if exog is not None:
            regressors = pd.DataFrame(dmatrix(self._exog_formula, data=exog, return_type='dataframe'))
            returnval = pd.DataFrame(regressors.values @ self.params.values, columns=self.regressand_names)
        elif h is not None:

            rxt_coeff_indices = np.ravel(self.regression_coefficient.index.get_loc('excess_rtn'))
            rxt_innov_indices = np.ravel(self.innov_cov.index.get_loc('excess_rtn'))
            coeff_indices = np.ravel([self.regression_coefficient.index.get_loc(label) for label in
                                      self.regressand_names])

            intercept = np.zeros(self.regression_coefficient.shape[0])
            intercept[coeff_indices] = self.params.iloc[0]

            prediction = _har_forecast(regressor=np.ravel(self.regressors.iloc[-1]), intercept=intercept,
                                       coeff=self.regression_coefficient, H=h,
                                       reggressand_coeff_indices=coeff_indices,
                                       rxt_innov_indices=rxt_innov_indices, rxt_coeff_indices=rxt_coeff_indices)

            returnval = pd.DataFrame(prediction, columns=self.regressand_names)

        else:
            raise ValueError("Either h or exog must not be zero.")

        return returnval

    def density_predict(self, H, forecast_dim=None):
        """
        Provide h-step density predictions of the data.

        Parameters
        ---------
        H: potive int

        Returns
        --------
        dataframe

        """
        rxt_coeff_indices = np.ravel(self.regression_coefficient.index.get_loc('excess_rtn'))
        rxt_innov_indices = np.ravel(self.innov_cov.index.get_loc('excess_rtn'))
        coeff_indices = np.ravel([self.regression_coefficient.index.get_loc(label) for label in
                                  self.regressand_names])

        if 'quad' in self.regressand_names and 'log_prop' in self.regressand_names:
            log_prop_form = True
        elif 'diffusion' in self.regressand_names and 'jumps' in self.regressand_names:
            log_prop_form = False
        else:
            raise ValueError("""Only the [total volatility and jump proportion]
                                and [diffusion and jumps] are currently supported """)
        intercept = np.zeros(self.regression_coefficient.shape[0])
        intercept[coeff_indices] = self.params.iloc[0]

        if forecast_dim is None:
            prediction = _har_density_forecast(regressor=np.ravel(self.regressors.iloc[-1]), intercept=intercept,
                                               coeff=self.regression_coefficient, innov_cov=self.innov_cov, H=H,
                                               reggressand_coeff_indices=coeff_indices,
                                               log_prop_form=log_prop_form, rxt_innov_indices=rxt_innov_indices,
                                               rxt_coeff_indices=rxt_coeff_indices)

            returnval = pd.DataFrame(prediction, columns=self.regressand_names)
        else:
            prediction = _excess_rtn_density_forecast(regressor=np.ravel(self.regressors.iloc[-1]),
                                                      intercept=intercept, coeff=self.regression_coefficient,
                                                      innov_cov=self.innov_cov, H=H,
                                                      reggressand_coeff_indices=coeff_indices,
                                                      rxt_innov_index=np.asscalar(rxt_innov_indices),
                                                      log_prop_form=log_prop_form,
                                                      rxt_coeff_indices=rxt_coeff_indices,
                                                      forecast_dim=forecast_dim)

            returnval = pd.DataFrame(prediction)
            returnval.columns = range(forecast_dim)

        returnval.index = range(H)

        return returnval

    def forecast_VaR_ES(self, percentile=0.05, H=1, simulation_dim=1000):
        """
        Compute value-at-risk and expected shortfall forecasts.

        Parameters
        ---------
        percentile: array-like
        H:  intenger, optional
        simulation_dim: int
            The number of forecasts to compute the results from.
        Returns
        ----
        value-at-risk: dataframe
        expected_shortfall : dataframe

        """
        percentile = np.asscalar(np.ravel(percentile))

        forecasts = self.density_predict(H, simulation_dim).values
        forecasts.sort(axis=1)
        small_forecasts = forecasts[:, :int(simulation_dim * percentile)]
        value_at_risk = pd.DataFrame(small_forecasts.max(axis=1), index=range(1, H + 1), columns=[percentile])
        expected_shortfall = pd.DataFrame(small_forecasts.mean(axis=1), index=range(1, H + 1), columns=[percentile])

        return value_at_risk, expected_shortfall


class HarModel(linear_model.RegressionModel):

    def __init__(self, data, regressand, regressor_vars, hasconst=True, weights=1, exclude=None, *args, **kwargs):

        hasconst_part = "1 + " if hasconst else "0 + "
        regressand = sorted(set(np.ravel(regressand)).union(set(regressor_vars.keys())))

        exog_formula = hasconst_part + " + ".join(["{}.shift({})".format(name, lag) for name, lags in
                                                   sorted(regressor_vars.items()) for lag in lags])
        endog_formula = " + ".join([f"0 + {name}" for name in regressand])
        endog, exog = HarModel._transform_params(endog_formula, exog_formula, data)

        weights = np.ravel(weights)

        if len(weights) == 1:
            weights = np.ones(endog.shape[0])
        else:
            weights = np.ravel(pd.DataFrame(weights, index=data.index).loc[endog.index])

        # self.weights = weights / np.mean(weights)
        self._exog_formula = exog_formula
        self._endog_formula = endog_formula
        self.regressand_names = list(endog.columns)
        self._data_in = data.sort_index(axis=1)
        self.exclude = sorted(list(set(np.ravel(exclude)) - set(self.lags.keys())))
        self.hasconst = hasconst
        super().__init__(endog=endog, exog=exog, weights=weights, hasconst=hasconst, *args, **kwargs)

        if not set(regressor_vars.keys()).issubset(set(self.regressand_names)):
            raise ValueError("The regressors must also be regressands.")

    @property
    def regressor_dim(self):
        return self.exog.shape[1]

    @property
    def regressand_dim(self):
        try:
            return self.endog.shape[1]
        except IndexError:
            return 1

    @property
    def regressor_names(self):
        if self.hasconst:
            name_list = ["intercept"]
        else:
            name_list = []

        for name, values in self.lags.items():
            for val in values:
                name_list.append(f"{name}.shift({val})")

        return name_list

    @property
    def lags(self):
        """Compute the dictionary representation of the lags from the _exog_formula property."""
        lags = convert_formula_to_lag_dict(self._exog_formula)
        return lags

    @property
    def regressors(self):
        """Return the regressors in a matrix format, i.e. X from OLS."""
        regressors = make_regressors(self.lags, self._data_in, self.regressand_names)
        return regressors

    @staticmethod
    def _transform_params(endog_formula, exog_formula, data):
        """Convert the data into regression matrices using the formulas."""
        max_lag = np.amax([int(val) for val in re.findall(r"shift\((\d+)\)", exog_formula)])
        endog = pd.DataFrame(dmatrix(endog_formula, data=data, return_type="dataframe")).iloc[max_lag:]
        if '0' in endog.columns[0]:
            endog.columns = [re.match(r"^([^\.]*)", val).group(0) for val in endog.columns]
        exog = pd.DataFrame(dmatrix(exog_formula, data=data, return_type='dataframe')).iloc[:endog.shape[0]]

        return endog, exog

    def whiten(self, X):
        """
        Whiten the data for the WLS model.

        It multiplies each column by sqrt(self.weights)

        Parameters
        ----------
        X : array-like

        Returns
        -------
        X

        """
        raise NotImplementedError("This function does nothing.")

    def loglike_in(self, params, root_cov):
        """Compute the log-likelihood."""
        params = np.atleast_2d(np.asarray(params).T).T
        root_cov = np.atleast_2d(root_cov)
        time_dim = self.endog.shape[0]
        pred_mean = np.dot(self.exog, params)

        if 'excess_rtn' in self.regressand_names:
            columns = np.argmax('excess_rtn' in self.regressand_names)
            pred_mean[:, columns] = np.exp(pred_mean[:, columns])

            resid = (np.atleast_2d(self.endog.T).T - pred_mean)
            resid[:, columns] = self.weights**.5 * resid[:, columns]
        else:
            resid = (np.atleast_2d(self.endog.T).T - pred_mean)

        try:
            weighted_resid = scilin.solve_triangular(root_cov, resid.T, lower=True, overwrite_b=True,
                                                     check_finite=False).T
            neg_log_det = - (time_dim * np.sum(np.log(np.diag(root_cov))))
            val = - .5 * np.sum(weighted_resid**2) + neg_log_det
        except np.linalg.LinAlgError:
            val = -np.inf

        return val

    def compute_bounded_params(self, results, x0=None, bounds=None):
        """
        Compute the parameters using MLE.

        It forces the variances to be positive, and coefficient for demeaned excess_rtn to be a convex combination
        of the forecasts of the demeaned jumps and diffusion.

        Paramters
        --------
        results
            The regression results.
        x0 : ndarray, optional
            The parameter vector.
        bounds : list of tuples

        Returns
        ------
        ndarray

        """
        num_cov_params = self.regressand_dim * (self.regressand_dim + 1) // 2

        def fun(x):
            return -self.loglike_in(make_param_mat(x, self.regressand_names, lags=self.lags,
                                                   exclude=self.exclude), make_root_cov(x[-num_cov_params:]))

        cov = pd.DataFrame(np.atleast_2d(np.cov(results.resid.T)), columns=self.regressand_names,
                           index=self.regressand_names)
        if x0 is None and bounds is None:
            x0, bounds = create_param_vector(results.params, cov, self.lags, ["excess_rtn"])

        minimize_rslt = minimize(fun=fun, x0=x0, method='L-BFGS-B', bounds=bounds)
        return minimize_rslt.x

    def fit(self, constrained_vol_premium=False, x0=None, bounds=None, *args, **kwargs):

        ols_return = super().fit(*args, **kwargs)
        results = HarResults(self._exog_formula, self._endog_formula, constrained_vol_premium, ols_return)
        # Call the property to construct params appropriately.
        results.params = results.params

        if constrained_vol_premium:
            estimates = self.compute_bounded_params(results, x0=x0, bounds=bounds)
            results.estimates = estimates
            results.params = pd.DataFrame(make_param_mat(results.estimates, self.regressand_names, lags=self.lags,
                                                         exclude=self.exclude), index=results.params.index)

            num_cov_params = self.regressand_dim * (self.regressand_dim + 1) // 2
            root_cov = make_root_cov(results.estimates[-num_cov_params:])
            results.innov_cov = pd.DataFrame(root_cov @ root_cov.T, index=self.regressand_names,
                                             columns=self.regressand_names)
        else:
            results.innov_cov = pd.DataFrame(np.cov(results.wresid.T), index=self.regressand_names,
                                             columns=self.regressand_names)

        return results


def make_regressors(lags, data, regressand_names):
    """Create the regression matrix with all relevant columns if we have mutiple lags of the regressors."""
    regressor_list = []
    name_list = []

    for name, values in lags.items():
        for val in range(1, np.amax(values) + 1):
            regressor_list.append(data[name].shift(val - 1))
            name_list.append(f"{name}.shift({val})")

    for name in set(regressand_names) - set(lags.keys()):
        regressor_list.append(data[name])
        name_list.append(f"{name}.shift({1})")

    regressors = pd.concat(regressor_list, axis=1).dropna()
    regressors.columns = name_list

    regressors = regressors.reindex(columns=sorted(name_list))

    return regressors


def make_root_cov(x):
    """Convert a parameter vector to a root covariance matrix."""
    n = int(np.amax(np.roots([1, 1, -2 * x.size])))

    returnmat = np.zeros((n, n))
    returnmat[np.diag_indices(n)] = x[:n]
    returnmat[np.tril_indices(n, k=-1)] = x[n:]

    return returnmat


def make_param_mat(x, regressand_names, lags, exclude):
    """Convert the x vector into a regression coefficient matrix."""
    x = np.ravel(x)
    constrained = np.ravel('excess_rtn') if 'excess_rtn' in regressand_names else []
    num_lags = np.sum([len(regressor) for regressor in lags.values()])

    regressor_names = lags.keys()
    regressand_dim = len(regressand_names)
    non_constrainedd_names = set(regressand_names) - set(constrained)
    non_constrainedd_dim = len(non_constrainedd_names)

    return_df = pd.DataFrame(np.zeros((num_lags + 1, regressand_dim)), columns=regressand_names)
    return_df.values[0, :] = x[:regressand_dim]
    lag_dim = non_constrainedd_dim * num_lags
    lags = x[regressand_dim:regressand_dim + lag_dim].reshape((num_lags, non_constrainedd_dim), order='C')
    return_df.loc[1:, sorted(non_constrainedd_names)] = lags

    loading_params = x[regressand_dim + lag_dim:regressand_dim + lag_dim + len(regressor_names)]

    if list(constrained) and num_lags > len(regressor_names):
        return_df.loc[1:, sorted(constrained)] = np.atleast_2d(return_df.loc[:, list(regressor_names)].values[1:].dot(
            loading_params)).T
    elif list(constrained):
        return_df.loc[1:, sorted(constrained)] = np.atleast_2d(loading_params.T).T

    return return_df


def create_param_vector(params, cov, lags, exclude):
    """
    Convert a regression coefficient and innovation coefficient into a parameter vector.

    I use it to initiliaze the optimization of the mle.

    Paramters
    ---------
    params : dataframe
    cov : ndarray
    lags
    exclude : iterable

    Returns
    -----------
    vec : ndarray
    bounds : list of tuples

    """
    exclude = list(np.ravel(exclude))
    params.index = range(0, params.shape[0])
    regressand_names = params.columns
    have_excess_rtn = 'excess_rtn' in regressand_names
    non_excluded_names = sorted(set(regressand_names) - set(exclude))
    regressor_names = list(lags.keys())

    exclude_dim = len(exclude)
    x_idx, y_idx = np.tril_indices(cov.shape[0], k=-1)
    L = np.linalg.cholesky(cov)

    identity_vec = params.iloc[0]
    if have_excess_rtn:
        excess_rtn_intercept = identity_vec.loc[exclude]
        identity_vec.loc[exclude] = (np.log(excess_rtn_intercept)
                                     if np.all(excess_rtn_intercept > 0) else np.log(0.041))

    identity_vec = np.ravel(identity_vec)
    lag_params = np.ravel(params.loc[1:, sorted(non_excluded_names)], order='C')

    if have_excess_rtn and params.shape[0] > params.shape[1]:
        A = params.loc[:, sorted(regressor_names)].iloc[1:len(regressor_names) + exclude_dim]
        b = params.loc[:, sorted(exclude)].iloc[1:len(regressor_names) + exclude_dim]
        loading_params = np.ravel(np.linalg.solve(A, b))
    elif have_excess_rtn:
        loading_params = np.ravel(params.loc[:, 'excess_rtn'].iloc[1:])
    else:
        loading_params = np.ravel([])

    # if loading_params.size > 0:
    #     if 'quad' in regressor_names and loading_params[-1] < 0:
    #         loading_params.fill(0)
    #     elif 'diffusion' in regressor_names and loading_params[0] < 0:
    #         loading_params.fill(0)

    var_params = np.diag(L)
    covariance_params = np.ravel(L[x_idx, y_idx])

    vec = np.concatenate([identity_vec, lag_params, loading_params, var_params, covariance_params])

    bounds = ([(None, None)] * len(identity_vec) + [(-5, 5)] * len(lag_params) +
              [(-5, 5)] * (len(loading_params)) + [(1e-7, 100)] * (len(var_params)) + [(-100, 100)] *
              len(covariance_params))

    return vec, bounds


def convert_lag_dict_to_coeff(lags, regressand_names, params):
    """Convert a dictionary of regressors and appropriate lags to a regression coefficient."""
    set_difference = set(regressand_names) - set(lags.keys())
    for name in set_difference:
        lags.update({name: [1]})
    data_dim = int(np.sum(np.amax(val) for val in lags.values()))

    lags = OrderedDict(sorted(lags.items()))
    coeff = np.eye(data_dim, k=-1)

    col_indices = []
    prev_regressor_dim = 0
    for name, values in lags.items():
        if name not in set_difference:
            col_indices.append(np.ravel([np.asarray(values) - 1 + prev_regressor_dim]))
            prev_regressor_dim += np.amax(values)
        else:
            prev_regressor_dim += 1
    col_indices = np.concatenate(col_indices)

    prev_regressor_dim = 0
    row_indices = []
    name_list = []
    for name, values in lags.items():
        for val in range(1, np.amax(values) + 1):
            name_list.append(f"{name}.shift({val})")
        row_indices.append([prev_regressor_dim])
        coeff[prev_regressor_dim] = 0
        coeff[prev_regressor_dim, col_indices] = np.squeeze(params[name].iloc[1:])
        prev_regressor_dim += np.amax(values)

    row_indices = np.concatenate(row_indices)
    coeff_df = pd.DataFrame(coeff, index=name_list, columns=name_list)
    coeff_df_index = np.ravel(coeff_df.index)
    coeff_df_index[row_indices] = sorted(regressand_names)
    coeff_df.index = coeff_df_index

    return coeff_df


def convert_formula_to_lag_dict(formula):
    """Convert a patsy formula to a dictionary of regressor names and appropriate lags."""
    groups = re.findall('([a-z,_]+)\.shift\((\d+)\)', formula)
    results = defaultdict(list)
    for key, num in groups:
        results[key].extend([int(num)])
    return OrderedDict(sorted(results.items()))


def make_default_model_less(default_model):
    def comparer(param1, param2):
        arg1 = param1[3]
        arg2 = param2[3]
        if arg1 == arg2:
            return 0
        elif arg1 == default_model:
            return -1
        elif arg2 == default_model:
            return 1
        elif arg1 < arg2:
            return -1
        else:
            return 1
    return cmp_to_key(comparer)


def compute_regressor_names(lags):
    regressors = dict(lags)

    names = []
    for name, values in sorted(regressors.items()):
        if len(values) > 1:
            names.append("HAR-" + compute_names_in(name))
        else:
            names.append("AR(1)-" + compute_names_in(name))

        if len(names) == 2:
            val1, val2 = names
            if "AR(1)" in val1:
                names = [val2, val1.replace("AR(1)-", "")]
            elif "AR(1)" in val2:
                names = [val1, val2.replace("AR(1)-", "")]

    return "--".join(names)


def dataset_name(arg):
    string_rep = str(arg)
    if string_rep == 'log_vol':
        return "Log Volatility"
    elif string_rep == 'untransformed_vol':
        return "Untransformed Volatility"
    elif string_rep == 'root_vol':
        return "Root Volatility"
    else:
        raise ValueError(f'The string_rep is {string_rep}')


def compute_names_in(name):
    string_rep = str(name)

    if string_rep == "real_vol":
        return r"\realvolt"
    elif string_rep == "diffusion":
        return r"\diffvolt"
    elif string_rep == "jumps":
        return r"\jumpvolt"
    elif string_rep == "quad":
        return r"\totalvolt"
    elif string_rep == "bipower_diff":
        return r"\bipowerdiffvolt"
    elif string_rep == "bipower_jumps":
        return r"\bipowerjumpvolt"
    elif string_rep == "log_prop":
        return r"$\log(\jumppropt)$"
    elif string_rep == "excess_rtn":
        return r"\rxt"
    else:
        return string_rep


def regressand_name(arg):
    reg_list = [arg] if isinstance(arg, str) else list(arg)

    if len(reg_list) == 1:
        return_str = compute_names_in(reg_list[0])
    elif reg_list[-1] == 'constrained':
        return_str = r"\lbrack " + ", ".join(map(compute_names_in, reg_list[:-1])) + r" \rbrack" + "-constrained"
    else:
        return_str = r"\lbrack " + ", ".join(map(compute_names_in, reg_list)) + r" \rbrack"

    return return_str


def estimate(x, exclude, max_horizon=30, constrained_vol_premium=False, x0=None, bounds=None):
    """
    Estimate the model and return them and a string that tells you the values used to estimate the mode.

    Paramters
    -------
    x : tuple
        The paramters needed to estimate the model.
    max_horizon: int
        The maximum forecast horizon.
    constrained_vol_premimum: bool, optional
        Whether to force the expected excess return to be positive.
    exclude : array-like, optional
        The regressand values to remove from the regressors.
        #TODO is this needed?

    Returns
    -----
    string
    HarResults

    """
    vol_name, vol_data, regressors, regressand = x
    exclude = np.ravel(exclude)

    model = HarModel(vol_data, regressand=regressand, regressor_vars=regressors, weights=vol_data.weights,
                     exclude=exclude).fit(constrained_vol_premium=constrained_vol_premium, x0=x0, bounds=bounds)

    regressand_names = [regressand] if isinstance(regressand, str) else list(regressand)

    if constrained_vol_premium:
        regressand_names += ['constrained']

    key = "/".join([dataset_name(vol_name), compute_regressor_names(regressors),
                    regressand_name(tuple(regressand)), vol_data.index[-1].strftime('%Y-%m-%d')])

    return key, model


def nested_dict_merge_in(a, b, path=None):
    """
    Merge b into a.

    This was adapted from stackoverflow.com/questions/7204805

    Parameters
    ---------
    a : nested dictlike
    b : nested dictlike
    path : list

    Returns
    -------
    nested dict

    """
    if path is None:
        path = []
    for key in b:
        if key in a:
            if isinstance(a[key], dict) and isinstance(b[key], dict):
                nested_dict_merge_in(a[key], b[key], path + [str(key)])
            elif (isinstance(a[key], pd.DataFrame) or isinstance(a[key], np.asarray) and
                  np.all(np.asarray(a[key]) == np.asarray(b[key])) and np.all(a[key].index == b[key].index)):
                pass  # same leaf value
            elif a[key] == b[key]:
                pass  # same leaf value
            else:
                raise Exception('Conflict at %s' % '.'.join(path + [str(key)]))
        else:
            a[key] = b[key]
    return a


def nested_dict_merge(iterable):
    """Merge an iterable of dictioaries."""
    return reduce(nested_dict_merge_in, iterable)


def extract_results(estimate, loss_func, **kwargs):
    """
    Extract the results from the estimates from and evaluate the loss function element by element.

    The results should be put together as done by the estimate function above.

    Parameters
    ---------
    estimate: 2-element tuple
    loss_func : function
    **kwargs : passed to loss_func

    Returns
    ------
    tree of dataframes

    """
    results_in = tree()
    key, model = estimate

    vol_name, regressors, regressand, date = key.split('/')
    resid = loss_func(date=date, model=model, **kwargs)
    for h, column in resid.items():
        results_in[vol_name][regressand][regressors][h][date] = np.asscalar(column)

    return results_in


def reorganize_results(iterable):
    """Reorganize the results as computed using the extract_results function into a nested dictionary."""
    iterable = nested_dict_merge(iterable)
    results_out = tree()
    for vol_name, vol_data in iterable.items():
        for regressand, regressand_data in vol_data.items():
            for regressor, regressor_data in regressand_data.items():
                for h, horizon_data in regressor_data.items():
                    data = pd.DataFrame.from_records([(key, val) for key, val in
                                                      horizon_data.items()]).set_index(0)
                    data.index = pd.to_datetime(data.index)
                    data.columns = [h]
                    results_out[vol_name][regressand][regressor][h] = data

    return results_out
