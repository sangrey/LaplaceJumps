\documentclass[11pt, letterpaper, twoside]{article}
\usepackage{paper_preamble}
\usepackage{multirow}
\addbibresource{density_estimation.bib}

\title{Jumps, Realized Densities, and News Premia: Response to Hengie Ai's comments} 
\author{Paul Sangrey} 


\begin{document}

\maketitle    
\thispagestyle{empty}


First, I want to thank Hengie Ai for his excellent comments. 
I am very grateful for both the time spent and the level of insight provided. 
There are three main issues raised in the discussion. 
I will address each of them in turn.


\section{Theory portion}

To fully characterize news risk, I lay out the following consumer problem.
I first lay out a series of assumptions on the market environment.

\begin{assumption}{Market Environment}
    \label{ass:market_assumptions}
    \begin{enumerate}
        \item Both $u$ and $\mathcal{I}$ are Lipschitz continuous, \Frechet\ differentiable with Lipschitz
            derivatives. 
%
        \item $u: \R \to \R$ has strictly positive  first-order derivatives, and $\mathcal{I}$ is increasing with
            respect to first-order stochastic dominance.
%
        \item Consumption --- $C(t)$ --- is an \Ito\ semimartingale.
%
        \item A representative investor prices all assets.
    \end{enumerate}
\end{assumption}

This differs in one key way from the exposition in \textcite{ai2018risk} --- I do not assume that the underlying
state variables driving the economy are continuous. 
In addition, I let consumption be chosen endogenously. 
If the investor wants consumption to jump, it will jump.
\Citeauthor{ai2018risk} derive continuity of $C(t)$ by assuming that the predictable component of consumption is
continuous, and they also only considered a finite number of pre-scheduled announcements. 

Since I am working in a more general setup.
I do not require the underlying state variables (technology processes) driving consumption to be continuous and I
neither require require the news releases to be pre-scheduled nor to be finite-activity. 
It is not clear there exist general assumptions that imply the investor will endogenously choose $C(t)$ to be
continuous in this environment.

In the previous discussion I viewed requiring $C(t)$ to be continuous as a constraint on the problem.
By doing this, you can get the intuition for the jump case to go through.
In particular, future consumption is no longer a sufficient statistic for future value in general:
%
\begin{equation}
    \lim_{t \to^{+} \tau} V(t) \neq V\left(\lim_{ t \to^{+} \tau} C(t)\right).
\end{equation}

However, this is somewhat confusing and since my derivations do not require consumption to be continuous, I have
dropped that assumption and now tackle the case with jumps in consumption directly.
(See the appendix for further discussion.)

The investor solves the following optimal portfolio allocations problem.

\begin{problem}{Consumer's Portfolio Allocation}

    \label{defn:consumper_problem}
    \begin{gather}
        V\left(\Xi(t-\Delta), P(t)\right)  = \max_{C(t),\, \Xi(t)} \int_{t}^{t+\Delta} u(c(s)) \dif s  
         + \mathcal{I}\left[\exp(-\kappa\Delta) V\left(\Xi(t), P(t+\Delta)\right) \mvert
          \F_{t}\right] \\
%
        C(t) + \sum_{i} P_i(t) \xi_{i}(t-\Delta) = \sum_{i} P_i(t) \xi_{i}(t)
    \end{gather}
\end{problem}
%
Once I have set up that problem, I proved the following result, where $m(t)$ is the log SDF and $m_{*}(t)$ is the
log announcement SDF arising from curvature of the certainty equivalence functional.

\begin{restatable}[Asset-Pricing Equation]{theorem}{riskPremia}
    \label{thm:riskPremia}
    Let  the assumptions in \cref{ass:market_assumptions} hold, prices be an \Ito\ semimartingales, and the
    representative consumer face \cref{defn:consumper_problem} as $\Delta \to 0$.
    Assume that preferences are such that optimal consumption is strictly positive.
    Then risk-premia for some asset $i$ are 
%
    \begin{equation}
        \label{eqn:riskPremia}
        \E\left[\frac{\dif P_{i}(t)}{P_{i}(t-)} - \frac{\dif P_{rf}(t)}{P_{rf}(t-)} \mvert \F_{t-}\right]  = -
        \dif \predQV{m, \diff{p} + \jump{p}}(t) - \dif \predQV{m_{*},\jump{p}}(t).  
    \end{equation}
\end{restatable}


\section{Interpreting the Regression}

The question becomes how do we take \cref{eqn:riskPremia} to the data. 
In general, we want to regress expected excess returns on volatilities.
There are some econometric considerations, but I will ignore them for now.

The first question is what is $\predQV*{m, \diff{p} + \jump{p}}$?
There are two leading cases.
First, risk prices are static (equivalently we assume wealth is the only factor). 
In particular, we assume that $m(t)$ is proportional to market wealth.
Then $\predQV*{m, \diff{p} + \jump{p}} = \beta_1(\totalvolt)$.
If we make the same assumption for $m_{*}(t)$, then $\predQV{m_{*}(t), \jump{p}} = \beta_2 \jumpvolt$.


In the second case, we assume that consumption is endogenously continuous, and hence $m(t)$ is a continuous
function.
Then $\predQV*{m(t), \jump{p}} = 0$ because jump and diffusion processes are orthogonal.
Consequently, $\predQV*{m(t), \diff{p} + \jump{p}} = \beta_1 \diffvolt$.
We still have that $\predQV*{m_{*}(t), \jump{p}} = \beta_2 \jumpvolt$.

Since the volatility dynamics are almost linear in logs, but not in levels, I approximate the regressions above in
the logs.
If you run the regressions in levels, the results are the same or insignificant. 

\begin{table}[htb]
    \centering
    \caption{News Premia Estimates}
    \label{tbl:jump_premia_estimates}

    \sisetup{
        table-align-text-pre=false,
        table-align-text-post=false,
        round-mode=places,
        round-precision=2,
        table-format=2.2,
        table-space-text-pre=\lbrack,
        table-space-text-post=\rbrack,
    }

    \begin{tabularx}{.75\textwidth}{X | *{6}{S}}
        \toprule
        \multicolumn{5}{c}{Regressors} \\
        \midrule
%
        \multirow{2}{*}{Intercept}      
        & 2.9514    &  -2.4500  &  -5.035   & 3.2769    & 2.9543    & 2.3025    \\ 
        & [6.6129]  & [-5.1232] & [-0.5821] & [7.2217]  & [6.0712]  & [4.1060]  \\
%
        \rowcolor{row_background}
        & 0.2424    &           &  0.1366   &           &           &           \\
        \rowcolor{row_background} \multirow{-2}{*}{$\log\left(\totalvolt\right)$}
        & [5.8831]  &           & [2.6811]  &           &           &           \\
%
        \multirow{2}{*}{$\log\left(\jumppropt\right)$} 
        &           & -5.0088   & -4.1512   &           &           &           \\
        &           & [-5.8559] & [-4.9267] &           &           &           \\
          
        \rowcolor{row_background}
        &           &           &           & 0.2510    &           & 1.8975    \\
        \rowcolor{row_background} \multirow{-2}{*}{$\log\left(\diffvolt\right)$}  
        &           &           &           & [6.5081]  &           & [5.2153]  \\
%
        \multirow{2}{*}{$\log\left(\jumpvolt\right)$} 
        &           &           &           &           & 0.2309    & -1.7826   \\
        &           &           &           &           & [5.3987]  & [-4.5717] \\
%
        \bottomrule
    \end{tabularx}
\end{table}

The question becomes how to interpret these coefficients.\footnote{These estimates differ slightly from previous
    estimates because I am using different tuning parameters in estimate the volatilities from the high-frequency
data.}
If we assume that wealth is the only factor, the first three regressions are correctly specified.
If we assume that $m(t)$ is a continuous function, then the correctly-specified regressions are the last three.
In both cases, we have that $\beta_1 > 0$ and $\beta_2 < 0$ in \cref{tbl:jump_premia_estimates}.

This implies that we find late resolution of uncertainty if the representative investor has Epstein-Zin
preferences. 
To see this, consider the following derivation of $m(t)$ and $m_{*}(t)$ in the case of Epstein-Zin utility. 

I adopt the notation \textcite{bansal2004risks} except I let $\rho$ refer risk aversion and $\psi$ refer to the
intertemporal elasticity of substitution (IES). 
You can represent Epstein-Zin preferences over an length of time $\Delta$ as follows for a value function $U$ as
follows.

\begin{definition}[Epstein-Zin Utility]
    \begin{equation}
        \label{eqn:epstien_zin}
        U_t = \left[c_t^{1 - 1 / \psi} + \exp(-\kappa \Delta) \E\left[U_{t+ \Delta}^{1-\rho}\mvert \F_{t}
        \right]^{\frac{1 - 1 / \psi}{1 - \rho}}\right]^{\frac{1}{1 - 1 / \psi}}  
  \end{equation}
\end{definition}

This formulation of Epstein-Zin utility is not in a form useful for my purposes. 
Define $V_t \coloneqq U_{t}^{1 - 1 / \psi}$.
Then we can reparameterize \cref{eqn:epstien_zin} as 
%
\begin{equation}
    V_t = \left[c_t^{1 - 1/\psi} + \exp(-\kappa \Delta) \E\left[V_{t + \Delta}^{\frac{1 - \rho}{1-1 / \psi}}
    \mvert \F_{t} \right]^{\frac{1 - 1 / \psi}{1 - \rho}}\right].
\end{equation}
%
Define $\phi(V) \coloneqq \frac{1- \rho}{1 - 1 / \psi} V^{\frac{1-\rho}{1 - 1 / \psi}}$, then 
%
\begin{equation}
    V_t = \left[c_t^{1 - 1/\psi} + \exp(-\kappa \Delta) \phi^{-1} \left(\E\left[\phi\left(V_{t + \Delta}\right)
    \mvert \F_{t} \right]\right)\right].
\end{equation}


The log stochastic discount factor is the log of the derivative of the first term.
In other words, we have $m(t) \propto -\frac{1}{\psi} \log(c_t)$, this is negative and so consumption and
$m(t)$ co-move negatively. 
When you plug this into \cref{eqn:riskPremia} this gives the standard result where risk-aversion causes market
risk to command a positive premium.


Conversely, I find that $\jumppropt$ commands a negative premium. 
Again plugging this into $\cref{eqn:riskPremia}$ this implies that $m_{*}(t)$ and consumption positively co-move. 
Analogously to how $m(t)$ and the utility function are related, $m_{*}(t)$ is proportion to the log derivative of
$\phi$. 
Since the risk-aversion parameter $\rho > 1$, we have that $m_{*}(t)$ co-moves positively with consumption if
$\psi > 1$ and $1 / \psi < \rho$.
In this case, we have late resolution of uncertainty.
We could also have $\psi < 1$ and $\rho > 1 / \psi$.
Per \textcite[1486]{bansal2004risks}, in this case, the wealth effect dominates the consumption affect and so
higher expected growth causes the wealth-to-consumption ratio to fall.

There is one significant issue with the analysis in the previous paragraphs.
Both specifications I considered had static risk prices.
Since we live in a world where risk prices co-move with the business cycle, the particular analysis to the
Epstein-Zin case is incomplete.  
If risk-prices move, I am estimating the average risk prices in the regressions above.
Equivalently, I am estimating the local average co-movement between $m_{*}(t)$ and the market return between 2003
and 2017.

This implies that although the nonparametrically identified coefficients that I find challenge standard
calibrations in the literature, they do not imply that they are necessarily wrong. 
A very interesting avenue for future research is how to reconcile this puzzling new stylized fact with the
literature by building a fully specified finance model that tells exactly what the underlying pricing factors are
hence how the risk prices co-moved over the past several years. 


\section{Empirical Work on FOMC Premium}

I replicate \gentextcite{ai2018risk}  results on the slightly different sample.
In particular, the average FOMC premium  is \num{0.88} log percentage points on an annualized basis with
associated $t$-static of $[2.94]$.
However, this premium is not arising primarily in the immediate minutes preceding the FOMC
announcement, but is rather spread throughout the hours preceding the announcement.
This can be clearly seen in \cref{fig:hourly_fomc_returns}, where I report the average hourly return on FOMC days.
I shift the return in each day so that the FOMC makes its announcement at hour \num{0}.

\begin{figure}[htb]
    \caption{Hourly FOMC Returns}
    \label{fig:hourly_fomc_returns}
    \includegraphics[width=\textwidth, height=.3\textheight]{fomc_premium_present.pdf} 
\end{figure}

In fact, the most notable positive returns are actually at the market opening, especially on days when the FOMC
makes its announcements later than usual.
In addition, in a recent working paper \textcite{neuhierl2018monetary} demonstrate that markets start drifting
\num{25} days before expansionary monetary policy surprises.

It is likely that the FOMC announcement is not the only risk that investors face on FOMC days and highly likely
it is not the only over the \num{25} day period discussed there.
Consequently, to fully understand the news risk we must control for the other risks in some way.
This is why I prefer the regressions where I control for diffusive risk as reported in the paper.

Beyond that, it would be useful to test directly for whether FOMC days are a good proxy for $\jumppropt$.
One way to view a regression on a proxy is as an instrumental variables regression where the econometrician does
not observe the true regressor, and so projects the regressand directly onto the instruments.
Although, this will not identify the magnitude  of the relationship between outcome variable and the regressor, as
long as all of the variables are univariate, it will identify the sign.

To make this concrete, \textcite{ai2018risk} treated FOMC dates as a proxy of (implicitly) $\jumppropt$.
Since they did not have a consistent estimator for $\jumppropt$, they regressed $\rxt$ directly on $\FOMC_t$.
However, since I provide a consistent estimator for $\jumppropt$, we can test for the strength of this
relationship. 
This is useful because weak instruments weak often invalidate inference in these types of regressions.

To test for this, I regress $\jumppropt$ onto $\FOMC_t$.
I only consider the jump proportion regressions to isolate the effect of the jumps. 
If I were to also include $\diffvolt$, I would need to instrument it as well, and so my analysis would be
contaminated by the effect of the additional instruments.

\begin{table}[htb]
    \centering
    \caption{FOMC Dates First stage Regression}
    \label{tbl:fomc_first_stage}

    \sisetup{
        table-align-text-pre=false,
        table-align-text-post=false,
        round-mode=places,
        round-precision=2,
        table-format=3.3,
        table-space-text-pre=\lbrack,
        table-space-text-post=\rbrack,
    }

    \begin{tabularx}{.5\textwidth}{X | *{2}{S}}
        \toprule
        Regressand      & {$\log\left(\jumppropt\right)$}  & {$\jumppropt$}    \\
        \midrule
%
        \multirow{2}{*}{Intercept}  & -0.5967               & 0.5634            \\
                                    & [-61.442]             & [116.41]          \\
%
        \rowcolor{row_background}
                                    & 0.525                 & 0.0316            \\
        \rowcolor{row_background}
        \multirow{-2}{*}{$\FOMC_t$} & [2.3965]              & [2.7113]          \\
%
        $\bar{\R}^2$                & \SI{.17}{\percent}    & \SI{.23}{\percent}\\
%
        \rowcolor{row_background}
        $F$-Statistic                 & 5.7432                & 7.3410            \\
%
        \bottomrule
    \end{tabularx}

\end{table}

As we can see in \cref{tbl:fomc_first_stage}, although $\jumppropt$ and $\FOMC_t$ have the predicted relationship
and this relationship is statistically significant, $\FOMC_t$ explains less than \SI{1}{\percent} of the variation
in the jump proportion.
Since we only have \num{115} FOMC days, the implied $F$-statistic is within the region where weak
instruments often play a role, $\widehat{F} < 10$, .
In addition risk premia are known to be hard to estimate, and returns are highly heteroskedastic we should be
quite worried about weak instruments in this environment.
Since I provide a consistent estimator for $\jumppropt$, instead of developing the identification robust inference
procedures, we can use $\jumppropt$ directly and instrument for its predictable variation using its lags.
They are not weak instruments, and so the standard inference procedures are valid.


\begin{center}
    \scshape \large Appendices  
\end{center}

\appendix

\section{Continuous Consumption and Discontinuous Information}

In this section, I provide an example where consumption is continuous, preferences are time-separable, and jump
risk pis priced.
Let the investor face the following problem as $\Delta \to 0$.
I impose the constraint that $C(t+\Delta)$ is chosen at time $t$.
Intuitively, I require that you choose tomorrow's consumption today.  
This is equivalent to requiring consumption be continuous as $\Delta \to 0$ because the continuous processes
generate the predictable filtration, and so continuous and predictable processes can differ on at most a set of
measure zero.

I let the investor choose between a risky stock $x(t)$ and a risk-free bond $b(t)$.
To simplify the argument I assume that $b(t)$ has a price of $1$, i.e.\@ the risk-free rate equals zero.
I adopt the convention where time arguments refer to the time-period 
Further assume we have a finite number of time-periods --- T.

\begin{problem}{Continuous Consumption as a Constraint}

    \begin{gather}
        V(C(-\Delta), b(t-\Delta), x(t)) = \max_{C(t), b(t), x(t)} \log(C(t-\Delta)) + 
        \E\left[V(b(t), C(t), x(t+\Delta)) \mvert \F_{t}\right] \\
%
         C(t-\Delta) + b(t) + x(t) = b(t-\Delta) + R_x(t) x(t-\Delta) 
    \end{gather}
\end{problem}


We can substitute the constraint into  the objective function and differentiate.
Since $C(t-\Delta)$ is pre-determined, we cannot differentiate with respect to it, and so we substitute the
constraint inside of $V(t)$ instead. 
%
Then the following first-order condition with respect to $C(t-\Delta)$ holds
%
\begin{equation}
    \E\left[\frac{1}{C(t-\Delta)} - \frac{1}{b(t) +  R_x(t+\Delta)\left(x_t - C(t-\Delta)\right)} \mvert
    \F_t\right] = 0
\end{equation}

The clear difference between this first-order condition and the standard first-order condition is that it only
holds in expectation. 
Consequently, any variable contained within $\F_t$ is relevant for asset pricing.
We cannot use consumption as the single factor, even though only consumption is valued.
This implies that the derivative of value function might jump even though consumption does not.
This all continues to hold as $\Delta \to 0$.
The way that I included $\Delta$ implies that this is equivalent to taking limits from the right.
Consequently, $M(t)$ is not necessarily continuous, and so jump risk can be priced even though we have
time-operable preferences.


\end{document}


