

\section{Simulation Results (For online publication only)}\label{app:simulations}

In this section, I report several other simulation results. They are similar to those reported in the paper, i.e., paper's results are quite robust.

\subsection{Market Microstructure}\label{app:market_microstructore}

The data have substantial market microstructure noise. To mimic its effect, I follow \textcite{christensen2014fact}. They assume we observe $r_{i_n} + u_{i_n}$, here $u_{i_n}$ follows
%
\begin{equation}
  \label{eqn:microstructure_noise_simulation}
  u_{i_n} = \beta u_{i_n - 1} + \epsilon_{i_n},\quad \epsilon_{i_n} \stackrel{i.i.d.}{\sim} \N(0, \omega^2 (1 - \beta^2)).
\end{equation}
%
I set $\omega^2 = \num{1.0e-10}$ because that is the value obtained from the data using the jump robust noise variance bipower-type estimator of \textcite{oomen2006comment}: 
%
\begin{equation}
    \frac{1}{T} \sum_{t=1}^{T-1} \frac{\Delta^n}{2} \sum_{t-1 < i_n, i_n-1 < t} \abs*{\Delta^n_{i_n} p}\abs*{\Delta^n_{i_n -1} p}. 
\end{equation}
%
I set $\beta = \num{0.77}$, which is the value used in \textcite{christensen2014fact}. They set it to match the sign of the S\&P 500 futures contract on the day of the 2010 Flash Crash.

I now add the market-microstructure correction described in \cref{sec:estimation_framework}. I also set $\theta=0.5$ (the constant for the pre-averaging correction) and $\bar{\kappa} = 1000$ (the constant for the instantaneous estimator), which are the values used in the actual estimation. I chose these values because they appeared to work well in the simulated data. As we can see in \cref{fig:simulation_results_micro}, the estimators are slightly biased upwards in this scenario, especially the estimators for $\diffvolt$. 

\begin{figure}[!ht]
  \caption{Discrete-Time Simulation Results with Microstructure}
  \label{fig:simulation_results_micro}
  \centering

  \begin{subfigure}[t]{.55\textwidth}
    \caption[Root Diffusion Volatility]{$\protect\diffrootvol{_t}$}
    \label{fig:diffusion_simulation}
    \includegraphics[width=\textwidth, height=.22\textheight]{daily_diffusion_comp_24k_with_Li_micro.pdf} 
  \end{subfigure}%
%
  \hfill
%
  \begin{subfigure}[t]{.45\textwidth}
    \caption[Root Jump Volatility]{$\protect\jumprootvol{_t}$}
    \label{fig:jump_simulation}
    \includegraphics[width=\textwidth, height=.22\textheight]{daily_jump_comp_24k_with_Li_micro.pdf} 
  \end{subfigure}%
\end{figure}

Even though they are slightly biased upwards, the proposed estimators perform reasonably well in practice. This claim does not hold for the other estimators in the literature. In \cref{tbl:simulate_results_micro}, I report the mean-square error of the previous estimates averaged over a year's worth of simulations. Here I have approximately \num{1/2} the average error in estimating $\diffvolt$ and \num{1/5} the error in estimating $\jumpvolt$. Although the jump variation estimators in the literature are not consistent for $\jumpvolt$, they should be asymptotically unbiased. In large finite-samples, they appear both biased and inconsistent.
 
\begin{table}[htb]
  \centering
  \caption{Relative Simulation Error with Microstructure}
  \label{tbl:simulate_results_micro}
  \setlength{\tabcolsep}{3pt}

  \begin{tabularx}{\textwidth}{X | *{4}{S} | *{4}{S}}
    \toprule
%
    Obs.\@ per Min.\@ &
    \multicolumn{4}{c}{$\frac{\E[\left(\widehat{\diffrootvol{_t}} -
    \diffrootvol{_t}\right)^2]}{\E[\diffrootvol{_t}]}$} &
%
    \multicolumn{4}{c}{$\frac{\E[\left(\widehat{\jumprootvol{_t}} - %
    \jumprootvol{_t}\right)^2]}{\E[\jumprootvol{_t}]}$} \\
    \midrule
    & {BNS} & {LTT} & {5 Min.} & {Proposed} & {BNS} & {LTT} & {5 Min.} & {Proposed} \\ 
    \midrule
    $\approx 2$ & 0.7390 & {\num{0.4079}} & 0.4233 & 1.0016 & 1.0086 & 1.0086 & 0.8333 & {\num{0.6542}} \\
%
    \rowcolor{row_background} 
    $\approx 12$ & 0.8191 & 0.4584 & 0.4596 & {\num{0.3600}} & 1.0074 & 1.0074 & 0.8171 & {\num{0.4064}} \\
%
    $\approx 60$ & 1.1138 & 0.6882 & 0.6850 & {\num{0.3617}} & 1.0079 & 1.009 & 0.8408 & {\num{0.2070}} \\
%
    \rowcolor{row_background} 
    $\approx 180$ & 1.5824 & 1.0599 & 1.0599 & {\num{0.8499}} & 1.0090 & 1.090 & 0.8058 & {\num{0.1803}} \\
    \bottomrule
  \end{tabularx}
\end{table}



\subsection{Simulation Error with only a few Jumps}

I simulate the volatilities using the DGP described in \cref{tbl:simulation_vol_params}. Then instead of simulating the prices using \cref{sec:simulation_dgp}, I follow \textcite{huang2005relative} and assume the jump locations follow a time-invariant Poisson distribution and the magnitudes are Gaussian distributed. I set the Poisson's intensity to result in an average of one jump per day. I set the variance of the magnitude so that the jump process has the volatility given by $\jumpvolt$. This DGP should be quite difficult for my procedure because there are very few jumps. It drastically violates the infinite-activity assumption. I also add the microstructure noise as described in \cref{eqn:microstructure_noise_simulation}. 

\begin{table}[htb]
  \centering

  \caption{Relative Simulation Error with Microstructure and Poisson Jumps}

  \label{tbl:simulate_results_poisson_jumps}

  \begin{tabularx}{\textwidth}{X | *{4}{S} | *{4}{S}}
    \toprule
  
    Obs.\@ per Min.\@ & \multicolumn{4}{c}{$\frac{\E[\left(\widehat{\diffrootvol{_t}} - \diffrootvol{_t}\right)^2]}{\E[\diffrootvol{_t}]}$} &
%
    \multicolumn{4}{c}{$\frac{\E[\left(\widehat{\jumprootvol{_t}} - \jumprootvol{_t}\right)^2]}{\E[\jumprootvol{_t}]}$} \\
%
    \midrule
    & {BNS} & {LTT} & {5 Min.} & {Proposed} & {BNS} & {LTT} & {5 Min.} & {Proposed} \\ 
%
    \midrule
    $\approx 2$ & 0.8828 & {\num{0.1186}} & 0.2014 & 0.8817 & 1.0073 & 1.0073 & 0.7788 & {\num{0.3434}} \\
%
    \rowcolor{row_background} 
    $\approx 12$ & 0.9497 & {\num{0.1256}} & 0.2108 & 0.5064 & 1.0089 & 1.0089 & 0.7937 & {\num{0.3214}} \\
%
    $\approx 60$ & 1.1671 & 0.3189 & 0.4109 & {\num{0.0927}} & 1.0074 & 1.0074 & 0.8028 & {\num{0.3914}} \\
%
    \rowcolor{row_background} 
    $\approx 180$ & 1.5499 & 0.7717 & 0.8500 & {\num{0.5848}} & 1.0075 & 1.0075 & 0.7461 & {\num{0.3620}} \\
    \bottomrule
  \end{tabularx}
\end{table}



\section{Volatility: Empirical Results (For online publication only)}\label{app:vol_empirics}

\phantomsection
\addcontentsline{toc}{subsection}{Univariate Autoregression Models}

\begin{table}[htb]

    \caption{Univariate Autoregressive Models: AR(BIC)}
    \label{tbl:ar_models}

    \sisetup{
        detect-mode,
        tight-spacing           = true,
        group-digits            = false,
        input-symbols           = {(}{)},
        input-open-uncertainty  = ,
        input-close-uncertainty = ,
        round-mode              = places,
        round-precision         = 2,
        table-align-text-pre    = false,
        table-align-text-post   = false,
        table-alignment         = center,
    }

    \centering
    \begin{tabularx}{.9\textwidth}{Z | S >{{\lparen}} S[table-space-text-pre={\lparen}, table-space-text-post=, table-number-alignment=right] <{{,}} S[table-space-text-pre={\hspace{-2\tabcolsep}}, table-space-text-post={)}] <{{\rparen}} | S >{{\lparen}} S[table-space-text-pre={\lparen}, table-space-text-post=, table-number-alignment=right] <{{,}} S[table-space-text-pre={\hspace{-2\tabcolsep}}, table-space-text-post={)}] <{{\rparen}}}
%
        \toprule 
        & \multicolumn{3}{c}{$\log(\diffvolt)$} & \multicolumn{3}{c}{$\log(\jumpvolt)$} \\
        \midrule
        Intercept                         & -0.6783 & -0.8758 & -0.4809 & -0.6166 & -0.8130 & -0.4202 \\
        \rowcolor{row_background} Lag 1   &  0.5445 &  0.5123 &  0.5767 &  0.4627 &  0.4306 & 0.4948 \\ 
        Lag 2                             &  0.1474 &  0.1108 &  0.1841 &  0.1698 &  0.1343 & 0.2052 \\
        \rowcolor{row_background} Lag 3   &  0.0551 &  0.0181 &  0.0920 &  0.0534 &  0.0176 & 0.0892 \\
        Lag 4                             &  0.0735 &  0.0365 &  0.1104 &  0.0753 &  0.0395 & 0.1112 \\
        \rowcolor{row_background} Lag 5   &  0.0409 &  0.0039 &  0.0779 &  0.0903 &  0.0545 & 0.1262 \\
        Lag 6                             &  0.0031 & -0.0339 &  0.0401 &  0.0141 & -0.0219 & 0.0500 \\
        \rowcolor{row_background} Lag 7   & -0.0015 & -0.0384 &  0.0354 &  0.0103 & -0.0256 & 0.0462 \\
        Lag 8                             & -0.0002 & -0.0369 &  0.0364 & -0.0170 & -0.0525 & 0.0185 \\
        \rowcolor{row_background} Lag 9   &  0.0753 &  0.0431 &  0.1075 &  0.0834 &  0.0512 & 0.1155 \\
%
        {$\R^2$} & \multicolumn{3}{c}{\formatRSquared{75.5853}}  & \multicolumn{3}{c}{\formatRSquared{74.4466}} \\
%
        \rowcolor{row_background} {Innovation Variance} & \multicolumn{3}{c}{\num{0.3139899}} & \multicolumn{3}{c}{\num{0.2492025454141095}} \\
        \bottomrule
    \end{tabularx}
\end{table}

\phantomsection
\addcontentsline{toc}{subsection}{Vector Autoregression Models}

\begin{table}[htb]
    \caption{Vector Autoregression Models: VAR(BIC)} 
    \label{tbl:volatility_var_results}

    \centering


    \sisetup{
        detect-mode,
        tight-spacing           = true,
        group-digits            = false,
        input-symbols           = {(}{)},
        input-open-uncertainty  = ,
        input-close-uncertainty = ,
        round-mode              = places,
        round-precision         = 2,
        table-align-text-pre    = false,
        table-align-text-post   = false,
        table-alignment         = center,
    }


   \begin{tabularx}{.9\textwidth}{X | *{2}{S >{{(}} S[table-space-text-pre={(}] <{{,\,}}
        S[table-space-text-pre={\hspace{-2\tabcolsep}}, table-space-text-post={)}] <{{)}}}} 
%
        \toprule 
        & \multicolumn{3}{c}{$\log(\diffvolt)$} & \multicolumn{3}{c}{$\log(\jumpvolt)$} \\
        \midrule
%
        \rowcolor{row_background}  
        Intercept               & -0.3268   & -0.5446   &  -0.1089   & -0.8841   & -0.7312 & -0.6909    \\
        $\log(\diffvol{t-1})$    &  0.4025   &  0.3618   &   0.4431   &  0.2352   &  0.2388 &  0.2712    \\
        $\log(\jumpvol{t-1})$    &  0.2488   &  0.2032   &   0.2943   &  0.2969   &  0.1883 &  0.3372    \\
%
        \rowcolor{row_background}  $\log \diffvol{t-2}$ 
                                &  0.1146   &  0.0721   &   0.1571   &  0.0072   & -0.0610 &  0.0449    \\
        \rowcolor{row_background}  $\log \jumpvol{t-2}$ 
                                &  0.0117   & -0.0349   &   0.0583   &  0.1254   &  0.0926 &  0.1667    \\ 
%
        $\log(\diffvol{t-3})$    &  0.0523   &  0.0096   &   0.0949   & -0.0089   & -0.1152 &  0.0289    \\
        $\log(\jumpvol{t-3})$    & -0.0041   & -0.0509   &   0.0427   &  0.0591   &  0.0178 &  0.1006    \\
%
        \rowcolor{row_background} $\log \diffvol{t-4}$  
                                &  0.0652   &  0.0227   &   0.1077   & -0.0277   & -0.0842 &  0.0100    \\
        \rowcolor{row_background}  $\log \jumpvol{t-4}$ 
                                &  0.0123   & -0.0343   &   0.0589   &  0.1076   &  0.0498 &  0.1489    \\
%
        $\log(\diffvol{t-5})$   &  0.0337   & -0.0068   &   0.0742   & -0.0205   & -0.0373 &  0.0154    \\
        $\log(\jumpvol{t-5})$   &  0.0410   & -0.0046   &   0.0867   &  0.1380   & -0.0059 &  0.1785    \\
%
        \rowcolor{row_background}  
        $\R^2$  & \multicolumn{3}{c}{\formatRSquared{76.2833}} & \multicolumn{3}{c}{\formatRSquared{75.394}} \\
%
        Innovation Covariance   &  \multicolumn{6}{c}{$\displaystyle \left(\begin{array}{cc} \num{0.3059} &
        \num{0.1654} \\ \num{0.1654}  & \num{0.2406} \end{array}\right)$} \\ 
%
    \bottomrule 
   \end{tabularx}

\end{table}

\phantomsection
\addcontentsline{toc}{subsection}{Log-Volatility Correlations}

\begin{table}[htb]
  \caption{Log-Volatility Correlations}

  \label{tbl:log_vol_correlations}
  \centering

  \begin{tabularx}{\textwidth}{X | *{6}{S}}
    \toprule
    & {$\log(\diffvolt)$} & {$\log\left(\jumpvolt\right)$} & {$\log\left(\totalvolt\right)$} &
     {$\log\left(\jumppropt\right)$} & {$\rxt$} & {$\FOMC_t$} \\
    \midrule
%
    $\log\left(\diffvolt\right)$  & 1.000   & 0.8975  & 0.9696  & -0.5006  & -0.1785 & 0.0550 \\
%
    \rowcolor{row_background}
    $\log\left(\jumpvolt\right)$  & 0.8975  & 1.000   & 0.9764  & -0.0779  & -0.1423 & 0.0886 \\
%
    $\log\left(\totalvolt\right)$ & 0.9696  & 0.9764  & 1.000   & -0.2914  & -0.1643 & 0.0761 \\
%
    \rowcolor{row_background}
    $\log\left(\jumppropt\right)$ & -0.2914  & -0.0779  & -0.2914  & 1.0000  & 0.1281 & 0.0411 \\
%
    \bottomrule
  \end{tabularx}

\end{table}


