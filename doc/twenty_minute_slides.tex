\documentclass[smaller, handout, aspectratio=169]{beamer}
\usetheme{Boadilla}
\usecolortheme{rose}
\usepackage{slides_preamble}
\title[Realized Densities]{Jumps, Realized Densities, and News Premia} 
\author[Paul Sangrey]{Paul Sangrey}
\institute{University of Pennsylvania \\ \href{mailto:paul@sangrey.io}{\textsf{paul@sangrey.io}}}
\date{SoFiE Summer School, Chicago \protect\\ \vspace{.5\baselineskip} July 26, 2018}


\begin{document}

\begin{frame}[plain, noframenumbering]
	\maketitle
\end{frame}

\section{Introduction}

\begin{frame}[c]{Return Dynamics}
    \centering

    \begin{figure}[t]
            \centering
            \caption*{\usebeamercolor[fg]{frametitle}\large S\&P 500 Log-Return}
            \begin{minipage}{.49\textwidth}
                \centering
                \subcaption*{$1$-Second}
            \end{minipage}%
            \begin{minipage}{.49\textwidth}
                \centering
                \subcaption*{Daily}
            \end{minipage}
            \includegraphics[width=\textwidth, height=.5\textheight]{spy_log_rtn.pdf}
            \caption*{\centering  \textcolor{red}{Red} lines are large jumps.} 
     \end{figure}%
     \vspace{-.5\baselineskip}

     \large 

     How do \red{jumps} affect the investors' \green{time-varying risk}? 

     \vspace{\baselineskip}

\end{frame}

\begin{frame}[c]{What is investors' \green{time-varying} risk?}

    \begin{tabularx}{.62\textwidth}{l l c l} 
                \textcite{ai2018risk}       &News   &  +    &Recursive utility \\
                \textcite[WP]{tsai2018pricing}  &Jumps  &  +     &Recursive utility
    \end{tabularx}%
%
    $\displaystyle \Bigg\rbrace \implies$  
%
    \begin{minipage}{.27\textwidth} \centering \green{CAPM fails!} \\ \vspace{.5\baselineskip} $\Bigg(
                \parbox{.9\textwidth}{\centering Risks aren't normal-times \\ covariances.  }\Bigg)$
    \end{minipage}

    \vfill

    \begin{minipage}{.2\textwidth} \hphantom{Problem 1:} \end{minipage}
    \begin{minipage}{.79\textwidth}
        \begin{itemize}
            \item[Goal:] Nonparametrically identify investors' risks at the daily frequency.
                \vspace{.5\baselineskip}
            \item[Problem 1:] News / Jumps are continuous-time phenomena. 
                \vspace{.5\baselineskip}
            \item[Problem 2:] No framework exists that simultaneously identifies both the daily and high-frequency
                dynamics if prices jump.
        \end{itemize}
    \end{minipage}

\end{frame}

\begin{frame}[c]{Talk Structure / Contributions}                                                                                       
    \begin{enumerate}
        \item[\large 1.] Propose a new measure of \red{\qt{jump volatility}} 
            \begin{enumerate} 
                \item[a)] that yields a tractable, nonparametric expression for the return's time-varying
                    distribution 
                \item[b)] that measures jumps in investor's information \green{(news)}       
            \end{enumerate}
%
        \pause
%
        \vspace{\baselineskip}
        \item[\large 2.]  
            \begin{enumerate}
                \item[a)] Show that by conditioning on the return's volatilities, no-arbitrage implies this
                    expression holds exactly.
               \item[b)] Derive estimators for the return's volatilities and distributions.
            \end{enumerate} 
%
        \pause
%
        \vspace{\baselineskip}
        \item[\large 3.] 
            \begin{enumerate}
                \item[a)] Estimate the volatilities using high-frequency data on the S\&P 500. 
                \item[b)] Show how to nonparametrically identify curvature in investors' recursive preference
                    aggregator. 
                \item[c)] Show that jump volatility commands a smaller premium than diffusive volatility. 
                \item[d)] Show \gentextcite{lucca2015prefomc} surprising FOMC premium can be fully explained as a
                    volatility premium.
            \end{enumerate} 
    \end{enumerate}
    \vfill
\end{frame}                                                                                                        


\begin{frame}[c]{Jumps $\coloneqq$  Discontinuities in the Price Process. }

    \vspace{\baselineskip}
    {\usebeamercolor[fg]{frametitle} What are Jumps? }
    \vspace{.5\baselineskip}

    \begin{itemize}
        \item Price changes caused by discrete (possibly small) releases of information. 
            \vspace{.5\baselineskip}
        \item They may be observed by only a few investors. 
           \vspace{.5\baselineskip}
       \item{They may come at unpredictable times.}
           \vspace{.5\baselineskip}
    \end{itemize}

    \pause

    \vspace{\baselineskip}
    {\usebeamercolor[fg]{frametitle} Examples:}
    \vspace{.5\baselineskip}

    \begin{itemize}
        \item FOMC Announcements.
           \vspace{.5\baselineskip}
        \item A startup announcing a new product line. 
           \vspace{.5\baselineskip}
        \item Effectively anything in a Bloomberg or Associated Press feed relevant for asset pricing.
           \vspace{.5\baselineskip}
        \item Private communications between investors. 
    \end{itemize}

\end{frame}

\begin{frame}[c]{Data Generating Process}

    \centering
    \begin{figure}[h]
        \caption*{\usebeamercolor[fg]{frametitle} \large Price Process (Stochastic Volatility
        \only<3-|handout:1>{Jump} Diffusion)\vspace{\baselineskip}}
%
        \begin{tikzpicture} 
            \node at (-3, 2) {\footnotesize Diffusion Volatility};
            \node at (-.4, 2.5) {\footnotesize Wiener Process};
            \draw[->, thick] (-.4, 2.25) -- (-.4, 1.6); 
            \onslide<1-|handout:1>{
                \draw[->, thick] (-1.75, 1.8) -- (-1.5, 1.5); 
                \node at (-1.27,1) {\large $ \displaystyle \purple{\dif p(t)} =
                \underbrace{\blue{\sigma(t) \dif W(t)}}_{\text{Diffusion Part}\ \blue{(\dif p^D)}} $};}
            \onslide<2-|handout:1>{\node at (3.05,1) {\large $\displaystyle \purple{+}
                    \underbrace{\red{\int_{X} \delta(t,x) (\mu - \nu) (\dif t, \dif
                x)}}_{\text{Jump Part}\ \red{(\dif p^J)}}$};
            \node at (3.5, 2.5) {\footnotesize Jump Magnitude Function};
            \draw[->, thick] (1.92, 2.25) -- (1.8, 1.6); 
            \node at (5.5, 2) {\footnotesize Demeaned Jump Locations};
        \draw[->, thick] (3.75, 1.8) -- (3.5, 1.5); }
        \end{tikzpicture} 
    \end{figure}

    \onslide<3-|handout:1> {
        \begin{figure}[h]
            \caption*{\usebeamercolor[fg]{frametitle} \large Returns }
            \begin{tikzpicture}  
                \node at (1,1) {\large $\displaystyle \purple{r_t} \coloneqq \int_{t-1}^t \dif p(s), \qquad \left. 
                    \purple{r_t} \mvert \F_{t-1} \right. \sim h\left(r_t \mvert \F_{t-1}\right)$};
            \end{tikzpicture}
        \end{figure}
    }
\end{frame}

\begin{frame}[c]{The Literature Focuses on Models of the Form:}

    \centering 
    \vspace{.5\baselineskip}

    \begin{figure}[h]
    \begin{tikzpicture}
        \node at (1,1) {\large $\displaystyle \purple{\left. r_t \mvert \F_{t-1} \right. \sim h\left(r_t
            \mvert \F_{t-1}\right) = \int_{x_t} f\left(r_t \mvert x_t\right) \dif G \left(x_t \mvert
            \F_{t-1}\right)} $}; 
        \node at (4.5,.25) {\footnotesize Sufficient Statistic for the Dynamics};
        \draw[->, thick] (2.2, .25) -- (1.55, .55); 
    \end{tikzpicture}
    \end{figure}

    \begin{equation*}
    \text{How should we model}\ \purple{h(r_t | \F_{t-1})}? \to\
    \begin{cases}
        \text{What should we use for}\ \purple{x_t}? \\
        \text{What should we use for}\ \purple{f}? \\ 
        \text{What should we use for}\ \purple{G}? \\
    \end{cases}
    \end{equation*}

    \vspace{.5\baselineskip} 

    \begin{block}{Example: Simple Stochastic Volatility Model}

        \begin{minipage}{.49\textwidth} 
            \begin{align*}
                            r_t  &\sim \sigma_t \N(0,1) \\
                \log \sigma_t^2  &= \rho \log \sigma^2_{t-1} + \sigma_{\sigma} \N(0,1) \\
            \end{align*}
        \end{minipage}%
        %
        \begin{minipage}{.49\textwidth} 
        \begin{itemize}
            \item $x_t$ is the volatility $\sigma^2_t$.
            \item $f\left(r_t \mvert \sigma^2_t\right)$ is $\N(0,\sigma^2_t)$.
            \item $G$ is $AR(1)$. 
        \end{itemize}
    \end{minipage}
    \end{block} 
\end{frame}


\begin{frame}[c, label=jump_vol_defn]{This Paper (Jump Diffusion)}
 
    \begin{table}[t]
    \caption*{\usebeamercolor[fg]{frametitle} \large Two Kinds of Volatility:}

    \begin{tabularx}{\textwidth}{X X} Diffusion Volatility & \onslide<2->{Jump Volatility} \\ \\
        $\blue{\sigma^2(t)} \coloneqq \lim\limits_{\Delta \to 0} \frac{1}{\Delta} \E\left[ \abs*{\int_{t}^{t +
            \Delta} \dif p^{\blue{D}} (s) }^2\, \mvert \F_{t}\right]$ 
        & \onslide<2->{$\red{\gamma^2(t)} \coloneqq \lim\limits_{\Delta \to 0} \frac{1}{\Delta} \E\left[
          \abs*{\int_{t}^{t + \Delta} \dif p^{\red{J}} (s) }^2\, \mvert \F_{t} \right]$}
    \end{tabularx}
    \end{table}

    \begin{figure}[h]
        \onslide<3->{\caption*{\usebeamercolor[fg]{frametitle} \large Realized Density: \\ \normalsize
        \color{black} (Derived Below)}}
        \begin{tikzpicture}
            \onslide<3->{
                \node at (-6.3,0) {\purple{$RD_t$}}; 
                \node at (-3,-.22) {$\displaystyle \coloneqq f\left(r_t\mvert \blue{\sigma^2_t}, \red{\gamma^2_t}
                    \right) = f\Bigg\rvert_{x_t = \begin{bmatrix} \int_{t-1}^t \blue{\sigma^2(s)} \dif s \\
                    \int_{t-1}^t \red{\gamma^2(s)} \dif s \end{bmatrix}}$}; 
            }
            \onslide<4->{
                \node at (1.7, 0) {$\displaystyle = \blue{\N}\left(0, \int_{t-1}^t \blue{\sigma^2(s)} \dif
                    s\right)$}; 
            }
            \onslide<5->{
                \node at (3.7, 1.2) {\footnotesize Convolution};
                \draw[->, thick] (3.7, .95) -- (3.7, .25); 
                \node at (3.7, 0) {$\ast$}; 
                \node at (5.4,0) {$\displaystyle \red{\Lap}\left(0, \int_{t-1}^t \red{\gamma^2(s)} \dif
                    s\right)$}; 
                \node at (1.7, -.9) {\footnotesize Laplace Distribution};
                \draw[->, thick] (3.1, -.8) -- (4, -0.2); 
            }
        \end{tikzpicture}

        \raggedright
        \hyperlink{decomposition_of_gamma}{\beamerbutton{Decomposition}}
    \end{figure}

\end{frame}


\begin{frame}[c, label=continuous_time_model]{Continuous-Time Model}

    \textbf{Recall:}
%
    \begin{equation*}
        \dif p(t) = \blue{\sigma(t)} \dif W(t) + \int_{\mathcal{X}} \delta(t, x) (\mu - \nu) (\dif t, \dif x)  
    \end{equation*}
%
    \pause
    \textbf{Allow for drift $\mu(t)$:}
%
    \begin{equation*}
        \dif p(t) = \mu(t) \dif t + \blue{\sigma(t)} \dif W(t) + \int_{\mathcal{X}} \delta(t, x) (\mu - \nu) (\dif
        t, \dif x)  
    \end{equation*}
%
    \pause
    \textbf{Simplify jump representation:}
%
    \begin{equation*}
        \dif p(t) = \mu(t) \dif t + \blue{\sigma(t)} \dif W(t) +
        \Ccancel[hookersgreen]{\red{\frac{\gamma(t)}{\sqrt{2}} \dif \Lap(t)}}{\int_{\mathcal{X}} \delta(t, x) (\mu
        - \nu) (\dif t, \dif x)}  
    \end{equation*}

    \hyperlink{timechangetheorem}{\beamerbutton{Details}}
\end{frame}



\section{Empirics}

\begin{frame}[c,plain, noframenumbering] 

    \centering
    \begin{beamercolorbox}[sep=8pt,center,shadow=true,rounded=true]{title}
        \usebeamerfont{title}{Empirics}
    \end{beamercolorbox}

\end{frame}


\begin{frame}[c, label=volatility_graph]{Volatilities \& Returns}
    \vspace{-2\baselineskip}
    \begin{figure}[t]
        \includegraphics[width=.75\textwidth, height=.7\textheight]{volatilities.pdf}
    \end{figure}%
%
    \vfill \hfill \hyperlink{estimator_intuition}{\beamerbutton{Details}}
\end{frame}



\begin{frame}[c]{Are the Realized Densities Good Conditional Densities?}
    
    \begin{equation*}
        \purple{RD_t} = \blue{\N}\left(0, \int_{t-1}^t \blue{\sigma^2(s)} \dif s\right) \ast \red{\Lap}\left(0,
        \int_{t-1}^t \red{\gamma^2(s)} \dif s\right)
    \end{equation*}

    \begin{figure}[h]
        \begin{subfigure}[t]{.49\textwidth}
            \caption*{\purple{$RD_t$} and \purple{$r_t$}}
            \includegraphics[width=\textwidth, height=.55\textwidth]{realized_density.pdf}  
        \end{subfigure}% 
        %      
        \begin{subfigure}[t]{.49\textwidth}
            \caption*{Quantile-Quantile Plot}
            \includegraphics[width=\textwidth, height=.55\textwidth]{jump_diff_realized_density_qq.pdf} 
        \end{subfigure}
    \end{figure}

\end{frame}


\section{Volatility and News Premia}

\begin{frame}[c,plain, noframenumbering] 

    \centering
    \begin{beamercolorbox}[sep=8pt,center,shadow=true,rounded=true]{title}
        \usebeamerfont{title}{Volatility and News Premia}
    \end{beamercolorbox}

\end{frame}


\begin{frame}[c, label=news_premia_claim]{Example: Epstein-Zin Preferences}

    \vspace{\baselineskip}
    \begin{itemize}
        \item $\rho$: risk aversion
        \item $\psi$: intertemporal elasticity of substitution (IES)
    \end{itemize}
    \vspace{\baselineskip}

    \centering 
    \begin{equation*}
         U_t = \left[(1-\beta)C_t^{1 - 1 / \psi} + \beta \E\left[U_{t+ \Delta}^{1-\rho}\mvert \F_{t}
         \right]^{\frac{1 - 1 / \psi}{1 - \rho}}\right]^{\frac{1}{1 - 1 / \psi}}  
    \end{equation*}

    \pause
    \begin{itemize} 
        \item  Define $V_t \coloneqq U_{t}^{1 - 1 / \psi}$. \quad\quad\qquad $V_t  = \left[(1 - \beta) C_t^{1 -
            1/\psi} + \beta \E\left[V_{t + \Delta}^{\frac{1 - \rho}{1-1 / \psi}} \mvert \F_{t} \right]^{\frac{1 -
            1 / \psi}{1 - \rho}}\right]$ 
%
        \pause
        \item Define $\phi(V) \coloneqq \frac{1- \rho}{1 - 1 / \psi} V^{\frac{1-\rho}{1 - 1 / \psi}}. \quad\quad
            = \left[(1 - \beta) C_t^{1 - 1/\psi} + \beta \phi^{-1} \left(\E\left[\phi(V_{t + \Delta}) \mvert
            \F_{t} \right]\right)\right]$
    \end{itemize}

\end{frame}


\begin{frame}[c]{News Premia: Measurement}
    \begin{figure}[t]
%
        \begin{tikzpicture} 
            \node (a) at (0,0) {Risk Premium on Asset $i$}; 
%
            \node (b) [right=-.15cm of a, align=center] {$\displaystyle \propto -\langle\purple{m(t)},
                p^{\blue{D}}_i(t) + p^{\red{J}}_i(t) \rangle \dif t\, -$};
%
            \node(e) [above left=.75cm and -1.85cm of b, align=center]  {$\log \purple{\frac{V_t'}{\E[V_t']}}$}; 
            \draw[->, thick] (e) -- ([yshift=-.75cm] e.south);
%
            \node (c) [right=-.15cm of b, align=center] {$\displaystyle \langle \red{m_{*}}(t), p^{\red{J}}_i(t)
                \rangle \dif t$};
%
            \node(d) [above left=.75cm and -1.45cm of c, align=center]  {$\log
                \red{\frac{\phi'(V_t)}{\E[\phi'(V_t)]}}$}; 
            \draw[->, thick] (d) -- ([yshift=-.75cm] d.south);
        \end{tikzpicture}
    \end{figure}

        \vspace{\baselineskip}

        \begin{itemize}
            \item Concavity of $V \iff \purple{m(t)}$ increases in bad times  $\iff$ Positive Risk Premium
                for \totalvolt.
                \vspace{\baselineskip}
            \item Convexity of $\phi \iff \red{m_{*}(t)}$ increases in good times $\iff$ Negative Risk
                Premium for $\jumppropt$.
        \end{itemize}
\end{frame}

\begin{frame}[c]{$\purple{\rxt} = \beta_0 + \beta_1 \green{\FOMC_t} + \beta_2 \log\left(\totalvolt\right) +
    \beta_3 \log\left(\jumppropt\right) + \epsilon_t$}
    
    \begin{minipage}{.4\textwidth}
        \begin{tabular}{l c}
            Data: & \green{SPY S\&P 500 ETF}  \\
                  & Jan. 2003 -- Sep. 2017   \\
                  & ($\approx$\, 2900 Daily Observations)
        \end{tabular}
    \end{minipage}%
%
        \begin{minipage}{.55\textwidth}
            \begin{table}[htb]
                \centering
                \caption{OLS}
                \label{tbl:contemporaneous_relationship_return_and_vol}
                \sisetup{
                    table-align-text-pre=false,
                    table-align-text-post=false,
                    round-mode=places,
                    scientific-notation=false,
                    table-format=2.2,
                    round-precision=2,
                    table-space-text-pre=\lbrack,
                    table-space-text-post=\rbrack,
                }
            
                \begin{tabularx}{\textwidth}{*{4}{S}}
                    \toprule
%
                   {Intercept} & {$\FOMC_t$} & {$\log\left(\totalvolt\right)$} & {$\log\left(\jumppropt\right)$} \\
                    \\
%
                    \midrule
                    0.0069 & 0.9836& &  \\ {}
                    [0.1483] & [3.6387] & & \\
%
                    \rowcolor{row_background} 
                    -3.7598 & & -0.4102 & \\ 
                    \rowcolor{row_background}  {}
                    [-5.4027] & & [-5.7009] &  \\
%
                    1.2022 & & & 1.6747  \\ {}
                    [6.0458] & & & [5.5062]  \\
%
                    \rowcolor{row_background} 
                    -2.7232 & & -0.3539 & 0.7442  \\  
                    \rowcolor{row_background}  {}
                    [-3.1070] & & [-4.3568] & [2.2925] \\  
%
                    -2.8560 & 1.1764 & {\cellcolor{purple!50} \num{-0.3674}} & {\cellcolor{red!50} \num{0.8012}} \\  {}
                    [-3.300] & {\cellcolor{hookersgreen!50} [\num{4.3574}]} & [-4.4601] & [2.4521] \\
%
                    \bottomrule
                \end{tabularx}
            
            \end{table}
        \end{minipage}
        

\end{frame}


\begin{frame}[c]{$\purple{\rxt} = \beta_0 + \beta_1 \green{\FOMC_t} + \beta_2 \log\left(\totalvolt\right) +
    \beta_3 \log\left(\jumppropt\right) + \epsilon_t$}
    
    \begin{table}[htb]
        \label{tbl:jump_premia_estimates}
    
        \centering
    
        \caption{GMM}
    
        \sisetup{
            table-align-text-pre=false,
            table-align-text-post=false,
            round-mode=places,
            scientific-notation=false,
            table-format=2.2,
            round-precision=2,
            table-space-text-pre=\lbrack,
            table-space-text-post=\rbrack,
        }
    
        \begin{tabularx}{.55\textwidth}{*{4}{S}} 
           \toprule
    %
           {Intercept} & {$\FOMC_t$} & {$\log\left(\totalvolt\right)$} & {$\log\left(\jumppropt\right)$} \\
           \midrule
    %
           \rowcolor{row_background} 
           3.2239  & & 0.2945  & \\
           \rowcolor{row_background}  {}
           [6.3995] & & [5.8629] & \\
    %
           -1.6072 & &  & -2.9642  \\  {}
           [-5.3149] &  &  & [-6.3445]  \\
    %
           \rowcolor{row_background}
           0.8133 & & 0.1906 & -2.1595  \\ 
           \rowcolor{row_background} {}
           [1.0999] & & [3.3745] & [-4.4775]  \\ 
    %
           0.8305 & 0.0868 & {\cellcolor{purple!50} \num{0.1921}} & {\cellcolor{red!50} \num{-2.1488}} \\ {}
           [1.1362] & {\cellcolor{hookersgreen!50} [\num{0.5123}]} &  [3.4293] & [-4.4626] \\
    %%
           \bottomrule
        \end{tabularx}
    \end{table}
    

\end{frame}



\begin{frame}[c]{Talk Structure / Contributions (Recap)}                                                                                       
    \begin{enumerate}
        \item[\large \green{\cmark} 1.] Propose a new measure of \red{jump volatility}
                    \vspace{.5\baselineskip}
            \begin{enumerate} 
                \item[\green{\cmark} a)] that yields $\purple{r_t} \sim \blue{\N} \left(0,
                    \blue{\sigma^2_t}\right) \ast \red{\Lap}\left(0, \red{\gamma^2_t}\right)$                 
                    \vspace{.5\baselineskip}
                \item[\green{\cmark} b)]  that measures jumps in investor's information \green{(news)}
            \end{enumerate}
            \vspace{\baselineskip}
        \item[\large \green{\cmark} 2.]  
            \begin{enumerate}
                \item[ \green{\cmark} a)] Show that by using $\purple{x_t} = \left[\blue{\sigma_t^2},
                    \red{\gamma_t^2}\right]$, no-arbitrage implies this expression holds exactly.
                \item[\green{\cmark} b)] Derive estimators for \blue{$\sigma_t^2$}, \red{$\gamma_t^2$}, and
                    \purple{$RD_t = f\left(r_t \mvert \sigma^2_t, \gamma^2_t\right)$}. 
            \end{enumerate} 
            \vspace{\baselineskip}
        \item[\large \green{\cmark}  3.] 
            \begin{enumerate}
                \item[a)]  Estimate \blue{$\sigma^2_t$}, \red{$\gamma^2_t$} and \purple{$RD_t$}, using high-frequency data
                    on the S\&P 500.  
                    \vspace{.5\baselineskip}
                \item[b)] Show jump volatility commands a smaller premium than diffusion volatility. 
                    \vspace{.5\baselineskip}
                \item[c)] Show conditional on expected volatility, FOMC days are not special. 
            \end{enumerate} 
    \end{enumerate}
\end{frame}                    

                              
\appendix                    
                            
\begin{frame}[c, label=decomposition_of_gamma]{Decomposition of $\gamma^2_t$ in Compound Poisson Case}

    \begin{alignat*}{2}
        & \red{\gamma^2_t} &&= \lim_{\Delta \to 0} \frac{1}{\Delta} \E\left[\,\abs*{\red{\int_{t}^{t +
    \Delta} \dif p^{J} (s)^2}} \mvert \F_{t}\right]  \\ \vspace{.5\baselineskip}
        & &&= \lim_{\Delta \to 0} \frac{1}{\Delta} \E\left[ \E\left[ \sum_{s \in (t, t+\Delta]}(\text{jump
          magnitude})^2(s)) \mvert \F_{t},  s\ \text{is a jump} \right]\right] \\ \vspace{.5\baselineskip}
        & &&= \lim_{\Delta \to 0} \frac{1}{\Delta} \E\left[(\text{jump magnitude})^2\right]\E\left[\#\ \text{of
          jumps in}\ (t, t+\Delta\right]
        \label{eqn:jump_variance_decomposition}
    \end{alignat*}

    \vfill \hfill \hyperlink{jump_vol_defn}{\beamerbutton{Return}}
\end{frame}

\begin{frame}[c, label=timechangetheorem]{Time-Change Theorem}
            
    \vspace{\baselineskip}

    {\usebeamercolor[fg]{frametitle}\large Assumptions: }

    \begin{enumerate}
        \item $p(t)$ jumps in every interval.  
%
                \vspace{.25\baselineskip}
            \begin{itemize}
                \item[--] Jumps are very frequent / infinitely-active \parencite{aitsahalia2012analyzing,
                        gallant2018exact}.
            \end{itemize}
            \vspace{.5\baselineskip}
%
        \item $r_t$ has finite variance. ($p(t)$ is locally-square integrable.)
            \vspace{.5\baselineskip}
%
        \item  $p(t)$ has no predictable jumps.
            \vspace{.5\baselineskip}
%
        \item Innovations to prices and volatilities are independent.  The drift and volatilities may be
            correlated.  
    \end{enumerate}
    
    \vspace{\baselineskip}
    
    \begin{theorem}
        1 -- 3 imply $p^{\red{J}}(t)$ time-changed by its predictable quadratic variation is a standard
        variance-gamma process.  
    \end{theorem}

    \vfill \hfill \hyperlink{continuous_time_model}{\beamerbutton{Return}}
\end{frame}

\begin{frame}[c, label=estimator_intuition]{Estimation Strategy}

    \begin{enumerate}
        \item We want to estimate $\blue{\sigma_t^2}$, $\red{\gamma^2_t}$, and $\purple{RD_t}$.
            \vspace{\baselineskip}
        \item Estimating $\blue{\sigma_t^2} + \red{\gamma_t^2}$  is straightforward. How do we separate
            $\blue{\sigma_t^2}$ and $\red{\gamma_t^2}$?
            \vspace{\baselineskip}
        \item Adapt a truncation-based estimator from the literature to estimate \blue{$\sigma^2(t)$}.
            \parencites{barndorff2004power,jacod2013quarticity}.  
            \vspace{\baselineskip}
        \item Estimate $\red{\gamma^2(t)}$ by computing \purple{$\E[\abs*{p(t)}]$} as a function of
            \red{$\gamma^2(t)$} and \blue{$\sigma^2(t)$} and then solving for \red{$\gamma^2(t)$}.
            \vspace{\baselineskip}
        \item Estimate $\blue{\sigma^2_t}$ and $\red{\gamma^2_t}$ as the average of $\blue{\hat{\sigma}^2(t)}$ and
            $\red{\hat{\gamma}^2(t)}$ over the day. 
            \vspace{\baselineskip}
        \item Plug in $\red{\hat{\gamma}^2_t}$ and $\blue{\hat{\sigma}^2_t}$ into $\N \left(0, \blue{\sigma^2_t}
            \right) \ast \Lap\left(0, \red{\gamma^2_t} \right)$ to estimate \purple{$RD_t$}.
    \end{enumerate}

     \hyperlink{volatility_graph}{\beamerbutton{Return}}

\end{frame}
                                

\end{document}

