from hypothesis import given, assume, settings, strategies as st
import pandas as pd
import numpy as np
import sympy as sym
from scipy import special


# Derive the logpdf for the laplace-gaussian convolution 
x, y, z = sym.symbols('x y z', real=True)
gamma, sigma = sym.symbols('gamma sigma', real=True, positive=True)
lap_dens = (sym.sqrt(2) * gamma)**(-1) * sym.exp(-sym.sqrt(2) * (abs(x) / gamma))
gauss_dens = (sym.sqrt(2 * sym.pi) * sigma)**(-1) * sym.exp(- (x**2 / sigma**2) / 2)
conv_dens = sym.integrate(lap_dens * gauss_dens.replace(x, z-x), (x, -sym.oo, sym.oo)).simplify().expand()
log_conv_dens = sym.lambdify((z, sigma, gamma), sym.log(conv_dens).simplify(), modules=['numpy', 'math'])

# Derive the  the conditional variance of the gaussian and laplace parts for the laplace-gaussian convolution
integrand1 = (x**2 * lap_dens * gauss_dens.replace(x, z-x)).simplify()
integrand2 =  (x**2 * lap_dens.replace(x, z-x) * gauss_dens).simplify() 
diffusion_var = (sym.integrate(integrand2, (x, -sym.oo, sym.oo))).simplify()
diffusion_var_lambda = sym.lambdify((z, sigma, gamma), diffusion_var, modules=['numpy', 'math'])
jump_var = (sym.integrate(integrand1, (x, -sym.oo, sym.oo))).simplify()
jump_var_lambda = sym.lambdify((z, sigma, gamma), jump_var, modules=['numpy', 'math'])

#Derive the conditional variance of the gaussian and laplace parts for the expected shortfal.
es_integrand1 = (lap_dens.replace(abs(x), -x) * gauss_dens.replace(x, y)).simplify()
es_integrand2 = (lap_dens.replace(abs(x), x) * gauss_dens.replace(x, y)).simplify()


def test_package():
    """ Ensure that I can import the package."""

    import laplacejumps


@given(st.integers(min_value=1, max_value=1000),st.integers(min_value=1, max_value=100))
def test_generating_dates(infill_dim, time_dim):
    """ Runs the dates and ensures that it is the right size and sorted."""

    from laplacejumps import generate_business_dates

    dates = generate_business_dates(infill_dim, time_dim)

    assert dates.size == infill_dim * time_dim, 'The generated dates are not the correct size.'

    assert all(dates.reset_index().set_index(0).groupby(pd.Grouper(freq='D')).count() == infill_dim), \
        'We do not have the correct number of observations in each day.'

    assert all(dates.sort_values() == dates), 'The dates are not sorted.'


@settings(max_examples=10)
@given(st.floats(min_value=.01, max_value=20), st.floats(min_value=.01, max_value=10), 
       st.floats(min_value=.01, max_value=10)) 
def test_simulate_cir(kappa, omega, theta):
    """ Tests the simulate the Cox-Ingersoll-Rox Simulation """

    from laplacejumps import simulate_cir

    time_dim = 250
    infill_dim = 240
    # The values must be kind of reasonable. This is the condition for avoiding zero except it's missing a 2.
    assume(kappa * theta >= omega**2) 
                                     
    data = simulate_cir(kappa, omega, theta, time_dim=time_dim, infill_dim=infill_dim)

    assert np.isclose(data.mean(), theta, rtol=.7, atol=.7), "The mean is not in the right order of magnitude."
    
    assert np.isclose(data.var(), (theta * omega**2) / (2 * kappa), rtol=.7, atol=.5), \
                "The variance is not in the right order of magnitude."

@given(st.lists(st.one_of(st.just(np.nan), st.floats(max_value=100, min_value=-100))))
def test_real_vol(x):
    
    from laplacejumps import real_vol
    
    x = np.asarray(x)
    
    if np.allclose(x,0):
        pass
    elif x.size > 0 and np.all(np.isfinite(x)):
        assert real_vol(x) > 0, 'The realized volatility is not positive.'
    else:
        assert not np.isfinite(real_vol(x)), \
                'The real_vol function is not returnning nan when it is supposed to.'


@given(st.floats(min_value=.01, max_value=2), st.floats(min_value=.5, max_value=1), st.floats(min_value=.5,
                                                                                             max_value=1))
def test_lapgaussconv_logpdf(x, sigma_val, gamma_val):

    from laplacejumps import lapgaussconv

    with np.errstate(divide='raise'):
        try:
            val1 = lapgaussconv.logpdf(x, sigma=sigma_val, gamma=gamma_val)
            val2 =  log_conv_dens(x, sigma_val, gamma_val)

        except FloatingPointError:
            pass
        else:
            assert np.isclose(val1,val2, rtol=1e-2, atol=1e-2)
                      

@given(st.floats(min_value=.01, max_value=2), st.floats(min_value=.5, max_value=1), st.floats(min_value=.5,
                                                                                             max_value=1))
def test_diffusion_cond_var(x, sigma, gamma):

    from laplacejumps import diffusion_cond_var

    with np.errstate(divide='raise'):

        try: 
            val1 = diffusion_cond_var(x, sigma, gamma)
            val2 = diffusion_var_lambda(x, sigma, gamma)

        except FloatingPointError:
            pass
        else:
            assert np.isclose(val1,val2, rtol=1e-2, atol=1e-2)


@given(st.floats(min_value=.01, max_value=2), st.floats(min_value=.5, max_value=1), st.floats(min_value=.5,
                                                                                             max_value=1))
def test_diffusion_cond_var(x, sigma, gamma):

    from laplacejumps import jump_cond_var 

    with np.errstate(divide='raise'):

        try: 
            val1 = jump_cond_var(x, sigma, gamma)
            val2 = jump_var_lambda(x, sigma, gamma)

        except FloatingPointError:
            pass
        else:
            assert np.isclose(val1,val2, rtol=1e-2, atol=1e-2)


def test_es_diffusion_variance():

    from laplacejumps import es_diffusion_variance 

    val_y1 = sym.integrate(sym.integrate(y**2 * es_integrand1, (x, z-y, 0)), (y, z, sym.oo)).simplify()
    val_y2 = sym.integrate(sym.integrate(y**2 * es_integrand2, (x, 0, sym.oo)), (y, z, sym.oo))
    val_y3 = sym.integrate(sym.integrate(y**2 * es_integrand2, (x, z-y,sym.oo)), (y, -sym.oo, z))
    es_y_var_lambda = sym.lambdify((z, sigma, gamma), 2 * (val_y1 + val_y2 + val_y3).simplify(),
                                   modules=['numpy', 'math'])

    args = [(0,1,1), (2,1,1), (.3,2, .7), (.1, .1, 5), (.2, .3, .1)]
    for arg in args:
        assert np.isclose(es_diffusion_variance(*arg), es_y_var_lambda(*arg))


def test_es_jump_variance():

    from laplacejumps import es_jump_variance

    val_x1 = sym.integrate(sym.integrate(x**2 * es_integrand1, (x, z-y, 0)), (y, z, sym.oo)).simplify()
    val_x2 = sym.integrate(sym.integrate(x**2 * es_integrand2, (x, 0, sym.oo)), (y, z, sym.oo))
    val_x3 = sym.integrate(sym.integrate(x**2 * es_integrand2, (x, z-y,sym.oo)), (y, -sym.oo, z))
    es_x_var_lambda = sym.lambdify((z, sigma, gamma),2 * (val_x1 + val_x2 + val_x3).simplify(), 
                                   modules=['numpy', 'math'])

    args = [(0,1,1), (2,1,1), (.3,2, .7), (.1, .1, 5), (.2, .3, .1)]
    for arg in args:
        assert np.isclose(es_jump_variance(*arg), es_x_var_lambda(*arg))


def test_es_covariance_term():

    from laplacejumps import es_covariance_term

    val_xy1 = sym.integrate(sym.integrate(x * y * es_integrand1, (x, z-y, 0)), (y, z, sym.oo)).simplify()
    val_xy2 = sym.integrate(sym.integrate(x * y * es_integrand2, (x, 0, sym.oo)), (y, z, sym.oo))
    val_xy3 = sym.integrate(sym.integrate(x * y * es_integrand2, (x, z-y,sym.oo)), (y, -sym.oo, z))

    es_xy_var_lambda = sym.lambdify((z, sigma, gamma), 2 * (val_xy1 + val_xy2 + val_xy3).simplify(),
                                    modules=['numpy', 'math'])

    args = [(0,1,1), (2,1,1), (.3,2, .7), (.1, .1, 5), (.2, .3, .1)]
    for arg in args:
        assert np.isclose(es_covariance_term(*arg), es_xy_var_lambda(*arg))

